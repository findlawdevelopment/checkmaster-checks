var Check = require('./master').Check,
		urllib = require('url');

module.exports = function () {
	'use strict';

	return new Check('Contextual images should have alt text that matches the title tag.', 'CM007', {
		requires: ['crawler'],
		resultType: 'list',
		onlyOn: {
			roles: ['Developer'],
			activities: []
		},
		passFail: [],

		testForContextual: function (ele) {
			/*
				tests an element and it's parent if it contains the class or ID of contextual

				ele - cheerio object of the image
				
				var ele = '<img class="contextual" />'
				testForContextual(ele)
				=> true
			*/

			var pass,
					elements = [
						ele.attr('class'),
						ele.attr('id'),
						ele.parent().attr('class'),
						ele.parent().attr('id')
					];

			elements.forEach(function (ele) {
				pass = pass || /contextual/.test(ele);
			});

			return pass;
		},

		findContextual: function ($) {
			/*
				Finds images in the cheerio object and checks and returns the contextual image

				$ - cheerio object of the page.html

				findContextual($)
				=> '<img src="/design/images/i-default.jpg" class="contextual" alt="There should be text here" />'
			*/

			var img,
					check = this,
					contextual = "";


			$('img').each(function (i, ele) {
				var $el = $(ele);

				if (check.testForContextual($el)) {
					contextual = $el;
				}

			});

			return contextual;
		},

		onPageCrawl: function (page, response, pagesCrawled) {
			var $ = page.dom(),
					contextual = this.findContextual($),
					title = $('title'),
					alt = '';

			if (contextual) {
				alt = contextual.attr('alt');
				
				/* should not pass until all pages crawled */

				var pass = alt === title.text();
				

				if (!pass) {
					this.data.push({
						url: page.url,
						reason: 'The current alt text is: ' + (alt || 'empty') + '. When it should be: ' + title.text(),
						src: urllib.resolve(page.url, contextual.attr('src'))
					});
					this.passed = false;
				}
				this.passFail.push(pass ? "true" : "false");
			}
		},
		onCrawlFinish: function (pagesCrawled) {
			// Mark as failed if not explicitly passed
			if (this.passFail.indexOf("false") > -1) {
				this.passed = false;
			}
			else {
				this.passed = true;
			}
		}
	});
};