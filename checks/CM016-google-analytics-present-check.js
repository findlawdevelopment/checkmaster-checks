var Check = require('./master').Check;
var request = require('request');
var Q = require('q');

module.exports = function () {
	'use strict';

	return new Check('The Google Analytics code matches the live website', 'CM016', {
		resultType: 'list',
		onlyOn: {
			activities: [],
			types: ['preview', 'internal']
		},
		checkForLiveSite: function(url, siteInfo) {
			var deferred = Q.defer();
			
			this.setSiteData(url, siteInfo);

			url = this.getSiteInfo().domain;
			// console.log('\n\n\n\n\n\n\n\n\n\n anything has been found \n\n\n\n\n\n\n\n\n');

			if (url) {

					// Add http protocol since the service omits it
					url = 'http://' + url;

					deferred.resolve(this.getHTML(url));

			} else {

				this.passed = false;
				deferred.resolve();

			}

			return deferred.promise;
		
		},
		getHTML: function(url) {

			var that = this,
					deferred = Q.defer(),
					results;

			request.get(url, function (error, response, body) {

				if (!error && response.statusCode === 200) {
					results = that.searchAnalyticTag(body);
				} else {
					results = {
						gaPresent: false
					};
				}

				deferred.resolve(results);

			});

			return deferred.promise;

		},
		searchAnalyticTag: function(html) {

			var gaComment = html.match(/<!-- Google Analytics Code -->/),
					gaScript = html.match(/.google-analytics.com\/ga.js/);

			if ((!!gaComment) || (!!gaScript)) {

				return {
					gaPresent: true
				};

			} 

			return {
				gaPresent: false
			};

		},
		run: function (page, siteInfo) {
			
			var deferred = Q.defer(),
					check = this,
					gaPreview,
					gaLive;

			Q.all([
				check.getHTML(page.url),
				check.checkForLiveSite(page.url, siteInfo)
			]).spread(function(preview, live){

				gaPreview = preview.gaPresent;
				gaLive = live.gaPresent;

				if (gaPreview === false && gaLive === true) {
			
					check.passed = false;
					deferred.resolve();

				} else {
					
					check.passed = true;
					deferred.resolve();

				}

			});

			return deferred.promise;

		}
	});
};