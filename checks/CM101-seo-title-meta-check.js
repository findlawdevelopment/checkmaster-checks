var cheerio = require('cheerio');
var Q = require('q');

var Check = require('./master').Check;

module.exports = function () {
  'use strict';

  return new Check('All title tags, meta descriptions, and meta keywords are present.', 'CM101', {
    requires: ['crawler'],
    resultType: 'list',
    onlyOn: {
      roles: ['Developer', 'QA Member', 'Digital Marketer']
    },

    // Constants
    ERRORS: {
      title: {
        contentPage: '<title> contains the phrase "Content Page"',
        empty: '<title> tag is empty',
        missing: '<title> tag is missing',
        onlyPageTitle: '<title> tag only contains Page Title text',
      },
      description: {
        empty: '<meta> description is empty',
        missing: '<meta> description is missing',
        incorrectLength: '<meta> description is not between 25 and 160 characters in length',
        notUnique: '<meta> description is not unique (see list of duplicates pages above)',
        numbersMissing: 'No phone number was found in the <meta> description',
        numbersIncorrect: 'An incorrect phone number was found in the <meta> description'
      },
      keywords: {
        empty: '<meta> keywords is empty',
        missing: '<meta> keywords is missing',
      }
    },

    CONSTRAINTS: {
      description: {
        minLength: 25,
        maxLength: 160
      }
    },

    // Saved data
    descriptions: [],

    onPageCrawl: function (page, response, pagesCrawled) {
      var $ = cheerio.load(page.html);
      var description;
      var pageTitle;

      // Don't run this against non-text/html pages
      if (page.type !== 'text/html' || page.isExternal === true) {
        return false;
      }

      description = this.getMeta($, 'description');

      // Check if description has already been used
      description = this.findDuplicates(page.url, description);

      // Add page results to check.data
      this.data.push({
        url: page.url,
        pageTitle: this.getPageTitle($),
        title: this.getTitle($),
        description: description,
        keywords: this.getMeta($, 'keywords')
      });
    },

    fail: function (result, error, errorList) {
      this.passed = false;
      result.error = error;

      if (errorList) {
        result.errorList = errorList;
      }

      return result;
    },

    getNumber: function (text) {
      if (typeof text !== 'string') return text;
      return parseInt(text.replace(/\D/g, ''), 10);
    },

    testNumbers: function (num1, num2) {
      return this.getNumber(num1) === this.getNumber(num2);
    },

    findNumber: function (text) {

      var numberTest = /(?:^|\b|\()(?:\d{3}(?:\)\s?|-|\.))\d{3}(-|\.)\d{4}/g;
      var check = this;

      if (check.getSiteInfo().country === 'uk' || check.getSiteInfo().country === 'gb') {
        numberTest = /\d{3,4} \d{3,4} \d{4}/g;
      }
      var numbers = Array.prototype.slice.call(text.match(numberTest) || []);
      var notNumPos = numbers.indexOf('-');
      if (notNumPos !== -1) {
        numbers.splice(notNumPos, 1);
      }

      return numbers;
    },

    testNumbersExist: function (str) {
      var check = this;
      
      var goodNumbers = [].concat(
        check.getSiteInfo().numbers.ct || [],
        check.getSiteInfo().numbers.office || []
      );

      if (!goodNumbers || goodNumbers.length < 1) {
        return;
      }
      
      var numbersFound = check.findNumber(str);

      if (numbersFound.length < 1) {
        return check.fail({ text: str }, check.ERRORS.description.numbersMissing);
      }

      var badNums = [];

      numbersFound.forEach(function (num, i) {
        var tests = [];

        goodNumbers.forEach(function (goodNum, i) {
          
          tests.push(check.testNumbers(num, goodNum.number));

        });

        if (tests.indexOf(true) === -1 && badNums.indexOf(num) === -1) {
          badNums.push(num);
        }
      });
      if (badNums.length > 0) {
        return check.fail({ text: str, badNumbers: badNums }, check.ERRORS.description.numbersIncorrect);
      }

      return badNums.length === 0;
    },

    findDuplicates: function (url, description) {
      var entry;
      var duplicates = [];
      var i = this.descriptions.length;
      var pageExists = false;

      // Check if there were any unrelated errors found for the description
      /// (we're skipping the duplicate check if true)
      var hasOtherErrors = description.hasOwnProperty('error') !== false &&
        description.error !== this.ERRORS.description.notUnique;

      // Check if any descriptions have been added yet (i should be -1)
      // Do not check if any errors were already encountered
      if (i !== -1 && hasOtherErrors === false) {
        // Loop through all existing descriptions to find a duplicate
        while (i--) {
          entry = this.descriptions[i];
          if (entry.description === description.text && entry.url !== url) {
            duplicates.push(entry.url);
          }
          if (entry.url === url) {
            pageExists = true;
          }
        }
      }

      // Do not save page to list of descriptions if it
      // has already been added
      if (pageExists === false) {
        // Save current page description in descriptions
        this.descriptions.push({
          url: url,
          description: description.text
        });
      }

      // Fail if any duplicates are found
      if (duplicates.length > 0) {
        return this.fail(description, this.ERRORS.description.notUnique, duplicates);
      }

      return description;
    },

    getPageTitle: function ($) {
      return $('h1').last().text().trim();
    },

    getTitle: function ($) {
      var result = {};
      var pageTitle = this.getPageTitle($);
      var titleTag = $('title');
      var titleText = titleTag.text();

      // Make sure a <title> tag was found
      if (titleTag.length === 0) {
        result.text = '(missing)';
        return this.fail(result, this.ERRORS.title.missing);
      }

      // Make sure the <title> tag has text
      if (titleText.trim() === '') {
        result.text = '(empty)';
        return this.fail(result, this.ERRORS.title.empty);
      }

      result.text = titleText;

      // Make sure there's more text than just the Page Title (<h1>)
      if (pageTitle !== '') {
        if (titleText.replace(pageTitle, '').trim() === '') {
          return this.fail(result, this.ERRORS.title.onlyPageTitle);
        }
      }

      // Make sure default phrases do not exist
      if (titleText.indexOf('Content Page') > -1) {
        return this.fail(result, this.ERRORS.title.contentPage);
      }

      return result;
    },

    getMeta: function ($, name) {
      var result = {};
      var element = $('meta[name="' + name + '"]');
      var elementText = element.attr('content') || '';
      var elementTextLength = elementText.length;

      // Make sure a <meta> element was found
      if (element.length === 0) {
        result.text = '(missing)';
        return this.fail(result, this.ERRORS[name].missing);
      }

      // Make sure the <meta> element has text
      if (elementText.trim() === '') {
        result.text = '(empty)';
        return this.fail(result, this.ERRORS[name].empty);
      }

      // Save meta element text
      result.text = elementText;

      // Make sure <meta> element matches length constraints
      if (
        this.CONSTRAINTS.hasOwnProperty(name) === true &&
        (
          elementText.length < this.CONSTRAINTS[name].minLength ||
          elementText.length > this.CONSTRAINTS[name].maxLength
        )
      ) {
        return this.fail(result, this.ERRORS[name].incorrectLength + ' (length is at ' + elementTextLength + ' characters)');
      }

      if (name === 'description') {
        var numResult = this.testNumbersExist(elementText);

        if (numResult && numResult.error) {
          return numResult;
        }
      }

      return result;
    },

    onCrawlFinish: function (pagesCrawled) {
      var check = this;

      // Check all pages for duplicates that were missed
      check.data.forEach(function (page) {
        // Replace page.description with updated result
        page.description = check.findDuplicates(page.url, page.description);
      });

      // Mark as failed if not explicitly passed
      if (this.passed !== false) {
        this.passed = true;
      }
    }
  });
};
