var Q = require('q');
var _ = require('underscore');
var Check = require('./master').Check;
var request = require('request');
var cheerio = require('cheerio');
var async = require('async');

module.exports = function () {

	// 1. The blog home page should have a short form present

	return new Check('The home page of the blog has a short form present.', '341', {
		include: [],
		requires: [],
		onlyOn: {
			roles: ['Developer', 'QA Member'],
			activities: [],
			types: ['blog', 'preview', 'internal']
		},
		run: function (page) {

			var deferred = Q.defer();
			var self = this;

			// Use async to
			async.parallel([
				function(callback){
					request.get(page.url, function (error, response, body) {
						if (!error && response.statusCode == 200) {
							
							// Load the body of the web page via Cheerio.
							$ = cheerio.load(body);
							var shortFormPresent = $('html').find('#containerIntakeFormShortAutofill, containerIntakeFormShort');

							// Validate that there is a short form tag
							if (shortFormPresent.length < 1) {
								self.passed = false;
							}
						}

						callback(error, null);
					});
				}
			],
			// optional callback
			function(err, results){

				if(self.passed !== false) {
					self.passed = true;
				}

				deferred.resolve();
			});

			return deferred.promise;
		}
	});
};