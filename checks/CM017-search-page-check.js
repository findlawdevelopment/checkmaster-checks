var Q = require('q');
var Check = require('./master').Check;
var url = require('url');
var request = require('request');

module.exports = function () {
	'use strict';

	return new Check('If a search form exists, so should a search results page', 'CM017', {
		resultType: 'list',
		onlyOn: {
			roles: ['Developer', 'QA Member', 'Digital Marketer'],
			activities: [],
			types: ['internal']
		},
		run: function (page, siteInfo) {
			var deferred = Q.defer();
			var check = this;
			request.get(page.url, function (err, response, body) {

				if (!err) {
					// if search form exists
					var $ = check.renderDom(body);

					if ($('#searchFormDesign').length > 0) {
						var searchResultsURL = url.resolve(page.url, 'Search-Results.shtml');
						request.get(searchResultsURL, function (err, response, body) {
							if (!err) {

								check.passed = true;
								check.data = [{ message: 'Sucessfully found the search page' }];
								deferred.resolve();

							} else {

								check.passed = false;
								check.data = [{ message: 'Search page was not found at: ' +  searchResultsURL}];

								deferred.reject();

							}

						});

					} else {

						console.log('no search form exists');
						check.passed = true;
						check.data = [{ message: 'No search form was found.' }];
						deferred.resolve();

					}


				} else {
					
					check.passed = false;
					check.data = [{ message: 'An error occurred when requesting the home page of the site' }];
					deferred.reject(err);

				}

			});

			return deferred.promise;
		}
	});
};