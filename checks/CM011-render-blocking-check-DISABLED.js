var urllib = require('url');
var Q = require('q');
var Check = require('./master').Check;
var request = require('request');
var _ = require('lodash');

module.exports = function () {
	'use strict';

	return new Check('Sites should not use render-blocking JS in above-the-fold content.', 'CM011', {
		requires: ['crawler'],
		resultType: 'list',
		onlyOn: {
			roles: ['Developer', 'Digital Marketer'],
	    activities: []
		},
		homeChecked: false,
		internalChecked: false,
		PAGESPEED_API_KEY: 'AIzaSyC8u1ZSb80ApCdMZnTN_xJV7WS6cqmAoEo',
		PAGESPEED_URL: 'https://www.googleapis.com/pagespeedonline/v1/runPagespeed?url=',
		onPageCrawl: function (page, response, pagesCrawled) {
			var check = this;
			var deferred = Q.defer();
			var templateType = this.templateType(page);
			var query = page.urlData.search || '';

			// Skip processing if home + internal already checked
			if (this.homeChecked === true && this.internalChecked === true) {
				return false;
			}

			// Disable PageSpeed
			if (query.indexOf('?') > -1) {
				query += '&';
			} else {
				query += '?';
			}
			query += 'ModPagespeed=off';
			page.urlData.search = query;

			// Check if page should be reviewed
			if (this.shouldCheckPage(templateType) === true) {
				request.get(this.PAGESPEED_URL + escape(urllib.format(page.urlData)) +'&key=' + this.PAGESPEED_API_KEY, function (err, response, body) {
					check.parseResults(page, body, templateType);
					deferred.resolve();
				});
			} else {
				return false;
			}

			return deferred.promise;
		},
		applyVars: function (format, args) {
			return _.reduce(args, function (memo, arg, index) {
				return memo.replace('$' + (index + 1), arg.value);
			}, format);
		},
		parseResults: function (page, results, templateType) {
			var check = this;
			var renderBlocking;
			var scripts = [];

			// Parse JSON string
			try {

				results = JSON.parse(results).formattedResults.ruleResults;

			} catch (e) {

				console.log(e.toString());
				check.passed = false;
				return false;

			}

			// Check results for render blocking JS/CSS
			renderBlocking = results.MinimizeRenderBlockingResources;
			if (renderBlocking !== undefined && renderBlocking.ruleImpact > 0) {

				_.forEach(renderBlocking.urlBlocks, function (block, key) {
					// Omit all results other than render-blocking JS
					if (block.header.format.indexOf('Remove render-blocking JavaScript') < 0) {
						return false;
					}

					scripts = _.chain(block.urls)
						.filter(function (url, key) {
							// Exclude jQuery from render-blocking report
							if (
								url.result.args.length > 0 &&
								url.result.args[0].value.indexOf('/jquery.min.js') > -1
							) {
								return false;
							}

							return true;
						})
						.map(function (url, key) {
							// Replace variables in URL results
							return check.applyVars(url.result.format, url.result.args);
						}).value();
				});

				if (scripts.length > 0) {
					this.data.push({
						url: page.url,
						scripts: scripts
					});

					this.passed = false;
				}
			}
		},
		templateType: function (page) {
			if (page.isExternal === true) {
				return 'external';
			}

			// Check home page
			if (page.urlData.path === '/') {
				return 'home';
			}

			// Check internal page
			if (page.url.indexOf('Site-Map.shtml') > -1 || page.url.indexOf('Disclaimer.shtml') > -1) {
				return 'internal';
			}
		},
		shouldCheckPage: function (type) {
			switch(type) {
			case 'home':
				this.homeChecked = true;
				return true;
			case 'internal':
				this.internalChecked = true;
				return true;
			}

			return false;
		},
		onCrawlFinish: function (pagesCrawled) {
			// Mark as passed if not explicitly failed
			if (this.passed !== false) {
				this.passed = true;
			}
		}
	});
};