var fs = require('fs');
var pathlib = require('path');
var childProc = require('child_process');
var async = require('async');
var imageSize = require('image-size');
var Q = require('q');
var request = require('request');
var Check = require('./master').Check;

/**
  * Check #CM100: The live site matches the preview site.
  *
  * @return {Check} Returns a new Check - The live site matches the preview site (CM100)
  */
module.exports = function () {
  'use strict';

  return new Check('The live site matches the preview site.', 'CM100', {
    resultType: 'object',
    onlyOn: {
      roles: ['Developer', 'QA Member', 'Digital Marketer'],
      activities: [],
      types: ['internal']
    },
    THUMBNAIL_GEN_URL: 'http://fldev.int.thomson.com:8124/',
    VIEWPORT_WIDTHS: [1024, 768, 320],
    VIEWPORT_HEIGHT: 1500,
    screenshotDir: require('../server/config/environment').screenshotDir,
    /**
    * Runs the check.
    *
    * @method run
    * @param {String} url Site URL provided by Checkmaster
    * @param {Object} siteInfo Site info object
    * @return {Q.Promise} Returns a promise
    */
    run: function (url, siteInfo) {
      var check = this;
      var deferred = Q.defer();
      var paths = ['/'];

      // Quit early if no folder/live domain exist
      if (!siteInfo.previewDomain || !siteInfo.domain) {
        check.passed = true;
        deferred.resolve();
        return deferred.promise;
      }

      // Add /blog URL
      if (siteInfo.types.indexOf('slash_blog') > -1) {
        paths.push('/blog');
      }

      // Add FS Control internal page (hopefully there)
      if (siteInfo.types.indexOf('fscontrol') > -1) {
        paths.push('/CM/Custom/Site-Map.asp');
      }

      // Add Publisher internal page
      if (siteInfo.pages.siteMap && siteInfo.pages.siteMap.length > 0) {
        paths.push(siteInfo.pages.siteMap[0].path);
      }

      check.data = {
        previewURL: 'http://' + siteInfo.previewDomain,
        liveURL: 'http://' + siteInfo.domain,
        diffs: {}
      };

      // Fill in paths for images/diffs
      this.VIEWPORT_WIDTHS.forEach(function (width) {
        paths.forEach(function (path) {
          check.data.diffs[path + '--' + width.toString()] = {
            ratio: null,
            previewURL: check.data.previewURL + path,
            liveURL: check.data.liveURL + path,
            width: width,
            previewImage: check.getFileName(check.data.previewURL + path, width),
            liveImage: check.getFileName(check.data.liveURL + path, width),
            diffImage: check.getFileName(check.data.liveURL + path, width).replace(/\.png$/gi, '-diff.png')
          };
        });
      });

      check.getScreens().then(function () {
        // Pass check
        check.passed = true;

        var key;
        for (key in check.data.diffs) {
          var diff = check.data.diffs[key];
          if (diff.ratio !== 0) {
            check.needsFollowUp = true;
          }
        }

        deferred.resolve();
      });

      return deferred.promise;
    },
    /**
    * Generates a screenshot path for given URL and width.
    *
    * @method getScreenshotPath
    * @param {String} url A URL
    * @param {Number} width Width of viewport
    * @return {String} Returns the path to the image file based on the given URL & width
    */
    getScreenshotPath: function (url, width) {
      return pathlib.join(this.screenshotDir, this.getFileName(url, width));
    },
    /**
    * Generates a file name for given URL and width.
    *
    * @method getFileName
    * @param {String} url A URL
    * @param {Number} width Width of viewport
    * @return {String} Returns a file name based on the given URL & width
    */
    getFileName: function (url, width) {
      return url.replace(/^http:\/\//gi, '').replace(/\//gi, '!') + '-' + width + 'x' + this.VIEWPORT_HEIGHT + '.png';
    },
    /**
    * Retrives image size for a given file path
    *
    * @method imageSize
    * @param {String} path Path to the image file.
    * @return {String} Returns an object with "height" and "width" properties
    */
    imageSize: function (path) {
      return imageSize(path);
    },
    /**
    * Generates screenshots for each URL and viewport width.
    *
    * @method getScreens
    * @return {Q.Promise} Returns a promise
    */
    getScreens: function () {
      var check = this;
      var deferred = Q.defer();

      async.mapSeries(Object.keys(check.data.diffs), function (key, finish) {
        var diff = check.data.diffs[key];
        var previewURL = diff.previewURL.replace('http://', '');
        var liveURL = diff.liveURL.replace('http://', '');
        
        Q.all([
          check.getScreenShot(previewURL, diff.width),
          check.getScreenShot(liveURL, diff.width)
        ]).spread(function (previewError, liveError) {
          if (previewError) {
            diff.previewError = true;
          }
          if (liveError) {
            diff.liveError = true;
          }
          
          check
            // Make sure images are the same size
            .checkImageSizes(
              check.getScreenshotPath(previewURL, diff.width),
              check.getScreenshotPath(liveURL, diff.width)
            )
            // Compare the two screenshots
            .then(check.compareImages.bind(check, diff, key))
            .then(finish);
        });
      }, deferred.resolve);

      return deferred.promise;
    },
    /**
    * Generates screenshots for supplied URL and viewport width.
    *
    * @method getScreens
    * @param {String} url URL that needs screenshot.
    * @param {String} width Viewport width for screenshot.
    * @return {Q.Promise} Returns a promise
    */
    getScreenShot: function (url, width) {
      var check = this;
      var deferred = Q.defer();
      var error = false;
      var thumbnailGenURL = this.THUMBNAIL_GEN_URL + encodeURIComponent(url) + '@' + width + 'x' + check.VIEWPORT_HEIGHT;
      var req = request.get(thumbnailGenURL);

      // Save thumbnail to file
      req.pipe(fs.createWriteStream(check.getScreenshotPath(url, width)))
        .on('finish', function () {
          deferred.resolve(error);
        });

      req.on('response', function (response) {
        // Handle request errors
        if (response.statusCode !== 200) {
          console.error('Could not generate thumbnail: ' + response.statusCode);
          console.error(thumbnailGenURL);
          error = new Error('Bad status code: ' + response.statusCode);
        }
      });

      return deferred.promise;
    },
    /**
    * Checks two sizes and crops if they are not the same
    *
    * @method checkImageSizes
    * @param {String} firstImage Path to first image.
    * @param {String} secondImage Path to second image.
    * @return {Q.Promise} Returns a promise
    */
    checkImageSizes: function (firstImage, secondImage) {
      var deferred = Q.defer();
      var height;
      var imageToResize;
      var firstSize = this.imageSize(firstImage);
      var secondSize = this.imageSize(secondImage);

      // Check if heights are equal
      if (firstSize.height === secondSize.height) {
        deferred.resolve();
      } else {

        // Find larger size
        if (firstSize.height > secondSize.height) {
          imageToResize = secondImage;
          height = firstSize.height;
        } else {
          imageToResize = firstImage;
          height = secondSize.height;
        }
      
        // Resize image
        this.resize(imageToResize, firstSize.width, height).then(deferred.resolve);
      }

      return deferred.promise;
    },
    /**
    * Resizes a given image to the given width & height
    *
    * @method screenshot
    * @param {String} path Path to the image file
    * @param {Number} width The width to which the image will be resized
    * @param {Number} height The height to which the image will be resized
    * @return {Q.Promise} Returns a promise
    */
    resize: function (path, width, height) {
      var deferred = Q.defer();

      var proc = childProc.spawn('convert',
        [
          '-extent', width + 'x' + height,
          '-background', 'none',
          path,
          path
        ], {
          cwd: this.screenshotDir
        }
      );

      proc.stdout.on('data', function (data) {
        console.log(data.toString());
      });

      proc.stderr.on('data', function (data) {
        console.error(data.toString());
      });

      proc.on('close', function () {
        deferred.resolve();
      });

      return deferred.promise;
    },
    /**
    * Compares preview/live images
    *
    * @method compareImages
    * @param {Object} diff Object with information about diff.
    * @return {Q.Promise} Returns a promise
    */
    compareImages: function (diff) {
      var check = this;
      var deferred = Q.defer();
      var previewFile = this.getFileName(diff.previewURL, diff.width); 
      var liveFile = this.getFileName(diff.liveURL, diff.width);
      var result = '';

      var proc = childProc.spawn('compare',
        [
          '-metric', 'RMSE',
          '-dissimilarity-threshold', '1',
          previewFile,
          liveFile,
          liveFile.replace(/\.png$/gi, '-diff.png')
        ], {
          cwd: this.screenshotDir,
          stdio: ['ignore', 'pipe', 'pipe']
        });

      proc.stdout.on('data', function (data) {
        result += data.toString();
      });

      proc.stderr.on('data', function (data) {
        result += data.toString();
      });

      proc.on('close', function () {
        var ratio = result.replace(/.*\(([^\)]+)\).*/gi, '$1');

        // Check if replace worked
        if (ratio !== result) {
          // Convert to percentage
          ratio = parseInt(ratio * 100, 10);
        } else {
          // Notify user that the ratio was not found
          ratio = 'could not calculate ratio';
          console.error('CM100 compare utility: Could not find ratio in result.');
          console.error(result);
        }

        diff.ratio = ratio;
        deferred.resolve();
      });

      return deferred.promise;
    }
  });
};