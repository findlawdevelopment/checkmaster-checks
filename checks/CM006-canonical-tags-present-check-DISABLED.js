var _ = require('lodash');
var Check = require('./master').Check;
var Q = require('q');

module.exports = function () {
	'use strict';

	return new Check('Canonical tags should be used wherever necessary.', 'CM006', {
		resultType: 'list',
		requires: ['crawler'],
		onlyOn: {
			roles: ['Developer', 'Digital Marketer'],
			activities: [],
			types: ['internal']
		},
		onPageCrawl: function (page, response, pagesCrawled) {
			var deferred = Q.defer();
			// Determine if page should have a canonical tag
			var shouldHaveTag = this.shouldHaveTag(this.getSiteInfo(), page.url, page.type);
			var fail = false;

			if (page.dom()('link[rel="canonical"]').length > 0) {
				// Fail check if a canonical tag was found, but the page should not have a tag
				fail = shouldHaveTag.answer !== true;
			} else {
				// Fail check if a canonical tag was not found, but the page should have a tag
				fail = shouldHaveTag.answer !== false;
			}

			if (fail === true) {
				this.passed = false;
				this.data.push({
					url: page.url,
					reason: shouldHaveTag.reason
				});
			}

			deferred.resolve();
			return deferred.promise;
		},
		shouldHaveTag: function (siteInfo, url, type) {
			var types = siteInfo.types || [];
			
			var REASON_PREVIEW = 'The preview site should never have a canonical tag.';
			var REASON_BLOG_POSTS = 'All blog posts should have a canonical tag.';
			var REASON_NON_BLOG_POSTS = 'Only blog posts should have canonical tags.';
			var REASON_NON_UNIQUE = 'Non-unique pages should not have canonical tags.';
			var REASON_UNIQUE = 'All unique pages should have a canonical tag.';
			var REASON_NON_HTML = 'Non-HTML pages cannot have a canonical tag.';

			// Only check pages that are text/html
			if (type !== 'text/html') {
				return { answer: false, reason: REASON_NON_HTML };
			}

			// If /blog/ is found, consider it a blog
			if (url.match(/https?:\/\/[^\/]+\/blog\//i) !== null) {
				// Create types if it doesn't exist
				if (!types) {
					types = [];
				}
				types = types.concat('blog');
			} else {
				// If the site has a /blog/, but the /blog/ is not in the URL, we should
				// ignore the blog type logic (e.g. remove "blog" from the types array)
				// Note: if we don't do this, it will return false because the URL doesn't
				// match the /YYYY/MM/<page>.html rule defined in the blog type logic
				if (types.indexOf('slash_blog') > -1) {
					types.splice(types.indexOf('blog'), 1);
				}
			}

			// Follow rules by type
			if (types) {
				// Canonical tags should never be on a preview site
				if (types.indexOf('preview') > -1) {
					return { answer: false, reason: REASON_PREVIEW };
				}

				// Blog types
				if (types.indexOf('blog') > -1) {
					// If path ends with /YYYY/MM/<any-non-slash>.shtml
					//  or /blog/YYYY/MM/<any-non-slash>.shtml, it should have a tag
					if (/(?:\/blog)?\/\d{4}\/\d{2}\/[^\/]+\.shtml$/gi.test(url) === true) {
						return { answer: true, reason: REASON_BLOG_POSTS };
					} else {
						return { answer: false, reason: REASON_NON_BLOG_POSTS };
					}
				}
			}

			// Non-unique pages (determined by URL pattern) should not have a tag
			// - Resources
			// - Disclaimer
			// - Practice pages - TODO: figure out page type for PPs, add it to getSiteProps and remove this regex
			// - Pages that do not have the "custom page" page type (or anything that has a page type at this point)
			if (
				/\/Resources?(?:\.shtml|\/)$/i.test(url) === true ||
				/\/Disclaimer\.shtml$/i.test(url) === true ||
				/\/[^\/]+_PP(\/[^\/]*|.shtml)$/.test(url) === true ||
				/.*atom\.xml$/.test(url) === true ||
				this.checkPageIDExcludes(url, siteInfo.pages) === true
			) {
				return { answer: false, reason: REASON_NON_UNIQUE };
			}

			return { answer: true, reason: REASON_UNIQUE };
		},
		checkPageIDExcludes: function (url, pagesTypes) {
			// Loop through page types
			return _.find(pagesTypes, function (pageIDs) {

				// Loop through pageIDs
				return _.find(pageIDs, function (id) {
					// See if URL matches page ID
					// - .shtml
					// - / (ends with)
					return url.indexOf(id + '.shtml') > -1 || url.indexOf(id + '/', url.length - (id.length + 1)) > -1;
				}) !== undefined;

			}) !== undefined;
		},
		onCrawlFinish: function (pagesCrawled) {
			// Mark as failed if not explicitly passed
			if (this.passed !== false) {
				this.passed = true;
			}
		}
	});
};