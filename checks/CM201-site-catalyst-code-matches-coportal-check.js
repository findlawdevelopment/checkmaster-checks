var Q = require('q');
var Check = require('./master').Check;

module.exports = function () {
	'use strict';

	return new Check('Site Catalyst Report Suite ID matches the s_account on live website', 'CM201', {
		requires: ['crawler'],
		resultType: 'object',
		onlyOn: {
			roles: ['Developer', 'QA Member', 'Digital Marketer'],
			activities: [],
			types: ['live', 'internal']
		},
		onPageCrawl: function (page, response, pagesCrawled) {
			var deferred = Q.defer(),
					check = this;

			this.getSiteCatalystCode(page).then(deferred.resolve(page));

			return deferred.promise;
		},
		getSiteCatalystCode: function(page) {

			// console.log(page.html);

			var deferred = Q.defer(),
					check = this,
					$ = check.renderDom(page.html),
					comments,
					scripts;

			// Check for site catalyst comment node
			comments = $('body').contents();

			for (var comment in comments) {

				var siteCatalystComment = comments[comment]['data'];

				if (typeof siteCatalystComment === 'string') {

					siteCatalystComment = siteCatalystComment.trim();
				}
			
				if (siteCatalystComment === 'Site Catalyst code') {

					// Site catalyst code exists on the site so find the s_account
					continue;
				} else {

					// If site catalyst code does not exist then pass the check
					check.passed = true;
					deferred.resolve(page);
				}

			}

			// Check for s_account
			scripts = $('body script').contents();

			for (var code in scripts) {

				var data = scripts[code]['data'];

				if (typeof data === 'string') {

					var re = /.*s_account="([^"]*)"/,
							s_account = data.match(re);
			
					if (s_account && s_account[1]) {

						// console.log('\ns_account: ' + s_account[1]);

						if (check.getSiteInfo().reportSuiteID === s_account[1]) {

							deferred.resolve(page);
						} else {

							check.passed = false;
							deferred.reject(page);
						}
					}
				}
			}

			return deferred.promise;
		},
		onCrawlFinish: function (pagesCrawled) {
			
			// Mark as failed if not explicitly passed
			if (this.passed !== true) {
				this.passed = false;
			}
		}
	});
};