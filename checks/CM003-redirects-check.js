var Check = require('./master').Check;
var Q = require('q');

module.exports = function () {
	'use strict';

	return new Check('No same-domain redirects should be present on site.', 'CM003', {
		resultType: 'list',
		requires: ['crawler'],
		onlyOn: {
			roles: ['Developer', 'Digital Marketer'],
			activities: []
		},

		onPageCrawl: function (page) {
			var deferred = Q.defer();
			if (page.referrer && page.finalURL) {
				// Don't consider external page redirects as a problem
				if (page.isExternal === true) {
					return false;
				}

				// Add redirect to data
				this.data.push({
					url: page.url,
					redirectsTo: page.finalURL,
					referrers: [page.referrer]
				});

				// Mark check as failed
				this.passed = false;
			}
			deferred.resolve();
			return deferred.promise;
		},
		onCrawlFinish: function (pagesCrawled) {
			var self = this;

			// Update referrers now that all pages are available
			this.data = this.data.map(function (redirect) {
				return {
					url: redirect.url,
					redirectsTo: redirect.redirectsTo,
					referrers: self.referringPages(redirect.url, pagesCrawled)[0].referringPages
				};
			});

			// Mark as true if no redirects occur
			if (this.passed !== false) {
				this.passed = true;
			}
		}
	});
};