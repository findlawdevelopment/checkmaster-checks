var Check = require('./master').Check;
var Q = require('Q');

module.exports = function () {
	'use strict';

	return new Check('There should be no pages that require carryover.', 'CM009', {
		requires: ['crawler'],
		resultType: 'list',
		onlyOn: {
			roles: ['Developer', 'Digital Marketer'],
			activities: [],
			types: ['internal']
		},
		onPageCrawl: function (page, pagesCrawled) {
			var deferred = Q.defer();
			var $ = page.dom();
			var pageText = $('.content').text();
			var pageHTML = $('.content').html();
			var pageID = this.getPageID(page);
			var needsContent = false;
			var videos = this.getSiteInfo().pages.video;

			// if this is a video page, do not test it
			var isVideo = false;

			videos.forEach(function (videoID) {

				if (pageID === videoID) {
					isVideo = true;
				}

			});

			if (!isVideo) {
				// Check if "carry over" text is in the content
				needsContent = this.needsContent(pageText, pageHTML);
			}

			if ( needsContent !== false ) {
				// If carryover is needed, fail check
				this.passed = false;

				this.data.push({
					id: pageID,
					url: page.url,
					reason: needsContent.reason,
					carryover: needsContent.carryover
				});
			}
			deferred.resolve();
			return deferred.promise;
		},
		getPageID: function (page) {
			var pageID;

			// Set pageID to "Home" if path is "/"
			if (page.urlData.path === '/') {
				return 'Home';
			}

			// Strip everything but the last file/folder name (without an extension or trailing slash)
			pageID = page.url.replace(/.*\/([^\.\/]+)(?:\.[^\.\/]+|\/)$/, '$1');

			// Make sure pageID is valid
			if (pageID === page.url) {
				return 'PAGE-ID-NOT-FOUND';
			}

			return pageID;
		},
		needsContent: function (text, html) {
			// Check if content contains a URL and is less than 500 characters
			if (text.indexOf('http://') > -1 && text.length < 500) {
				return this.findCarryoverURL(html);
			}
			return false;
		},
		findCarryoverURL: function (html) {
			var carryoverURLs = html.match(/https?:\/\/[\w\d_\-\.\/\?\&;=+!%]+/gi);
			var carryoverURL;
			var reason;

			// Check if no carryover URLs are found
			if (carryoverURLs === null) {
				return {
					reason: 'Found page that requires carryover, but could not find carryover URL. Please check the page for a URL and/or follow up with someone who would be able to provide you with the URL.',
					carryover: ''
				};
			}

			// Check if multiple carryover URLs are found
			if (carryoverURLs.length > 1) {
				return {
					reason: 'Found multiple carryover URLs for one page. Please determine what carryover this page should have and/or follow up with someone who would be able to provide you with the URL.',
					carryover: carryoverURLs
				};
			}

			// Create empty array for carryover URLs
			if (this.carryoverURLs === undefined) {
				this.carryoverURLs = [];
			}

			carryoverURL = carryoverURLs[0];
			reason = 'Carryover URL Found';

			// Add URL to list if it's a new URL
			if (this.carryoverURLs.indexOf(carryoverURL) < 0) {
				this.carryoverURLs.push(carryoverURL);
			} else {
				// If the same carryover URL is found on multiple pages, update reason for
				// this record and the others that use the same carryover URL
				reason = 'Same carryover URL exists on multiple pages. Please determine which page should have the carryover and/or follow up with someone who would be able to provide you with the answer.';
				this.updateReasons(carryoverURL, reason);
			}

			return {
				reason: reason,
				carryover: carryoverURL
			};
		},
		updateReasons: function (carryoverURL, reason) {
			// Update other records that have this URL
			this.data = this.data.map(function (item) {
				if (item.carryover === carryoverURL) {
					item.reason = reason;
				}
				return item;
			});
		},
		onCrawlFinish: function (pagesCrawled) {
			if (this.data.length === 0) {
				// no data pages were pushed to data, meaning no failures
				this.passed = true;
			} else if (this.passed !== true) {
				// Mark as failed if not explicitly passed
				this.passed = false;
			}
		}
	});
};