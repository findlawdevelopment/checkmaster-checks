var Check = require('./master').Check;
var cheerio = require('cheerio');
var Q = require('q');
var request = require('request');
var _ = require('lodash');

module.exports = function () {
	'use strict';

	return new Check('The UK-specific fine print is present on the website', 'CM015', {
		resultType: 'list',
		onlyOn: {
			roles: ['Developer', 'Digital Marketer'],
			activities: [],
			countries: ['uk'],
			types: ['internal']
		},
		returnHTML: function(url) {

			var that = this,
					deferred = Q.defer();

			request.get(url, function (error, response, body) {

				if (!error && response.statusCode === 200) {
					
					that.findLinkSource(body);
					deferred.resolve();

				} else {

					deferred.reject(new Error('There was an error during the request or the status code was not 200.'));

				}

			});

			return deferred.promise;

		},
		findLinkSource: function(html) {

			var $ = cheerio.load(html),
					check = this,
					lawyerMarketingUK = $('#branding a[href="http://www.lawyermarketinguk.co.uk"], .branding a[href="http://www.lawyermarketinguk.co.uk"]'),
					findLawUK = $('#branding a[href="http://www.findlaw.co.uk/"], .branding a[href="http://www.findlaw.co.uk/"]'),
					cookieVerification = $('#branding script[src="/includes/cookieVerification.js"], .branding script[src="/includes/cookieVerification.js"]'),
					deferred = Q.defer();

			if (lawyerMarketingUK.length === 0 || findLawUK.length === 0) {
				
				check.data.push({
					message: 'The FindLaw UK branding include is missing',
					resolve: 'Add <!--#include virtual="/includes/branding_UK.shtml" --> within the "branding" paragraph.',
					needsFollowUp: false
				});

			}

			if (cookieVerification.length === 0) {

				check.data.push({
					message: 'The cookie verification script is not on the website',
					resolve: 'Check with the account manager to see if this should be omitted, if not, Add <!--#include virtual="/includes/branding_UK_without_cookies.shtml" --> within the "branding" paragraph.',
					needsFollowUp: true
				});

			}

			return deferred.promise;

		},
		run: function (page, siteInfo) {
			var check = this,
					deferred = Q.defer();

			this.returnHTML(page.url).then(function() {

				// loop through check.data to find any nonFailures
				var warning = _.pluck(check.data, 'needsFollowUp'),
						needsFollowUp = warning.indexOf(true);

				// pass the check if the array
				if ((check.data.length === 0) && (needsFollowUp === -1)) {

					check.passed = true;
					check.data.push({
						message: 'The correct UK-specific fine print is present on the website.',
						resolve: null,
						needsFollowUp: true
					});

				} else {

					check.passed = false;

				}

				if (needsFollowUp === 0) {
					
					check.passed = true;
					check.needsFollowUp = true;
					
				}

				deferred.resolve();
			});

			return deferred.promise;

		}
	});
};