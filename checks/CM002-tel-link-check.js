var Check = require('./master').Check;
var Q = require('q');

module.exports = function () {
	'use strict';

	return new Check('No tel: links exist.', 'CM002', {
		requires: ['crawler'],
		resultType: 'list',
		onlyOn: {
			roles: ['Developer', 'Digital Marketer'],
			activities: []
		},
		passFail: [],

		testForTelLink: function (ele) {
			/*
				tests an element to see if it has a link with tel:

				ele - cheerio object of the link
				
				var ele = '<a href="tel:" />'
				testForTelLink(ele)
				=> true
			*/


			var href = ele.attr('href') || "";
			// If there are any tel: links on the page, return a number more than -1
			return href.indexOf('tel:') > -1;
		},

		findTelLink: function ($) {
			/*
				Finds all links on the page

				$ - cheerio object of the page.html

				findTelLink($)
				=> '<a href="tel:">'
			*/

			var links = [],
				check = this,
				findTelLink = "";


			$('a').each(function (i, ele) {
				// Set the cherio el equal to all the content on the page so we can grab it all and do stuff to it
				var $el = $(ele);

				// If there are any tel: links, push them to the cherio element 'el'
				if (check.testForTelLink($el)) {
					links.push($el);
				}

			});

			return links;
		},

		onPageCrawl: function (page) {
			var $ = this.renderDom(page.html),
					links = this.findTelLink($),
					tel = '',
					fail = links.length > 0,
					deferred = Q.defer();

			//If there are any tel: links on the page
			if (fail) {
				
				// Automatically set to fail
				this.passed = false;

				var allLinks = [];

				// Underscore object with these 
				links.forEach(function (link){
					var attrs = {};
					attrs.href = link.attr('href');
					attrs.text = link.text();
					//pushing href and text allLinks so we can use it down below
					allLinks.push(attrs);
				});

				this.data.push({
					url: page.url,
					reason: 'There is a link with tel: on the site',
					allLinks: allLinks
				});
			}
			deferred.resolve();
			return deferred.promise;
		},
		onCrawlFinish: function () {
			// Mark as failed if not explicitly passed
			if (this.passed !== false) {
				this.passed = true;
			}
		}
	});
};