var Check = require('./master').Check;
var Q = require('q');
var request = require('request');

module.exports = function () {
	'use strict';

	return new Check('The Google Webmaster Tools verification matches the live website', 'CM012', {
		onlyOn: {
			activities: [],
			types: ['preview', 'internal']
		},
		run: function (page, siteInfo) {

			var check = this,
					deferred = Q.defer();

			Q.all([
				check.getPreviewMeta(page.url),
				check.getLiveMeta(page.url, siteInfo)
			]).spread(function(preview, live){
					if (preview === live && check.passed !== false) {

						check.passed = true;
						deferred.resolve();

					} else {

						check.passed = false;
						deferred.resolve();
						
					}
					
				})
			;

			return deferred.promise;

		},
		getPreviewMeta: function (url) {
			var deferred = Q.defer();

			this.getHTML(url).then(this.searchForMetaTag).then(function (meta) {

				deferred.resolve(meta);

			});

			return deferred.promise;
		},
		getLiveMeta: function (url, siteInfo) {
			var deferred = Q.defer();

			this.checkForLiveSite(url, siteInfo).then(this.getHTML).then(this.searchForMetaTag).then(function (meta) {

				deferred.resolve(meta);

			});

			return deferred.promise;
		},
		checkForLiveSite: function(url, siteInfo) {

			var deferred = Q.defer();
			
			this.setSiteData(url, siteInfo);

			if (this.getSiteInfo().domain) {

					url = this.getSiteInfo().domain;

					// Add http protocol since the service omits it

					url = 'http://' + url;

					deferred.resolve(url);

			} else {

				// TODO: needsFollowUp condition if domain is not defined

				this.passed = false;

				deferred.resolve(url);

			}

			return deferred.promise;
			
		},
		getHTML: function(url) {

			var deferred = Q.defer();

			request.get(url, function (error, response, body) {

				if (!error && response.statusCode === 200) {

					deferred.resolve(body);
					
				} else {

					deferred.resolve('');

				}

			});

			return deferred.promise;

		},
		searchForMetaTag: function(html) {
			var deferred = Q.defer();
			var cheerio = require('cheerio');

			// Load the body of the web page via Cheerio and find meta tag
			var $ = cheerio.load(html),
					metaTag = $('html').find('meta[name=google-site-verification]');
			
			if (metaTag.length > 0) {

				deferred.resolve(metaTag[0].attribs.content);

			} else {

				deferred.resolve();

			}

			return deferred.promise;

		}
	});
};