/* jshint strict: true */

var Q = require('q');
var request = require('request');
var _ = require('lodash');
var Check = require('./master').Check;

module.exports = function () {
	"use strict";

	// 1. this check should report any test posts on the live blog

	return new Check('The home page of the blog displays test posts only on the preview URL.', '342', {
		resultType: 'object',
		requires: ['crawler'],
		onlyOn: {
			roles: ['Developer', 'QA Member'],
			activities: [],
			types: ['blog', 'live', 'internal']
		},
		TEST_TAGS: ['Test Comments Disabled', 'Test Extended Post', 'Test G+', 'Test List Post', 'Test Long Post', 'Test Old Post', 'Test Related1', 'Test Related2', 'Test Related3', 'Test Related4', 'Test Related5', 'Test Tag'],
		TEST_CATEGORIES: ['Test Category'],

		onPageCrawl: function (page, response, pagesCrawled) {
			var check = this,
				passed = true,
				deferred = Q.defer(),
				isBlogPage = page.url.indexOf(this.getBlogURLs()) > -1;

			if (!Array.isArray(check.data.tags)) {
				check.data.tags = [];
				check.data.titles = [];
				check.data.categories = [];
			}

			if (isBlogPage) {

				var $ = page.dom(), check = this, tagMatch = [], catMatch = [], pageTitle = '';

				if ($('body.blog-entry').length > 0) {
					var pageTitle = check.getPageTitle($);
				}

				var tags = check.getTextFromList($, '#tagCloud'),
						categories = check.getTextFromList($, '#categories');

				categories = check.removeNumberText(categories);

				tagMatch = _.intersection(check.TEST_TAGS, tags);
				catMatch = _.intersection(check.TEST_CATEGORIES, categories);

				if (pageTitle.match(/^test/i)) {
					check.data.titles.push({
						url: page.url,
						title: pageTitle,
						message: 'The following title might be a test post: '
					});

					check.needsFollowUp = true;

				}


				if (tagMatch.length > 0) {
					var single = catMatch.length > 1;

					check.passed = false;
					check.data.tags.push({
						url: page.url,
						tags: tagMatch,
						message: 'The following tag' + single? ' is' : 's are' + ' related to test-posts: '
					});

					check.passed = false;

				}


				if (catMatch.length > 0) {
					single = catMatch.length > 1;

					check.passed = false;
					check.data.categories.push({
						url: page.url,
						categories: catMatch,
						message: 'The following categor' + single? 'y is' : 'ies are' + ' related to test-posts: '
					});

					check.passed = false;

				}

			}
		
			deferred.resolve();

			return deferred.promise;

		},

		onCrawlFinish: function (pagesCrawled) {

			if (this.passed !== false && this.needsFollowUp !== true) {

				this.passed = true;

			}

		},

		getTextFromList: function ($, selector) {

			var tags = [];

			if (!selector) { console.error('please pass in a selector'); return; }

			$(selector).find('li').each(function (i, ele) {

				tags[i] = $(this).text();

			});

			return tags;

		},

		removeNumberText: function (arr) {
			var tmp = [];

			arr.forEach(function (cat, i, arr) {

				tmp.push(cat.replace(/\s\(\d*\)/, ''));

			});

			return tmp;

		},

		getPageTitle: function ($) {

			return $('#pageTitle, .page-title').first().text();

		}
	});
};