var _ = require('underscore');
var winston = require('winston');
var Check = require('./master').Check;

module.exports = function () {
	return new Check('No errors occurred during Checkmaster crawl.', 'CM001', {
		requires: ['crawler'],
		resultType: 'list',
		onlyOn: {
			roles: ['Developer'],
			activities: [],
		},
		onPageCrawl: function () {},
		onError: function (page, response, pagesCrawled) {
			var errorMessage = page.error.message;

			if (
				// ECONNREFUSED errors are not errors (as part of this check)
				page.error.code === 'ECONNREFUSED' ||
				// ECONNRESET errors are not errors (as part of this check)
				page.error.code === 'ECONNRESET' ||
				// ENOTFOUND errors are not errors (as part of this check)
				page.error.code === 'ENOTFOUND' ||
				// ENOTFOUND errors are not errors (as part of this check)
				page.error.code === 'EADDRINFO' ||
				// Timeouts are not errors (as part of this check)
				page.error.code === 'ETIMEDOUT' ||
				// Bad status codes are not errors (as part of this check)
				page.error.code === 'STATUS CODE ERROR' ||
				// jQuery problems are not errors (as part of this check)
				errorMessage.indexOf('DOMNodeInsertedIntoDocument') > -1 ||
				// Redirect loops are not errors (as part of this check)
				errorMessage.indexOf('Exceeded maxRedirects') > -1 ||
				// Invalid URIs are not errors (as part of this check)
				errorMessage.indexOf('Invalid URI') > -1 ||
				// SSL connection errors are not errors (as part of this check)
				errorMessage.indexOf('SSL23_GET_SERVER_HELLO:unknown protocol') > -1 ||
				// Protocol support errors are not errors (as part of this check)
				(
					errorMessage.indexOf('Protocol:') > -1 && errorMessage.indexOf(': not supported.') > -1
				)
			) {
				return false;
			}

			// Add URL to list of error pages
			this.data.push({
				url: page.url,
				error: errorMessage,
				referringPages: page.referrer
			});

			// Formatted console error message
			winston.error('\n/====================\n\033[1m CM001\033[0m\n URL:      %s\n Error:    %s\n Referrer: %s\n\\====================', page.url, errorMessage, page.referrer);

			// Fail test
			this.passed = false;
		},
		onCrawlFinish: function (pagesCrawled) {
			var self = this;

			if (this.passed !== false) {
				// If no errors occur, this is green
				this.passed = true;
			}
		}
	});
};