var async = require('async');
var libxmljs = require('libxmljs');
var Mustache = require('mustache');
var Q = require('q');
var request = require('request');
var winston = require('winston');
var _ = require('lodash');

var Check = require('./master').Check;

var gfpConstants = require('./utils/getFocusPages').constants;

module.exports = function () {
  'use strict';

  return new Check('No special characters appear in the narrative.', '364', {
    resultType: 'object',
    onlyOn: {
      roles: ['QA Member', 'Directory'],
    },
    types: ['internal'],
    ERROR_DETAILS: '\n/====================\n DT Item #364\n URL:   {{{url}}}\n Error: {{error}}\n\\====================',
    UNWANTED_TEXT: 'Unwanted text found in narrative.',
    run: function (page, siteInfo) {
      var check = this;
      this.deferred = Q.defer();

      // Always pass
      this.passed = true;

      siteInfo.getFocusPages().then(function (results) {
        if (results.error !== gfpConstants.NO_FOCUS_PAGES) {
          check.needsFollowUp = true;
        } else {
          results.nonFatal = true;
        }

        check.setData(results)
          .then(check.checkForErrors.bind(check))
          .then(check.deferred.resolve);
      });

      return this.deferred.promise;
    },
    setData: function (results) {
      // Note I structured it this way to reduce the size of the data sent to the front end
      // and to avoid adding new keys that are later added to the getFocusPages utility
      var check = this;
      var deferred = Q.defer();

      function filterKeys(obj, filter) {
        var cleanObj = {};

        // Only add properties that are in the filter an exist in the original object
        _.forEach(filter, function (prop) {
          if (obj.hasOwnProperty(prop) === true) {
            cleanObj[prop] = obj[prop];
          }
        });

        return cleanObj;
      }

      // Merge and filter result props
      this.data = filterKeys(results, [
        'error',
        'url',
        'nonFatal',
        'focusPages'
      ]);

      // Merge and filter focus page props
      if (results.focusPages) {
        _.forEach(results.focusPages, function (focusPage, i) {
          check.data.focusPages[i] = filterKeys(focusPage, [
            'title',
            'narrative',
            'url',
            'error'
          ]);
        });
      }

      deferred.resolve();
      return deferred.promise;
    },
    checkForErrors: function () {
      var check = this;
      var deferred = Q.defer();

      // Loop through each article and determine if they have any bad code
      if (this.data.focusPages && this.data.focusPages.length > 0) {
        async.mapSeries(this.data.focusPages, function (focusPage, finish) {
          check.hasBadCode(focusPage).then(finish);
        }, deferred.resolve);
      } else {
        deferred.resolve();
      }

      return deferred.promise;
    },
    hasBadCode: function (focusPage) {
      var check = this;
      var deferred = Q.defer();
      var issues = [];
      var narrative = focusPage.narrative;

      var CASE_SEARCH = /\[[A-Z\s]*\]/g;
      var NO_CASE_SEARCH = /\b(?:goes here|note to)\b/gi;

      function addToIssues(searchResults, description) {
        _.forEach(searchResults, function (textMatch, index) {
          issues.push({
            found: textMatch,
            context: narrative.substring(index - 50, textMatch.length + 50),
            description: description
          });
        });
      }

      // Case insensitive search
      var noCaseSearch = NO_CASE_SEARCH.exec(narrative);
      if (noCaseSearch !== null) {
        addToIssues(noCaseSearch, this.UNWANTED_TEXT);
      }

      // Case sensitive search
      var caseSearch = CASE_SEARCH.exec(narrative);
      if (caseSearch !== null) {
        addToIssues(caseSearch, this.UNWANTED_TEXT);
      }

      // Grammatical errors
      // Delay grammar check
      setTimeout(function () {
        check.grammarCheck(focusPage, issues).then(finish);
      }, 500);

      function finish() {
        if (issues.length > 0) {
          focusPage.issues = issues;
        }
        deferred.resolve();
      }

      return deferred.promise;
    },
    grammarCheck: function (focusPage, issues) {
      var check = this;
      var deferred = Q.defer();
      var narrative = focusPage.narrative;

      // We only want text, use cheerio to convert HTML to text
      var narrativeText = check.renderDom('<div>' + narrative + '</div>').text();

      // Use: http://service.afterthedeadline.com/
      request.post('http://service.afterthedeadline.com/checkGrammar', {
        form: {
          data: narrativeText
        }
      }, function (err, response, xml) {
        var xmlDoc;

        if (err) {
          winston.error(Mustache.render(check.ERROR_DETAILS, {
            url: focusPage.url,
            error: err.message
          }));
        }

        if (response.statusCode !== 200) {
          winston.error(Mustache.render(check.ERROR_DETAILS, {
            url: focusPage.url,
            error: 'Bad status code when requesting grammar check: ' + response.statusCode
          }));
        }

        try {
          xmlDoc = libxmljs.parseXml(xml);
        } catch (err) {
          winston.error(Mustache.render(check.ERROR_DETAILS, {
            url: focusPage.url,
            error: 'Could not load grammar XML - ' + err.message
          }));
          winston.error(xml);
        }

        if (xmlDoc) {
          xmlDoc.root().find('//error').forEach(function (grammarError) {
            var string = grammarError.find('//string')[0].text();
            var description = grammarError.find('//description')[0].text();
            var precontext = grammarError.find('//precontext')[0].text();

            issues.push({
              found: string,
              description: description,
              context: precontext + ' ' + string
            });
          });
        }

        deferred.resolve();
        
      });

      return deferred.promise;
    }
  });
};