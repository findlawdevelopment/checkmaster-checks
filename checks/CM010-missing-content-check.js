var Check = require('./master').Check;
var Q = require('q');

module.exports = function () {
	'use strict';

	return new Check('No pages are missing, or have very little content.', 'CM010', {
		resultType: 'list',
		requires: ['crawler'],
		onlyOn: {
			roles: ['Digital Marketer'],
			activities: [],
			types: ['internal']
		},
		CONTENT_SELECTOR: '.content,#content,content',
		MAP_SELECTOR: 'iframe',
		ANCHOR_SELECTOR: 'a',
		FORM_SELECTOR: 'form',
		OBJECT_SELECTOR: 'object',
		onPageCrawl: function (page) {
			var $ = page.dom();
			var pageText = $(this.CONTENT_SELECTOR).text().replace(/(?:^\s+|\s+$)/gi, ''); // remove starting/trailing white space
			var deferred = Q.defer();

			if (pageText === '' || pageText.length < 500) {

				// Check if page is excluded
				if (!this.isExcluded(page, $)) {					

					if ($('.content').children('ul').length === 0) {

						// If content is missing, fail check
						this.needsFollowUp = true;

						var instance = {
							url: page.url,
							message: 'Page has very little content'
						};

						if (pageText.length === 0) {
							instance.message = 'Page has no content';
							this.passed = false;
						}

						// Add to data
						this.data.push(instance);
					}
				}
			}

			deferred.resolve();
			return deferred.promise;
		},
		getPageID: function (url) {
			return url.replace(/.*\/([^\.\/]+)(?:\.[^\.\/]+|\/)$/, '$1');
		},
		isExcluded: function (page, $) {
			var anchors;
			var iframe;
			var pageID = this.getPageID(page.url);
			var pageText = $(this.CONTENT_SELECTOR).text();
			var disclaimerPages = this.getSiteInfo().pages.disclaimer;

			/*
			| URL/header level excludes
			*/

			// Accomodate sites that use a custom disclaimer page
			if (pageID.match(/disclaimer|denegacion/gi) !== null) {
				return true;
			}

			// Exclude if pageText includes a URL
			if (pageText.match(/\w+:\/\//gi) !== null) {
				return true;
			}

			// Exclude if page is the Disclaimer page
			// - if the Disclaimer page ID matches the page's ID, exclude it
			if (disclaimerPages && disclaimerPages.indexOf(pageID) > -1) {
				return true;
			}

			// ignore non-text/html resources
			if (page.type !== 'text/html') {
				return true;
			}

			// Exclude if page is the atom.xml file
			if (page.url.indexOf('atom.xml', page.url.length - 8) > -1) {
				return true;
			}

			/*
			| Content-level excludes
			*/

			// Exclude if a map iframe is found (when it's an office location page)
			iframe = $(this.CONTENT_SELECTOR).find(this.MAP_SELECTOR);
			if (iframe.length > 0 && typeof iframe.attr('src') === 'string' && iframe.attr('src').indexOf('maps.google.com') > -1) {
				return true;
			}

			// Exclude if there are at least two <a> tags in the content area
			anchors = $(this.CONTENT_SELECTOR).find(this.ANCHOR_SELECTOR);
			if (anchors.length > 1) {
				return true;
			}

			if (
				// Exclude if the page has a form
				$(this.CONTENT_SELECTOR).find(this.FORM_SELECTOR).length > 0 ||
				// Exclude if the page contains an <object>
				$(this.CONTENT_SELECTOR).find(this.OBJECT_SELECTOR).length > 0
			) {
				return true;
			}

			return false;
		},
		onCrawlFinish: function () {
			if (this.passed !== false) {
				// Mark as failed if not explicitly passed
				this.passed = true;
			}
		}
	});
};
