var Check = require('./master').Check;
var Q = require('q');

module.exports = function () {
	'use strict';

	return new Check('A block of geographical information is present on the site.', '601', {
		requires: ['crawler'],
		resultType: 'list',
		onlyOn: {
			roles: ['Developer', 'QA Member', 'Digital Marketer'],
			activities: [],
			types: ['internal']
		},
		GEO_SELECTORS: '.geography,#geographicalFooter',
		onPageCrawl: function (page) {
			var deferred = Q.defer();

			var $ = page.dom();
			var $geos = $(this.GEO_SELECTORS);
			var geoContent = $geos.text().replace(/(?:^\s*|\s*$)/, '');

			if (page.type === 'text/html') {
				if (geoContent === '') {
					this.needsFollowUp = true;
					this.data.push({
						isMissing: null,
						url: page.url
					});
				}
			}

			deferred.resolve();
			return deferred.promise;
		},
		onCrawlFinish: function () {
			// Mark as passed if not explicitly failed
			if (this.passed !== false) {
				this.passed = true;
			}
		}
	});
};