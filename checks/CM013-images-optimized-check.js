var fs = require('fs');
var pathlib = require('path');
var childProcess = require('child_process');
var urllib = require('url');
var async = require('async');
var request = require('request');
var Q = require('q');
var Check = require('./master').Check;

module.exports = function () {
    'use strict';

    return new Check('All images are optimized.', 'CM013', {
        requires: ['crawler'],
        resultType: 'object',
        onlyOn: {
            roles: ['Developer', 'Digital Marketer'],
            activities: [],
        },
        onPageCrawl: function (page) {
            var check = this;
            var images = page.html.match(/src="[^"]+\.(png|jpeg|jpg|gif)"/gi);
            var deferred = Q.defer();

            if (images === null) {
                deferred.resolve();
            } else {
                async.map(images, function (image, finish) {
                    image = image.substring(0, image.length - 1).replace('src="', '');
                    image = urllib.resolve(page.url, image);

                    // Skip external images
                    if (image.indexOf(page.urlData.protocol + '//' + page.urlData.hostname) < 0)  {
                        finish();
                        return;
                    }

                    if (check.data.hasOwnProperty(image) === false) {
                        check.downloadImage(image, finish);
                    } else {
                        finish();
                    }
                }, function () {

                    var src;

                    for (src in check.data) {
                        var image = check.data[src];
                        if (image.savings >= 10) {
                            check.passed = false;
                        }
                    }

                    deferred.resolve();
                });
            }

            return deferred.promise;
        },
        downloadImage: function (image, finish) {
            var check = this;
            var filename = image.replace(/.*\//gi, '');
            var tmpImage = pathlib.join(__dirname, 'tmp', 'CM013-' + (new Date().getTime()) + '_' + filename);

            check.data[image] = {};

            var reqObj = request.get(image);

            reqObj.pipe(fs.createWriteStream(tmpImage))
                .on('close', function () {
                    var statusCode = reqObj.req.res.statusCode;

                    if (statusCode !== 200) {
                        check.data[image].error = 'Image could not be downloaded: (' + statusCode + ' status code)';
                        check.removeImage(tmpImage);
                        finish();
                        return;
                    }

                    check.optimizeImage(image, tmpImage, finish);
                });
        },
        optimizeImage: function (image, tmpImage, finish) {
            var check = this;
            var ext = image.replace(/.*\.(png|jpeg|jpg|gif)$/i, '$1');

            fs.stat(tmpImage, function (err, stats) {
                // Save initial size
                check.data[image].size = stats.size;

                // Optimize JPEGs
                if (ext === 'jpg' || ext === 'jpeg') {
                    check.optimizeJPEG(image, tmpImage, finish);
                    return;
                }

                // Optimize PNGs
                if (ext === 'png') {
                    check.optimizePNG(image, tmpImage, finish);
                    return;
                }

                // Optimize GIFs
                if (ext === 'gif') {
                    check.optimizeGIF(image, tmpImage, finish);
                    return;
                }

                finish();
            });
        },
        optimizeJPEG: function (image, tmpImage, finish) {
            var check = this;
            var child = childProcess.spawn('jpegoptim', ['-n', tmpImage]);
            
            child.stderr.on('data', function (data) {
                console.error(data.toString());
            });

            child.stdout.on('data', function (data) {
                data = data.toString();

                if (data.indexOf(', optimized') > -1) {
                    check.data[image].savings = parseInt(data.replace(/.*\(([^\)]+)%\), optimized./gi, '$1'), 10);
                }

                if (data.indexOf(', skipped') > -1) {
                    check.data[image].savings = 0;
                }
            });

            child.on('exit', function () {
                check.removeImage(tmpImage);
                finish();
            });
        },
        optimizePNG: function (image, tmpImage, finish) {
            var check = this;
            var child = childProcess.spawn('pngcrush', ['-reduce', tmpImage, tmpImage + '.out'], { stdio: ['ignore', 'pipe', 'pipe'] });
            
            child.stderr.on('data', handleOutput);
            child.stdout.on('data', handleOutput);

            function handleOutput (data) {
                data = data.toString();
                if (data.indexOf('filesize reduction)') > -1) {
                    check.data[image].savings = parseInt(data.replace(/[\s\S]*\(([^%]+)% filesize reduction\)[\s\S]*/gi, '$1'), 10);
                }

                if (data.indexOf('(no filesize change)') > -1) {
                    check.data[image].savings = 0;
                }
            }

            child.on('exit', function () {
                check.removeImage(tmpImage);
                check.removeImage(tmpImage + '.out');
                finish();
            });
        },
        optimizeGIF: function (image, tmpImage, finish) {
            var check = this;
            var child = childProcess.spawn('gifsicle', ['--optimize', tmpImage]);
            var tmpOutFile = fs.createWriteStream(tmpImage + '.out', { flags: 'a' });
            var writingDone = false;
            var compressionDone = false;

            child.stdout.pipe(tmpOutFile);

            child.stderr.on('data', function (data) {
                data = data.toString();
                console.error(data);

                if (data.indexOf('file not in GIF format') > -1) {
                    check.data[image].error = 'Invalid GIF file.';
                }
            });

            child.on('exit', function () {
                compressionDone = true;
                if (writingDone === true) {
                    done();
                }
            });

            tmpOutFile.on('finish', function () {
                writingDone = true;
                if (compressionDone === true) {
                    done();
                }
            });

            function done() {
                fs.stat(tmpImage + '.out', function (err, stats) {
                    check.data[image].savings = parseInt((1 - (stats.size / check.data[image].size)) * 100, 10);
                    check.removeImage(tmpImage);
                    check.removeImage(tmpImage + '.out');
                    finish();
                });
            }
        },
        removeImage: function (path) {
          fs.exists(path, function (exists) {
            if (exists) fs.unlink(path);
          });
        },
        onCrawlFinish: function () {
            // Mark as failed if not explicitly passed
            if (this.passed !== false) {
                this.passed = true;
            }
        }
    });
};