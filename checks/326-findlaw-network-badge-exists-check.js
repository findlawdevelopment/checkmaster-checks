var urllib = require('url');
var Q = require('q');
var jsdom = require('jsdom');
var Check = require('./master').Check;

module.exports = function () {
	'use strict';

	/*

	Success criteria:

	1. Link must point to "http://legalblogs.findlaw.com/"
	2. Image source must be one of the following:
		- http://video-transcripts.findlaw.com/includes/findlaw-blog-network.gif
		- http://video-transcripts.findlaw.com/includes/findlaw-blog-network-dark.gif
		- http://video-transcripts.findlaw.com/includes/findlaw-blog-network-transparent.png
		- if it's not one of the URLs, the user should be prompted as to whether or not the image is a FindLaw Network badge
		- FUTURE RELEASE: check background URLs
	3. If no image is found, this will fail

	*/

	return new Check('The FindLaw network badge exists and links to http://legalblogs.findlaw.com/.', '326', {
		resultType: 'list',
		onlyOn: {
			roles: ['Developer', 'QA Member'],
			activities: [],
			types: ['blog', 'internal']
		},

		getSrc: function (link) {
			/*
				Finds the src of the image used for the given link.
				First checks if there's a nested image
				Then looks for a bg image on the link

				link - jQuery object of an HTML link

				getSrc($('<a href="" style="background: url(some/image.jpg)"></a>'))
				=>"some/image.jpg"

				getSrc($('<a href=""><img src="some/image.jpg" /></a>'))
				=>"some/image.jpg"

				getSrc($('<a href=""></a>'))
				=>false

			*/

			var img = link.find('img');

			if(img.length > 0) {

				return img.attr('src') || '';

			}

			var bg = link.css('background-image').replace(/^url|[\(|\)]/g, '');

			if(bg.length > 0) {

				return bg;

			}

			return '';

		},

		run: function (page, siteInfo) {
			var check = this;
			var deferred = Q.defer();
			var blogUrl = this.getBlogURLs()[0];

			jsdom.env(
				blogUrl,
				['http://code.jquery.com/jquery-1.11.0.min.js'],
				function (err, window) {

					if (!err) {

						var $ = window.$;

						var validImageSRCs = [
								'http://video-transcripts.findlaw.com/includes/findlaw-blog-network.gif',
								'http://video-transcripts.findlaw.com/includes/findlaw-blog-network-dark.gif',
								'http://video-transcripts.findlaw.com/includes/findlaw-blog-network-transparent.png'
							];

						// Check for badge link
						var badgeLink = $('a[href="http://legalblogs.findlaw.com/"]');

						if (badgeLink.length === 0) {
							check.data = {
								message: 'FindLaw Network Badge link is missing',
								url: blogUrl
							};
							check.passed = false;
							deferred.resolve();

							return false;

						}

						var src = check.getSrc(badgeLink);
						
						if (src === '') {
							check.data = {
								message: 'FindLaw Network Badge image is missing',
								url: blogUrl
							};
							check.passed = false;
							deferred.resolve();

							return false;

						}

						var badgeImageURL = validImageSRCs.indexOf(src);

						if (badgeImageURL === -1) {

							check.needsFollowUp = true;

							check.data = {
								message: 'FindLaw Network Badge image is missing',
								url: blogUrl,
								src: urllib.resolve(page.url, src),
								nonFailure: true
							};
							check.passed = true;
							deferred.resolve();

							return false;

						}

						check.passed = true;
						deferred.resolve();
						
						return false;

					} else {

						check.data = {
							error: 'An error occured when connecting to: ' + blogUrl
						};
						deferred.reject();

					}
				}

			);

			return deferred.promise;
		}
	});
};