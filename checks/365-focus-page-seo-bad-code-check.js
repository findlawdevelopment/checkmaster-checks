var _ = require('lodash');
var async = require('async');
var Check = require('./master').Check;
var gfpConstants = require('../core/utils/getFocusPages').constants;
var Mustache = require('mustache');
var Q = require('q');
var request = require('request');

module.exports = function () {
	'use strict';

	return new Check('No special characters appear in the SEO elements of a Focus Page/Premium Profile', '365', {
		resultType: 'object',
    onlyOn: {
      roles: ['QA Member', 'Directory'],
    }
    types: ['internal'],
    // TODO: Once we figure out what these issues are then we can check against them
    // PLACEHOLDER_TEXT: 'Unwanted placeholder text found on the focus page(s)/premium profile(s).',
    // LATIN_TEXT: 'Unwanted latin text found on the focus page(s)/premium profile(s).', 
    // INTERNAL_COMMENT_TEXT: 'Unwanted internal comment text found on the focus page(s)/premium profile(s).',
    // EXTRA_PUNCTUATION: 'Additional punctuation found on the focus page(s)/premium profile(s).',
		run: function(page, siteInfo) {
			var check = this;
      this.deferred = Q.defer();

      // Always pass
      this.passed = true;
					
			siteInfo.getFocusPages().then(function (results) {
			
        if (results.error !== gfpConstants.NO_FOCUS_PAGES) {
          check.needsFollowUp = true;
        } else {
          results.nonFatal = true;
        }

        check.setData(results)
          .then(check.checkForErrors.bind(check))
          .then(check.deferred.resolve);
      });

			return this.deferred.promise;
		},
		setData: function(results) {
      // Note I structured it this way to reduce the size of the data sent to the front end
      // and to avoid adding new keys that are later added to the getFocusPages utility
      var check = this;
      var deferred = Q.defer();

      function filterKeys(obj, filter) {

        var cleanObj = {};

        // Only add properties that are in the filter an exist in the original object
        _.each(filter, function (prop) {
          
          if (obj.hasOwnProperty(prop) === true) {
            
            cleanObj[prop] = obj[prop];
          }
        });

        return cleanObj;
      }

      // Merge and filter result props
      this.data = filterKeys(results, [
        'error',
        'url',
        'nonFatal',
        'focusPages'
      ]);

      // Merge and filter focus page meta props
      if (results.focusPages) {
        _.each(results.focusPages, function (focusPage, i) {
         
          check.data.focusPages[i] = filterKeys(focusPage, [
            'meta',
            'practicePhrase',
            'lawyerPhrase',
            'url',
            'error'
          ]);

        });
      }

      deferred.resolve();
      return deferred.promise;
    },
    checkForErrors: function() {
      var check = this;
      var deferred = Q.defer();

      // Loop through each article and determine if they have any bad code
      if (this.data.focusPages && this.data.focusPages.length > 0) {
        async.mapSeries(this.data.focusPages, function (focusPage, finish) {
          finish(focusPage);

          // check.hasPlaceholderText(focusPage)
          //   .then(hasLatinText(focusPage))
          //   .then(hasInternalCommentText(focusPage))
          //   .then(hasExtraPunctuation(focusPage))
          //   .then(finish);

        }, deferred.resolve);
      } else {
        deferred.resolve();
      }

      return deferred.promise;
    }
    // TODO: Add in error functions
    // hasPlaceholderText: function hasPlaceholderText(focusPage) {
    //   var check = this;
    //   var deferred = Q.defer();

    //   console.log(focusPage);
      
    //   deferred.resolve();
    //   return deferred.promise;
    // },
    // hasLatinText: function hasLatinText(focusPage) {
    //   var check = this;
    //   var deferred = Q.defer();
      
    //   deferred.resolve();
    //   return deferred.promise;
    // },
    // hasInternalCommentText: function hasInternalCommentText(focusPage) {
    //   var check = this;
    //   var deferred = Q.defer();
      
    //   deferred.resolve();
    //   return deferred.promise;
    // },
    // hasExtraPunctuation: function hasExtraPunctuation(focusPage) {
    //   var check = this;
    //   var deferred = Q.defer();
      
    //   deferred.resolve();
    //   return deferred.promise;
    // }
	});
};