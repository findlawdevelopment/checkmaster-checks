var urllib = require('url');
var async = require('async');
var uglifycss = require('uglifycss');
var UglifyJS = require('uglify-js');
var request = require('request');
var Q = require('q');
var Check = require('./master').Check;

module.exports = function () {
	'use strict';

	return new Check('Site should be using minified JS and CSS.', 'CM005', {
		resultType: 'object',
		requires: ['crawler'],
		onlyOn: {
			roles: ['Developer', 'Digital Marketer'],
			activities: []
		},
		OPTIMUM_RATIO: 0.88,
		JS_CSS_INCLUDE_REGEX: /(?:href|src|value)\s*=\s*(["'])([^\1>\n\r]+\.(?:js|css))\1/gi,
		onPageCrawl: function (page, response, pagesCrawled) {
			var deferred = Q.defer();
			var check = this;
			var includes = page.html.match(this.JS_CSS_INCLUDE_REGEX);

			if (includes) {
				includes = this.fixIncludes(includes, page.url);

				// Loop through each include
				async.map(includes, function (include, finish) {

					// See if include was already added, skip whole process if it was
					if (check.data.hasOwnProperty(include) === true) {
						finish();
						return false;
					}

					// Add include to list of includes (contents of object will be defined later)
					check.data[include] = {
						isProcessing: true, // Notify user that request is still processing
						isMinified: false // Not minified until proven minified
					};

					// Get contents of include
					request.get(include, function (err, response, body) {
						check.data[include].isProcessing = false;

						// Display error
						if (err) {
							check.data[include].error = 'Error: ' + err.message;
							check.passed = false;
							console.log(err);
							finish();
							return false;
						}
						
						// Display status code errors
						if (response && response.statusCode !== 200) {
							check.data[include].error = response.statusCode + ' status code';
							check.passed = false;
							finish();
							return false;
						}

						// Files that are less than 1000 characters are ignored
						if (body.length < 1000) {
							check.data[include].isMinified = true;
							finish();
							return false;
						}

						// Minify JS/CSS with the appropriate utility
						if (include.indexOf('.js', include.length - 3) > -1) {
							check.minifyJS(page, include, body, finish);
						} else {
							check.minifyCSS(page, include, body, finish);
						}
					});
				}, function () {
					// All done, resolve promise
					deferred.resolve();
				});
			}
				return deferred.promise;
		},
		fixIncludes: function (includes, url) {
			var check = this;
			var fixedIncludes = [];

			includes.forEach(function (include) {
				var newIncludes;

				// Strip extra href/src and quotes from include
				include = include.replace(check.JS_CSS_INCLUDE_REGEX, '$2');

				// Look for video player includes
				if (include.indexOf('mediaControls') === 0) {
					// Split includes by &amp;
					newIncludes = include.split('&amp;').map(function (include) {
						// Remove mediaControls, videoList, and playlistCombo
						return 'src="' + include.replace(/^(?:mediaControls|videoList|playlistCombo)=/i, '') + '"';
					});
					
					fixedIncludes = fixedIncludes.concat(check.fixIncludes(newIncludes, url));
					return false;
				}

				// Resolve include so it exists as a full path to the resource
				include = urllib.resolve(url, include);

				fixedIncludes.push(include);
			});

			return fixedIncludes;
		},
		minifyJS: function (page, include, contents, finish) {
			var uglified;

			try {
				// Minify JavaScript
				uglified = UglifyJS.minify(contents, { fromString: true });
			} catch (e) {
				console.log('Could not minify: ' + include);
				console.log(e.message + ' on line ' + e.line + ' col ' + e.col);
				this.data[include].error = 'Parsing error: ' + e.message + ' on line ' + e.line + ' col ' + e.col;
				this.passed = false;
				finish();
				return false;
			}

			this.compareMin(page, include, contents, uglified.code, finish);
		},
		minifyCSS: function (page, include, contents, finish) {
			var uglified;

			// Minify CSS
			uglified = uglifycss.processString(contents);

			this.compareMin(page, include, contents, uglified, finish);
		},
		compareMin: function (page, include, contents, minified, finish) {
			// Get ratio of minification (minified/unminified)
			var ratio = minified.length/contents.length;
			var includeURLData = urllib.parse(include);

			// Store ratio and whether include is considered "minified"
			this.data[include].ratio = 1 - ratio;
			this.data[include].isMinified = ratio >= this.OPTIMUM_RATIO;

			// Fail check if include is not minified "enough"
			if (this.data[include].isMinified !== true) {

				// Do not fail check when finding unminified external/special includes
				if (
					// Internal URLs only
					includeURLData.host === page.urlData.host &&
					// not /includes/*
					includeURLData.pathname.indexOf('/includes/') < 0 &&
					// not init.js
					includeURLData.pathname !== '/design/scripts/init.js' &&
					// not /blog/mt.js
					includeURLData.pathname !== '/blog/mt.js'
				) {
					this.passed = false;
				} else {
					this.data[include].justWarn = true;
				}
			}

			// Tell async we're done
			finish();
		},
		onCrawlFinish: function (pagesCrawled) {
			if (this.passed !== false) {
				this.passed = true;
			}
		}
	});
};