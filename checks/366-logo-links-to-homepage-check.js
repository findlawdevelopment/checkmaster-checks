var Check = require('./master').Check;
var request = require('request');
var Q = require('q');

module.exports = function () {
  'use strict';

  return new Check('Clicking the logo brings the user to the homepage', '366', {
    requires: ['crawler'],
    resultType: 'list',
    onlyOn: {
      roles: ['Developer', 'QA Member', 'Digital Marketer']
    },
    onPageCrawl: function (page, response) {
      var check = this;
      var deferred = Q.defer();
      var blogUrl = this.getBlogURLs()[0];
      var $ = page.dom();
      var results = [];
      var selectorNames = [
        'brand',
        'logo',
        'firmName',
        'banner'
      ];

      if (page.type === 'text/html') {
        $(this.generateSelectors(selectorNames)).each(function () {
          var $el = $(this);
          results.push($el.attr("href") === "/");
          
          // This is the same as:
          // if ($el.attr("href") === "/") {
          //  results.push(true);
          // }
          // else {
          //  results.push(false);
          // }
        });
      }
      else {
        results.push(true);
      }

      // I found .indexOf in underscore documentation
      // if any item in the results array exists, then the check should pass.     
      // // if indexOf = -1, the item doesn't exist in the array
      if (results.length === 0 || results.indexOf(true) === -1) {                
        
        check.passed = false;
        check.data.push(page.url);
        deferred.resolve();     
      }
      else {      
        deferred.resolve();
        
        // deferred.resolve(page.url);  
      }     

      return deferred.promise;
    },
    generateSelectors: function (names) {
      var selectors = [];

      names.forEach(function (name) {
        selectors.push('a.' + name); // a.class
        selectors.push('a#' + name); // a#id
        selectors.push('.' + name + ' a'); // .class a
        selectors.push('#' + name + ' a'); // #id a
      });

      return selectors.join(',');
    },
    onCrawlFinish: function () {
      var check = this;
      if (check.passed != false) {
        check.passed = true;
      }
    }
  });
};