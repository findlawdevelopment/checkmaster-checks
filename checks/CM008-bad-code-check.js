var _ = require('underscore');
var Check = require('./master').Check;
var Q = require('q');

module.exports = function () {
  'use strict';

  return new Check('Site should not include any bad code.', 'CM008', {
    requires: ['crawler'],
    resultType: 'list',
    onlyOn: {
      roles: ['Developer', 'Digital Marketer'],
      activities: []
    },
    NAME_ATTRIBUTE_REGEX: /name\s*=\s*(['"]+)([^\1]+?)\1/i,
    CHARSET_ATTRIBUTE_REGEX: /charset\s*=/i,
    CONTENT_MODULE_PATTERN: /\*module\d+\*/gi,
    REASONS: {
      BROKEN_FREEMARKER: {
        message: 'Broken FreeMarker found on page.',
        instructions: 'Please fix the broken FreeMarker on this page.'
      },
      COMMENTED_FREEMARKER: {
        message: 'Warning: Commented out FreeMarker found.',
        instructions: 'Please uncomment or remove the commented FreeMarker code.'
      },
      DIRECTIVE_ERROR: {
        message: '[an error occurred while processing this directive] found on page.',
        instructions: 'Please investigate and resolve issue with module/Publisher.'
      },
      DUPLICATE_METAS: {
        message: 'Duplicate <meta> tags found (names listed).',
        instructions: 'Please remove the duplicate tag(s) from the <head> of the site\'s template.'
      },
      PUB3008_REF: {
        message: 'References to Pub3008 found on page.',
        instructions: 'Please replace the Pub3008 references with the /includes/ or another relative path to a file residing on your site.'
      },
      CONTENT_MODULE_ERROR: {
        message: 'A content module pattern *Module#* was found on the page. This pattern is reserved for modular content modules in the Content Editor Tool.',
        instructions: 'Please review the content and remove/update the text that matches this naming convention.'
      }
    },
    onPageCrawl: function (page, response, pagesCrawled) {
      var self = this;
      var badCode = this.badHTMLText(page.html);
      var deferred = Q.defer();

      // Check if any bad code was found
      if (badCode.hasBadCode === true) {
        this.passed = false;
      }

      // Check if any warnings were found
      if (badCode.hasWarnings === true) {
        this.needsFollowUp = true;
      }

      if (badCode.hasBadCode === true || badCode.hasWarnings === true) {
        // Add URL and reason to data
        this.data = this.data.concat(_.map(badCode.reasons, function (reason) {
          return {
            url: page.url,
            reason: reason
          };
        }));
      }

      deferred.resolve();
      return deferred.promise;
    },
    getContext: function (html, text) {
      var location = html.indexOf(text);
      return html.substring(location - 50, location + text.length + 50);
    },
    badHTMLText: function (html) {
      var check = this;
      var reasons = [];
      var hasBadCode = false;
      var hasWarnings = false;

      /*
      | Errors
      */

      // Look for directive errors
      if (html.indexOf('[an error occurred while processing this directive]') > -1) {
        reasons.push({
          message: this.REASONS.DIRECTIVE_ERROR.message,
          instructions: this.REASONS.DIRECTIVE_ERROR.instructions,
          list: [
            this.getContext(html, '[an error occurred while processing this directive]')
          ]
        });
        hasBadCode = true;
      }

      // Look for broken FreeMarker
      // Ignore FreeMarker within comments
      if (html.replace(/<!--[\s\S]*?-->/gi , '').indexOf('&lt;#') > -1) {
        reasons.push(this.REASONS.BROKEN_FREEMARKER);
        hasBadCode = true;
      }
      
      // Look for links to Pub3008
      if (html.indexOf('file://///fl-custops-nas') > -1) {
        reasons.push({
          message: this.REASONS.PUB3008_REF.message,
          instructions: this.REASONS.PUB3008_REF.instructions,
          list: html.match(/file:\/\/\/\/\/fl-custops-nas[^"\n\r>]*/gi)
        });
        hasBadCode = true;
      }

      // Look for duplicate meta tags
      var metaTags = html.match(/<meta[^>]+>/gi);
      var metaNames = [];
      var metaDuplicates = [];
      if (metaTags !== null) {

        _.each(metaTags, function (tag) {

          var name;
          var nameAttribute = tag.match(check.NAME_ATTRIBUTE_REGEX);

          if (nameAttribute === null) {
            if (tag.match(check.CHARSET_ATTRIBUTE_REGEX) !== null) {
              name = 'charset';
            } else {
              return false;
            }
          } else {
            name = nameAttribute[0].replace(check.NAME_ATTRIBUTE_REGEX, '$2');
          }

          if (metaNames.indexOf(name) > -1) {
            if (name === 'charset') {
              metaDuplicates.push('<meta charset>');
            } else {
              metaDuplicates.push('<meta name="' + name + '">');
            }
          }

          metaNames.push(name);
        });

        if (metaDuplicates.length > 0) {
          reasons.push({
            message: check.REASONS.DUPLICATE_METAS.message,
            instructions: check.REASONS.DUPLICATE_METAS.instructions,
            list: metaDuplicates
          });
          hasBadCode = true;
        }
      }

      // Look for content module pattern
      var contentModules = html.match(this.CONTENT_MODULE_PATTERN);
      if (contentModules !== null) {
        reasons.push({
          message: this.REASONS.CONTENT_MODULE_ERROR.message,
          instructions: this.REASONS.CONTENT_MODULE_ERROR.instructions,
          list: [
            this.getContext(html, contentModules[0])
          ]
        });
        hasBadCode = true;
      }

      /*
      | Warnings
      */

      // Look for commented out FreeMarker
      // - find all comments
      var comments = html.match(/<!--[\s\S]*?-->/gi);
      if (comments !== null) {
        _.each(comments, function (comment) {
          // Check for FreeMarker within comments
          if (comment.indexOf('&lt;#') > -1) {
            reasons.push({
              message: check.REASONS.COMMENTED_FREEMARKER.message,
              instructions: check.REASONS.COMMENTED_FREEMARKER.instructions,
              isWarning: true,
              list: [comment]
            });
            hasWarnings = true;
          }
        });
      }

      return {
        hasBadCode: hasBadCode,
        hasWarnings: hasWarnings,
        reasons: reasons
      };
    },
    onCrawlFinish: function (pagesCrawled) {
      // Mark as passed if not explicitly failed
      if (this.passed !== false) {
        this.passed = true;
      }
    }
  });
};