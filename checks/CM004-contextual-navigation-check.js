var Check = require('./master').Check;
var Q = require('Q');

module.exports = function () {
	'use strict';

	return new Check('Contextual navigation is used in the design.', 'CM004', {
		requires: ['crawler'],
		resultType: 'list',
		onlyOn: {
			roles: ['Developer', 'Digital Marketer'],
			activities: [],

			types: ['internal']
		},
		ATTORNEY_REGEX: /\/(Our\-)?Attorneys?(\-Profiles?)?\//gi,
		onPageCrawl: function (page) {
			var $;
			var section;
			var deferred = Q.defer();
			
			if (
				// Ignore pages that aren't child pages or sections
				page.urlData.pathname.match(/^\/[^\/]+\//) === null ||
				page.urlData.pathname.match(this.ATTORNEY_REGEX) !== null ||
				page.urlData.pathname.match(/^\/blog\//gi) !== null ||
				page.urlData.pathname.match(/^\/mt-bin\//gi) !== null
			) {
				deferred.resolve();
				// exit early
				return deferred.promise;
			}

			// Load DOM
			$ = page.dom();
			// Get section
			section = page.urlData.pathname.replace(/(\/.*\/).*/g, '$1');

			if (this.hasUnrelatedLinks($, section) === true) {
				this.data.push(page.url);

				// Passing check so styles show up correctly (it never fails anyway)
				this.passed = true;

				// Mark check as needing followup (since this is just a warning)
				this.needsFollowUp = true;

			}
			deferred.resolve();


			return deferred.promise;
		},
		hasUnrelatedLinks: function (dom, section) {
			var self = this;

			// Find all links within selected element
			return dom('[data-nav="contextual"] a').filter(function (i, el) {
				var href = el.attribs.href || '';
				// Determine if HREF matches section
				return self.notSection(href, section);
			}).length > 0;
		},
		notSection: function (href, section) {
			// Exclude paths that are within section
			return href.indexOf(section) !== 0 &&
				// Exclude attorney sections
				href.match(this.ATTORNEY_REGEX) === null &&
				// Exclude paths that are not section children (e.g. /Section/Child-Page.shtml)
				href.match(/^\/[^\/]+\/[^\.]+\.shtml$/i) !== null;
		},
		onCrawlFinish: function () {
			// Mark as passed if not explicitly failed
			if (this.passed !== false) {
				this.passed = true;
			}
		}
	});
};