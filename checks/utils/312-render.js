module.exports = function () {
  // for testing only:
  if (this && this.test === true) {
    document = this.document;
  }

  return [].slice.call(document.getElementsByTagName('a')).map(function (pageLink) {
    // Convert array of DOM elements into objects with the href and target
    return {
      href: pageLink.getAttribute('href'),
      target: pageLink.getAttribute('target'),
      text: pageLink.innerText
    };
  });
};