var async = require('async');
var cheerio = require('cheerio');
var libxmljs = require('libxmljs');
var Mustache = require('mustache');
var request = require('request');
var Q = require('q');
var winston = require('winston');
var _ = require('lodash');

var constants = {
  BAD_STATUS_CODE: 'Could not retrieve Focus Pages (returned bad status code).',
  FP_FAIL: 'Could not retrieve Focus Page content.',
  META_FAIL: 'Could not retrieve Focus Page meta tags.',
  PRACTICE_PHRASE_FAIL: 'Could not retrieve Focus Page practice phrase.',
  LAWYER_PHRASE_FAIL: 'Could not retrieve Focus Page lawyer phrase.',
  INVALID_XML: 'Could not retrieve Focus Pages (bad XML in profile).',
  ERROR_DETAILS: '\n/====================\n getFocusPages\n URL:   {{{url}}}\n Error: {{error}}\n\\====================',
  NO_FOCUS_PAGES: 'No Focus Pages were found for this firm.',
  NO_WLD_MESSAGE: 'WLD ID for this FirmSite was not found.',
  PVIEW_URL: 'http://pview.findlaw.com/view/{{ wldID }}_1',
  PVIEW_XML_URL: 'http://pview.findlaw.com/view/{{ wldID }}_1?internal=6',
  XML_EMPTY: 'Could not get Focus Pages (no data found for firm).'
};

function fail(deferred, error, url) {
  deferred.resolve({
    error: error.message,
    url: url
  });
}

function noFocusPages(deferred, url) {
  deferred.resolve({
    error: constants.NO_FOCUS_PAGES,
    noFP: true,
    url: url
  });
}

function getFocusPages(wldID) {
 
  var deferred = Q.defer();

  if (!wldID) {
    fail(deferred, new Error(constants.NO_WLD_MESSAGE));
  } else {
      // Get pview XML
      getPViewData(deferred, wldID)
        // Find Focus Page URLs
        .then(getFPURLs.bind(undefined, deferred))
        // Retrieve Focus Page content
        .then(getFPContent.bind(undefined, deferred))
        // Finish process
        .then(deferred.resolve);
    }

  return deferred.promise;
}

function getPViewData(mainPromise, wldID) {
  var deferred = Q.defer();

  var url = Mustache.render(constants.PVIEW_XML_URL, { wldID: wldID });
  var publicURL = Mustache.render(constants.PVIEW_URL, { wldID: wldID });

  // Request pview XML
  request.get(url, function (err, response, xml) {

    if (err) {
      // Fail if an error occurs during request
      deferred.reject();
      fail(mainPromise, err, publicURL);
      return false;
    }

    if (response.statusCode !== 200) {
      // Fail if a bad status code is encountered
      deferred.reject();
      fail(mainPromise, new Error(constants.BAD_STATUS_CODE), publicURL);
      return false;
    }

    if (!xml.trim()) {
      // Fail if XML is empty
      deferred.reject();
      fail(mainPromise, new Error(constants.XML_EMPTY), publicURL);
      return false;
    }

    // Pass on XML, XML URL, and public URL
    deferred.resolve({
      xml: xml,
      url: url,
      publicURL: publicURL
    });
  });

  return deferred.promise;
}

function getFPURLs(mainPromise, data) {
  var deferred = Q.defer();

  var focusPages = [];
  var practiceGroupParents;
  var practiceGroups;
  var publicURL = data.publicURL;
  var url = data.url;
  var xml = data.xml;
  var xmlDoc;

  // Parse XML
  try {
    xmlDoc = libxmljs.parseXml(xml);
  } catch (err) {

    // Log details to console/log file
    winston.error(Mustache.render(constants.ERROR_DETAILS, {
      url: url,
      error: err.message
    }));

    // Fail if XML failed to parse
    deferred.reject();
    fail(mainPromise, new Error(constants.INVALID_XML), url);
    return false;
  }

  practiceGroupParents = xmlDoc.root().find('//PracticeGroups');

  if (!practiceGroupParents[0]) {
    // If no PracticeGroups elements are found, there are no Focus Pages
    noFocusPages(mainPromise, publicURL);
    return false;
  }

  practiceGroups = practiceGroupParents[0].find('//PracticeGroup');

  if (!practiceGroups[0]) {
    // If no PracticeGroup elements are found, there are no Focus Pages
    noFocusPages(mainPromise, publicURL);
    return false;
  }

  practiceGroups.forEach(function (group) {

    if (group.attr('pg_status_code').value() !== 'A') {
      return true;
    }

    var url = group.find('pg_vanity_URL_addr')[0].text();

    if (url.indexOf('/FP', url.length - 3) > -1) {
      focusPages.push(url);
    }
    
  });

  if (focusPages.length === 0) {
    // If no valid Focus Page URLs are found, there are no Focus Pages
    noFocusPages(mainPromise, publicURL);
    return false;
  }

  deferred.resolve(focusPages);

  return deferred.promise;
}

function getFPContent(mainPromise, focusPages) {
  var deferred = Q.defer();
  var results = [];

  async.mapSeries(focusPages, function (focusPage, finish) {

    request.get(focusPage + '?internal=6', function (err, response, xml) {
      var $;
      var fpFail = {
        error: constants.FP_FAIL,
        url: focusPage
      };
      var xmlDoc;

      if (err) {
        winston.error(Mustache.render(constants.ERROR_DETAILS, {
          error: err.message,
          url: focusPage
        }));
        results.push(fpFail);
        finish();
        return false;
      }

      if (response.statusCode !== 200) {
        // Fail if request returns a bad status code
        results.push(fpFail);
        finish();
        return false;
      }

      // Parse XML
      try {
        xmlDoc = libxmljs.parseXml(xml);
      } catch (err) {

        // Log details to console/log file
        winston.error(Mustache.render(constants.ERROR_DETAILS, {
          url: focusPage,
          error: err.message
        }));

        // Fail if XML failed to parse
        results.push(fpFail);
        finish();
        return false;
      }

      results.push({
        title: getTitle(xmlDoc),
        narrative: getNarrative(xmlDoc),
        meta: {
          title: getMetaTitle(xmlDoc),
          description: getMetaDescription(xmlDoc),
          keyword: getMetaKeyword(xmlDoc)
        },
        practicePhrase: getPracticePhrase(xmlDoc),
        lawyerPhrase: getLawyerPhrase(xmlDoc),
        url: focusPage
      });

      finish();
    });
  }, function () {
    deferred.resolve({
      focusPages: results
    });
  });

  return deferred.promise;
}

function getTitle(xmlDoc) {
  var Organization = xmlDoc.find('//Organization')[0];
  var OrgName;
  var title = '';

  if (Organization) {
    OrgName = Organization.find('//OrgName')[0];

    if (OrgName) {
      title = OrgName.text();
    }
  }

  return title;
}

function getNarrative(xmlDoc) {
  var narrative = xmlDoc.find('//NarrativeText')[0];
  var narrativeText = '';

  if (narrative) {
    narrativeText = narrative.text();
  }

  return narrativeText;
}

function getMetaTitle(xmlDoc) {
  var metaTitle = xmlDoc.find('//meta_title')[0];
  var metaTitleText = '';

  if (metaTitle) {
    metaTitleText = metaTitle.text();
  } else {
    metaTitleText = 'No meta title was found.';
  }

  return metaTitleText;
}

function getMetaDescription(xmlDoc) {
  var metaDescription = xmlDoc.find('//meta_description')[0];
  var metaDescriptionText = '';

  if (metaDescription) {
    metaDescriptionText = metaDescription.text();
  } else {
    metaDescriptionText = 'No meta description was found.'
  }

  return metaDescriptionText;
}

function getMetaKeyword(xmlDoc) {
  var metaKeyword = xmlDoc.find('//meta_keyword')[0];
  var metaKeywordText = '';

  if (metaKeyword) {
    metaKeywordText = metaKeyword.text();
  } else {
    metaKeywordText = 'No meta keyword was found.';
  }

  return metaKeywordText;
}

function getPracticePhrase(xmlDoc) {
  var practicePhrase = xmlDoc.find('//practice_phrase')[0];
  var practicePhraseText = '';

  if (practicePhrase) {
    practicePhraseText = practicePhrase.text();
  } else {
    practicePhraseText = 'No practice phrase was found.';
  }

  return practicePhraseText;
}

function getLawyerPhrase(xmlDoc) {
  var lawyerPhrase = xmlDoc.find('//lawyer_phrase')[0];
  var lawyerPhraseText = '';

  if (lawyerPhrase) {
    if (lawyerPhrase.text() === 'Dont Display') {
      lawyerPhraseText = 'The lawyer phrase was listed in the profile as "Don\'t Display"';
    } else {
      lawyerPhraseText = lawyerPhrase.text();
    }
  } else {
    lawyerPhraseText = 'No lawyer phrase was found.';
  }

  return lawyerPhraseText;
}

exports.constants = constants;
exports.getFocusPages = getFocusPages;