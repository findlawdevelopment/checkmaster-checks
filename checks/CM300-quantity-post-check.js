var Q = require('q');
var Check = require('./master').Check;
var request = require('request');

module.exports = function () {
	'use strict';

	return new Check('At least 11 posts exist on the preview blog', 'CM300', {
		resultType: 'list',
		onlyOn: {
			roles: ['Developer'],
			activities: [],
			types: ['preview', 'blog', 'internal']
		},
		run: function (url, siteInfo) {
			var deferred = Q.defer(),
					check = this;

			request.get(check.getBlogURLs()[0], function (err, res, body) {

				if (!err) {

					var $ = check.renderDom(body);

					var archiveLinks = check.getArchiveLinks($);
					var qtyPosts = check.countArchiveLinks(archiveLinks);

					if (qtyPosts > 10) {

						deferred.resolve();

					} else {

						deferred.reject(new Error('Not enough posts'));

					}


				} else {

					deferred.reject(err.toString());

				}

			});

			return deferred.promise;
		},

		countArchiveLinks: function (links) {

			links = links || [];

			var count = 0;
			links.forEach(function (link, i) {

				var match = link.match(/\((\d*)\)/i),
						qty;

				if (match) {

					qty = parseInt(match[1], 10);

				}

				count += qty || 0;

			});

			return count;

		},

		getArchiveLinks: function ($) {

			var arr = [];
			
			$('#archives').find('li').each(function (i, ele) {

				arr.push($(this).text());

			});

			return arr;

		}
	});
};