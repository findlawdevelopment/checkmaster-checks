/*jshint strict:true */
'use strict';
var Q = require('q')
  , pathlib = require('path')
  , debug = require('debug')('loader')
;

var allChecks = []
  , checksDir = pathlib.join(__dirname, '..', 'checks')
;

/**
 * Adds a promise interface to the readdir fs function
 *
 */
var readChecks = function () {
  var fs = require('fs');
  var deferred = Q.defer();
  fs.readdir(checksDir, deferred.makeNodeResolver());
  return deferred.promise;
};

/**
 * Checks to see if fileNames are in cache
 *  * If cache exists, returns cache
 *  * If no cache, reads check directory and builds cache
 * 
 */
var cacheChecks = function () {
  var deferred = Q.defer();

  if (allChecks.length === 0) {
    // Load all checks

    readChecks().done(
      function (results) {
        results.forEach(function (fileName) {
          var check;

          // Only load valid Check files (should end with "-check.js")
          if (fileName.indexOf('-check.js', fileName.length - 9) > -1) {

            // Load check object
            check = require(pathlib.join(checksDir, fileName))();

            allChecks.push(check);

          }
        });

        deferred.resolve();
      },
      debug
    );

  } else {

    deferred.resolve();

  }

  return deferred.promise;
};

/*
 * Loads all checks into memory
 * NOTE: POPULATION OF allChecks SHOULD ONLY HAPPEN ONCE
 *
 * @param filters {object} - the path, filters, and check IDs for loading checks
 */

module.exports = function (filters, dir) {
  var deferred = Q.defer();

  if (dir) checksDir = dir;
  // simulate async
  setTimeout(function () {
    cacheChecks().done(function () {

      var checks = allChecks.filter(function (check) {
        var tests = [];
        tests.push(check.shouldRun(filters));

        if (check.hasOwnProperty('shouldRun')) {
          // also call the original shouldRun to avoid duplicating code
          tests.push(
            require('./master').Check.prototype.shouldRun.call(check, filters)
          );
        }

        return tests.indexOf(false) === -1;
      });

      deferred.resolve(checks);

    });
  });

  return deferred.promise;
};