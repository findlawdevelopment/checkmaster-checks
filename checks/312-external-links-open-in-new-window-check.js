// Native modules
var pathlib = require('path');
var childProcess = require('child_process');
var urllib = require('url');

// Vendor modules
var Q = require('q');

// Authored modules
var Check = require('./master').Check;

module.exports = function () {
  'use strict';

  return new Check('External links open in a new browser window.', '312', {
    requires: ['crawler'],
    resultType: 'list',
    onlyOn: {
      roles: ['Developer', 'QA Member'],
    },
    ERRORS: {
      unableToRender: 'Unable to check page for external links. Please check links for this page manually.'
    },

    onPageCrawl: function (page) {
      var check = this;
      var deferred = Q.defer();

      check
        .renderPage(page)
        .then(check.findMissingTargets.bind(check, page), this.handleError.bind(check, page, deferred))
        .done(function (missingTargets) {
          if (missingTargets && missingTargets.length > 0) {
            // Add failure to data
            check.data = check.data.concat(missingTargets);
            // Fail check
            check.passed = false;
          }
          deferred.resolve();
        });

      return deferred.promise;
    },

    handleError: function (page, deferred, error) {
      // User needs to manually check this page
      this.needsFollowUp = true;

      // Tell user about the error
      this.data.push({
        error: error,
        page: page.url
      });

      deferred.resolve();
    },

    renderPage: function (page) {
      var check = this;
      var deferred = Q.defer();
      var error = '';
      var phResponse = '';

      // Run URL through PhantomJS
      var phantomJS = childProcess.spawn('phantomjs', [pathlib.join(__dirname, '..', 'core', 'utils', 'render.js'), page.url], {
        stdio: ['pipe', 'pipe', 'pipe']
      });

      // Pipe check-specific render file into render utility
      phantomJS.stdin.write(pathlib.resolve(pathlib.join(__dirname, 'utils', '312-render.js')));

      phantomJS.stdout.on('data', function (data) {
        phResponse += data.toString();
      });

      phantomJS.stderr.on('data', function (data) {
        console.error(data.toString());
        error += data.toString();
      });

      phantomJS.on('close', function () {
        var results;

        // Clean up PhantomJS response (grab only the good parts)
        phResponse = phResponse.replace(/[\s\S]*PHANTOMJS_RESPONSE_JSON>>>([\s\S]*)<<<PHANTOMJS_RESPONSE_JSON[\s\S]*/gi, '$1');

        // Make sure phResponse is JSON
        if (phResponse.trim() !== '') {
          try {
            results = JSON.parse(phResponse.trim());
          } catch (e) {

            // Log error
            console.error('Unable to parse PhantomJS response:');
            console.error(e);
            console.error(phResponse);

            // Reject promise
            deferred.reject(check.ERRORS.unableToRender);
            return false;
          }
        }

        // If results were found, find missing targets
        if (results) {
          deferred.resolve(results);
        } else if (error !== '') {
          deferred.reject(check.ERRORS.unableToRender);
        }
      });

      phantomJS.stdin.end();

      return deferred.promise;
    },

    findMissingTargets: function (page, results) {
      var deferred = Q.defer();
      var check = this;
      var missingTargets = [];

      // Loop through each link
      results.forEach(function (link) {
        // If host is not page's host and target is not "_blank", fail check
        if (check.shouldOpenInNewWindow(page, link) === true) {
          missingTargets.push({
            page: page.url,
            linkHREF: link.href,
            linkTarget: link.target,
            linkText: link.text
          });
        }
      });

      deferred.resolve(missingTargets);

      return deferred.promise;
    },

    shouldOpenInNewWindow: function (page, link) {
      var linkURLData = urllib.parse(urllib.resolve(page.url, link.href));
      var outOfOurControlPatterns = [
        'flashplayer'
      ];
      var skip;

      outOfOurControlPatterns.forEach(function (pattern, i) {
        if (link.href.indexOf(pattern) >= 0) {
          skip = true;
        }
      })

      return (
        // Make sure it's an external link
        linkURLData.host !== page.urlData.host &&
        // Check if "_blank" was added
        link.target !== '_blank' &&
        // Check if this is a JS link
        link.href.indexOf('javascript:') !== 0 &&
        // Check if this is a mailto link
        link.href.indexOf('mailto:') !== 0 &&
        // Check if this is a tel link
        link.href.indexOf('tel:') !== 0 &&
        // check if this link is out of our control
        !skip
      );
    },

    onCrawlFinish: function () {
      // Mark as passed if not explicitly failed
      if (this.passed !== false) {
        this.passed = true;
      }
    }
  });
};