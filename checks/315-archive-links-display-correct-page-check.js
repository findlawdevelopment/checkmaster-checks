/* jshint strict: true */

var async = require('async');
var request = require('request');
var Q = require('q');
var Check = require('./master').Check;

module.exports = function () {
	'use strict';

	/*

	Success criteria:

	1. This should fail if Navigation Blog is not present
    2. This should fail if The welcome post is present
	3. This should prompt the user if there are no tag or archive links
	4. The page title should be the tag name on tag pages
	5. The page title should be the date on archive links
	6. Should indicate number of archive links and tags (positive result)

	*/

	return new Check('The links listed in the archives navigation area of the site display the intended archives page when clicked.', '315', {
		resultType: 'list',
		onlyOn: {
            activities: ['Complete F.E.D.', 'QA Static Preview'],
            types: ['blog', 'preview', 'internal'],
            roles: ['Developer', 'QA Member', 'Digital Marketer']
        },
        async: true,

        // Cheerio selectors for tag cloud and archive links
        tagSelector: '#tagCloud ul a, .nav-tag-cloud ul a, .tags a',
        archiveSelector: '#archives ul a, .nav-archives ul a',

        getBlogURLs: function () {
            return this._blogURLs ? this._blogURLs : [];
        },

        run: function (page, siteInfo) {
            var check = this;
            var deferred = Q.defer();

            request.get(check.getBlogURLs()[0], function (err, response, body) {
                if (!err) {

                    check.onBlogPage.apply(check, [err, response, body]);
                    deferred.resolve();

                } else {

                    deferred.reject('Archive links check failed: ' + err);
                    
                }

            });
            
            return deferred.promise;
        },
        getMissingLinks: function (tag, archive) {
            /*
                Given the selected tag and archive links, return text that represents which is empty

                tag - cheerio object of tag links
                archive - cheerio object of archive links

                getMissingLinks($('a.tag-links'), $());
                => ['archive links']
                
                getMissingLinks($(), $('a.archive-links'));
                => ['tags']

            */

            var missing = [];

            if (tag.length === 0) {
                missing.push('tags');
            }

            if (archive.length === 0) {
                missing.push('archive links');
            }

            return missing;
        },
        hasWelcomePost: function (body) {
            /*
                Determine if the welcome post is displayed on the blog

                body - cheerio object of the body html element

                hasWelcomePost($('.content')) - welcome post exists
                => true

                hasWelcomePost($('.content')) - welcome post does not exist
                => false
            */

            return body.find('.post-only').find('h2').text().indexOf('Welcome') === 0;
        },

        isBlogNavMissing: function (body) {
            /*
                Determine if the blog navigation is missing on the blog

                body - cheerio object of the body html element

                isBlogNavMissing($('.content')) - blog navigation exists
                => true
                
                isBlogNavMissing($('.content')) - blog navigation does not exist
                => false
            */

            return body.find('#navigationBlog, .navigation-blog, .nav-blog').length === 0 && body.find(this.tagSelector).length === 0 && body.find(this.archiveSelector).length === 0;
        },
        onBlogPage: function (err, response, body) {
            var $ = this.renderDom(body),
                check = this;

            // Find all tag links
            var tagLinks = this.getText(this.tagSelector, $, 'tag');
            var archiveLinks = this.getText(this.archiveSelector, $, 'archive');

            this.getPages(tagLinks.concat(archiveLinks), function(err, results) {

                if (check.isBlogNavMissing($('body'))) {

                    check.data.push({
                        warning: 'The blog navigation is missing. Please add it to continue testing',
                        warningType: 'noNavBlog'
                    });

                    check.passFail(null, [false]);

                } else if (check.hasWelcomePost($('body'))) {

                    check.data.push({
                        warning: 'There are no posts. Please add test posts and run Checkmaster again',
                        warningType: 'noPosts'
                    });

                    check.passFail(err, [false]);

                } else if ((tagLinks.length === 0 || archiveLinks.length === 0)) {

                    check.passFail(err, ['nonFailure']);
                    
                    var missing = check.getMissingLinks(tagLinks, archiveLinks);

                    check.data.push({
                        warning: 'There are no ' + missing.join(', and '),
                        warningType: 'noLinks'
                    });

                } else {

                    check.passFail(err, results);

                }

            });
        },
        passFail: function(err, results) {
            if(results.indexOf(false) > -1) {
                this.passed = false;
            } else if (results.indexOf('nonFailure') > -1) {
                this.needsFollowUp = true;
            } else {
                this.passed = true;
            }
        },
        getText: function (selector, $, type) {
            var items = [];

            $(selector)
                .each(function() {
                    // Strip off post count from the link -- e.g. November 2012 (1)
                    var tagNameText = $(this).text().replace(/(.*?)\s*\(\d+\)\s*$/, '$1');
                    var tagHREF = $(this).attr('href');

                    // Complete Archives link uses "Archives" as the page title
                    if (tagNameText === 'Complete Archives') {
                        tagNameText = 'Archives';
                    }

                    items.push({
                        text: tagNameText,
                        href: tagHREF,
                        type: type
                    });
                })
            ;
            return items;
        },
        getPages: function (pages, callback) {

            var check = this;

            async.map(pages, function (item, finished) {
                request.get(item.href, function (err, response, body) {
                    var hasCorrectPageTitle = check.checkPageTitle(body, item.text, item.type);
                    finished();
                });
            }, callback);
        },
        checkPageTitle: function (html, linkText, type) {
            var $ = this.renderDom(html);
            var regex;
            
            switch (type) {
                case 'tag':
                    regex = /.*"([^"]*)".*/g;
                    break;
                case 'archive':
                    regex = /(.*)\sArchives.*/g;
                    break;
            }
            
            var pageTitle = $('.page-title, #pageTitle').text().replace(regex, '$1');
            
            return pageTitle === linkText;
        }
	});
};