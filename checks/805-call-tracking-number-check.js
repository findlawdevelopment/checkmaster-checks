var Q = require('q');
var Check = require('./master').Check;
var _ = require('lodash');

module.exports = function () {
  'use strict';

  return new Check('Call Tracking Number', '805', {
    requires: ['crawler'],
    onlyOn: {
  		activities: [],
  		types: ['internal']
    },
    resultType: 'list',
    roles: ['Developer', 'QA Member', 'Digital Marketer'],
    numbers: {},
    getNumbersFromHTML: function (page) {
      var numbers = [],
          $ = page.dom(),
          check = this;

      $('*').each(function (i, ele) {
        var $el = $(ele),
            type = $el.name,
            text = '',
            attr = '';

        if (type === 'a') {

          attr = $el.attr('href');
          text = $el.text();

        } else if (type === 'img') {

          attr = $el.attr('alt');

        } else {

          text = $el.text();

        }

        var numberTest = /(?:^|\b|\()(?:\d{3}(?:\)\s?|-|\.))\d{3}(-|\.)\d{4}/g;

        if (check.getSiteInfo().country === 'uk' || check.getSiteInfo().country === 'gb') {

          numberTest = /\d{3,4} \d{3,4} \d{4}/g;

        }

        var matches = [
          text.match(numberTest),
          attr.match(numberTest)
        ];
        _.forEach(matches, function (match, i) {

          if (match) {
            match = Array.prototype.slice.call(match);
            numbers = numbers.concat(
              _.map(match, function (number) {
                return {
                  number: number.replace(/\D/g, ''),
                  location: page.url,
                  originalNumber: number
                };
              })
            );
          }
        });

      });
      return numbers;

    },

    reduceNumbers: function (numbers) {

      var uniques = [];
      _.forEach(numbers, function (n) {
          var match = false;
          _.forEach(uniques, function (u) {
              
              if (n.number === u.number) match = true;
          
          });
          
          if (!match) uniques.push(n);
      });

      return uniques;

    },

    onPageCrawl: function (page, response, pagesCrawled) {

      var deferred = Q.defer();

      this.numbers = this.getSiteInfo().numbers;
      this.info = this.info || {};
      this.info.numbers = this.numbers;
      this.info.country = this.getSiteInfo().country;

      this.data = this.data.concat(
        this.reduceNumbers(
          this.getNumbersFromHTML(page)
        )
      );
      deferred.resolve();

      return deferred.promise;

    },

    onCrawlFinish: function (pagesCrawled) {
      this.needsFollowUp = true;
      // assume true. All phone number filtering happens in the browser
      this.passed = false;

      _.forEach(this.getSiteInfo().numbers.wld, function (wldNum) {

        this.data.push({
          number: wldNum.number,
          originalNumber: wldNum.number,
          location: 'http://pview.findlaw.com/cmd/view?pid=1&wld_id=' + this.getSiteInfo().wldid
        });

      }, this);

    }
  });
};