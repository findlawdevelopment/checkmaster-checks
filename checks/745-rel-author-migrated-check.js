var Q = require('q');
var request = require('request');
var Check = require('./master').Check;

module.exports = function () {
	'use strict';

	return new Check('Verify that the rel="author" code was migrated correctly.', '745', {
		resultType: 'object',
		onlyOn: {
			roles: ['Developer', 'QA Member', 'Digital Marketer'],
			activities: [],
			types: ['preview', 'internal']
		},
		run: function (page) {
			var check = this;
			var deferred = new Q.defer();
			var liveSite = 'http://' + check.getSiteInfo().domain;
			
			check.data.errors = [];
			check.data.liveSite = liveSite;

			Q.all([
				// Check preview site for link tag
				check.getAuthorLink(page.url, 'preview'),
				// Check live site for link tag
				check.getAuthorLink(liveSite, 'live')
			]).spread(function (preview, live) {

				if (check.data.errors.length > 0) {
					deferred.reject();
					return false;
				}

				// If live site has the link tag and preview does not, fail the check
				if (live.hasLink === true && preview.hasLink === false) {
					check.passed = false;
				} else {
					check.passed = true;
				}

				deferred.resolve();

			});

			return deferred.promise;
		},
		getAuthorLink: function (url, type) {
			var check = this;
			var deferred = Q.defer();

			// Get page HTML
			request.get(url, function (err, response, html) {
				var result = false;

				if (err || response.statusCode !== 200) {
					check.data.errors.push('Error: could not get HTML from ' + url + '.');
					check.data.errors.push(err || 'Bad status code: ' + response.statusCode);
				} else {
					// Determine if a <link rel="author"> tag is present
					if (html.match(/<link[^>]+rel\s*=\s*(\'|"|)\s*author\s*\1[^>]*>/gi) !== null) {
						result = true;
					}
				}

				deferred.resolve({
					type: type,
					hasLink: result
				});
			});

			return deferred.promise;
		}
	});
};