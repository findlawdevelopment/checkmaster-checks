/* jshint strict: true */

var urllib = require('url');
var async = require('async');
var Q = require('q');
var request = require('request');
var Check = require('./master').Check;

function findSearchForm($) {
	"use strict";

	var searchForm = $('#searchForm'); // <form id="searchForm"></form>

	if (searchForm.length > 0) {
		return searchForm;
	}

	searchForm = $('#searchFormDesign form'); // <div id="searchFormDesign"><form></form></div>

	if (searchForm.length > 0) {
		return searchForm;
	}

	searchForm = $('form[action$="-Results.shtml"]'); // <form action="/Search-Results.shtml"></form>

	if (searchForm.length > 0) {
		return searchForm;
	}

	return null;
}

module.exports = function () {
	"use strict";

	return new Check('A search form is included in the design on all pages. The form text says "Search our blog".', '335', {
		resultType: 'list',
		onlyOn: {
			roles: ['Developer', 'QA Member'],
			activities: [],
			types: ['blog', 'internal']
		},
		run: function (page, siteInfo) {
			var check = this,
				correctSearchText = '',
				correctSearchTextLower,
				passed = true,
				deferred = Q.defer(),
				urlData = urllib.parse(page.url),
				hostname = urlData.protocol + '//' + urlData.hostname;

			// Set correct search text
			if (siteInfo.types.indexOf('slash_blog') > -1) {
				// Set correct site search text to "Search our site"
				correctSearchText = 'Search our site';
			} else {
				// Set correct site search text to "Search our blog"
				correctSearchText = 'Search our blog';
			}

			correctSearchTextLower = correctSearchText.toLowerCase();

			// Check each URL
			// Create a group of asynchronous requests for each URL we need to check
			var urlRequests = this.getBlogURLs().map(function (url) {
				return function (finishCallback) {
					// Get HTML at URL
					request.get(url, function (err, response, body) {
						// Find search form
						var $ = check.renderDom(body),
							searchForm = findSearchForm($), // Find search form via Cheerio
							searchInput;

						// If search form doesn't exist, this fails
						if (searchForm === null) {
							passed = false;
							finishCallback(null, {
								message: 'Search form is missing',
								url: url
							});
							return false;
						}

						// Find search input
						searchInput = searchForm.find('input').filter(function () {
							return $(this).attr('title') ? $(this).attr('title').toLowerCase() === correctSearchTextLower : false;
						});

						// If the search input (that matches the text) isn't found, this fails
						if (searchInput.length === 0) {
							passed = false;
							finishCallback(null, {
								message: 'Search text should be "' + correctSearchText + '"',
								url: url
							});
							return false;
						}

						finishCallback(null, null);
					});
				};
			});

			async.parallel(urlRequests, function (err, results) {
				// Only include results that are actual failures
				check.data = results.filter(function (failure) {
					return failure !== null;
				});

				check.passed = check.data.length === 0;

				deferred.resolve();
			});

			return deferred.promise;
		}
	});
};