var _ = require('lodash');
var Check = require('./master').Check;
var Q = require('q');

module.exports = function () {

	return new Check('There are no broken links, including links to external sites.', '323', {
		include: ['external links'],
		requires: ['crawler'],
		resultType: 'list',
		onlyOn: {
			roles: ['Developer', 'QA Member', 'Digital Marketer'],
			activities: [],
			statusCodes: ['error']
		},
		SSL_CONNECTION_ERROR: 'SSL connection error',
		DOMAIN_NOT_FOUND: 'DOMAIN NOT FOUND',
		onPageCrawl: function (page, pagesCrawled) {
			var deferred = Q.defer();
			if (page.error) {
				var issue = '';

				if (page.error.message && page.error.message.indexOf('SSL23_GET_SERVER_HELLO:unknown protocol') > -1) {
					issue = this.SSL_CONNECTION_ERROR;
				} else if (page.error.code === 'ENOTFOUND' || page.error.code === 'EADDRINFO') {
					issue = this.DOMAIN_NOT_FOUND;
				} else if (page.error.message.indexOf('Protocol:') > -1 && page.error.message.indexOf(': not supported.') > -1) {
					issue = '"' + page.error.message.replace(/Protocol:([^:]+): not supported\..*/gi, '$1') + '" protocol not supported';
				} else if (page.statusCode && page.statusCode !== 200) {
					issue = 'Bad status: ' + page.statusCode;
				} else {
					issue = page.error.message;
				}

				if (page.isExternal === false && page.statusCode === 401) {
					this.needsCredentials = page.url;
				}

				this.passed = false;

				this.data.push({
					brokenLink: page.url,
					redirects: page.redirects,
					error: issue,
					referrers: [page.referrer]
				});
			}
			deferred.resolve();
			return deferred.promise;
		},
		getAllReferrers: function (pagesCrawled) {
			var check = this;

			// Loop through all broken links
			_.forEach(check.data, function (item, index) {

				// Get all possible URLs that represent this broken link
				var possibleURLs = item.redirects;
				possibleURLs.push(item.brokenLink);

				// Loop through all pages
				_.forEach(pagesCrawled, function (page) {

					// Skip the broken link pages
					if (possibleURLs.indexOf(page.url) > -1) {
						return true;
					}

					// Loop through all possible URLs
					_.forEach(possibleURLs, function (possibleURL) {

						// Adds referrer if all apply:
						//  - URL is in the page.links property
						//  - URL is not already in the broken link's referrers list
						//  - URL is not a broken link URL
						if (page.links.indexOf(possibleURL) > -1 && check.data[index].referrers.indexOf(page.url) < 0) {
							check.data[index].referrers.push(page.url);
						}
					});
				});
			});
		},
		onCrawlFinish: function (pagesCrawled) {
			// Update data for the last time since
			this.getAllReferrers(pagesCrawled);

			if (this.passed !== false) {
				this.passed = true;
			}
		}
	});
};