/*jshint strict:true */
var debug = require('debug')('Check');

/**
 * Check Class
 * -----------
 * Constructs a new check with common functionality to aid in processing checks
 *
 * @name {String} - a name that describes the check
 * @defectID {String} - the defect ID, or CM### code specific to this check
 * __Code Assignments__
 *  - CM1## - Derrick Gall
 *  - CM2## - Matt Biersdorf
 *  - CM3## - TJ Simons
 *  - CM4## - Kristy Wessell
 * @params {Object} - Check specific parameters
 */
function Check(name, defectID, params) {
	"use strict";

	if (!name) throw new Error('You must specify a name for your test.');
	if (!defectID) throw new Error('You must specify a defectID for your test.');

	params = params || {};

	params = require('underscore').extend({
		include: [], // ['external links']
		onlyOn: {
			// 'types': [], - blog, standard
			// 'country': [], - us, uk, ca
			// 'subs': [] - Controlled Access, Custom Intake Form IV
			// 'statusCodes': ['error']
			activities: []
		},
		resultType: 'message',
		roles: [],
		run: function () {},
		requires: [],
		onError: function () {},
		onPageCrawl: function () {},
		onCrawlFinish: function () {}
	}, params);

	var check = this;

	this.defectID = defectID;
	this.name = name;
	this.id = this.defectID + '-' + this.name;
	this.needsCredentials = false;
	this.needsFollowUp = false;
	this.passed = null;

    
  // Loop through params and add the property to "this" (the check)
  var key;
  for (key in params) {
  	check[key] = params[key];
  }


	// Set default result.data
	switch (params.resultType) {
		case 'message':
			this.data = '';
			break;
		case 'list':
			this.data = [];
			break;
		case 'object':
			this.data = {};
			break;
	}

	if (this.requires.indexOf('crawler') > -1) {
		if (
			this.hasOwnProperty('onPageCrawl') !== true ||
			typeof this.onPageCrawl !== 'function'
		) {
			throw new Error(
				'Please provide an onPageCrawl parameter (a function) for the crawler.'
			);
		}
		if (
			this.hasOwnProperty('onCrawlFinish') !== true ||
			typeof this.onCrawlFinish !== 'function'
		) {
			throw new Error(
				'Please provide an onCrawlFinish parameter (a function) for the crawler.'
			);
		}

		this.run = this.onPageCrawl;
		this._requiresCrawler = true;
	}
}

Check.prototype = {
	/**
	 * Adds a siteData property to each check
	 * If it's a blog, builds out the common blog URLs
	 *
	 * @param url {String} - The main URL of the site
	 * @param siteInfo {Object} - Details about the site that
	 *   would alter the way the check is run
	 */
	setSiteData: function (url, siteInfo) {
		'use strict';

		// Build blog URLs if this is a blog related
		// check and is running against a blog site
		if (
			typeof siteInfo === 'object' &&
			Array.isArray(siteInfo) === false &&
			siteInfo.hasOwnProperty('types') &&
			siteInfo.types.indexOf('blog') > -1 &&
			(
				// Make sure we're all inclusive (type doesn't matter)
				this.onlyOn.hasOwnProperty('types') === false ||
				(
					// If type matters, make sure "blog" is in the list
					this.onlyOn.hasOwnProperty('types') &&
					this.onlyOn.types.indexOf('blog') > -1
				)
			)
		) {

			this.buildBlogURLs(url, siteInfo);

		}

		// Save main URL and siteInfo locally
		this._mainURL = url;
		this._siteInfo = siteInfo;
	},

	/**
	 * The main method for processing a check
	 * Adds a finally handler to the promise to run the check's complete method
	 *
	 * @return {Object} - Q promise resolved when the check is complete
	 */
	exec: function () {
		"use strict";

		// Execute the run method with all supplied arguments
		var promise = this.run.apply(this, arguments)
		
		promise.finally(function () {
				if (!this._requiresCrawler) {
					this.complete();
				}
			}.bind(this)
		);

		return promise;
	},
	getBlogURLs: function () {
		'use strict';
		return this._blogURLs ? this._blogURLs : [];
	},
	getSiteInfo: function () {
		'use strict';
		return this._siteInfo || {};
	},
	getMainURL: function () {
		'use strict';
		return this._mainURL || '';
	},

	/**
	 * Given a url,
	 * # determine if it has a blog
	 * # build out standard blog urls
	 *
	 * @param url {String} - The main URL of the site
	 * @param siteInfo {Objet} - The info associated to this site
	 * @return {Boolean}
	 *  - true if successfully built blog URLs
	 *  - false if not a blog
	 */
	buildBlogURLs: function (url, siteInfo) {
		'use strict';

		var urlData = require('url').parse(url),
			hostname = urlData.protocol + '//' + urlData.hostname;

		if (siteInfo.hasOwnProperty('test_blogURLs')) {
			this._blogURLs = siteInfo.test_blogURLs;
			return true;
		}

		// Check if types is even defined
		if (siteInfo.hasOwnProperty('types')) {
			// Check for slash-blog
			if (siteInfo.types.indexOf('slash_blog') > -1) {
				this._blogURLs = [hostname + '/blog'];
				return true;
			}
			// Check for standalone
			if (siteInfo.types.indexOf('blog') > -1) {
				this._blogURLs = [hostname + '/', hostname + '/Disclaimer.shtml'];
				return true;
			}
		}

		// Do nothing if this is not a blog
		return false;
	},

	/**
	 * Called when the check is done processing the current page
	 *
	 */
	complete: function () {
		"use strict";

		if (this._requiresCrawler) {
			this.onCrawlFinish.apply(this, arguments);
		}

		if (!this.passed) {
			this.passed = false;
		}
	},

	/**
	 * Given the current url and pages already crawled, find referring pages
	 *
	 * @param url {String} - Current page URL
	 * @param pagesCrawled {Object} - The pages already crawled
	 * @return {Array} - A collection of all pages with the url and referrers
	 */
	_findReferrers: function (url, pagesCrawled) {
		'use strict';

		var referrersFound = [];

		var pageURL;
		for (pageURL in pagesCrawled) {
			if (pagesCrawled[pageURL].links.indexOf(url) > -1) {
				referrersFound.push(pageURL);
			}
		}

		return referrersFound;
	},

	/**
	 * Finds all the referring pages for each url given
	 *
	 * @param urls {Array || String} - the url(s) to find referrer for
	 * @param pagesCrawled {Array} - All the pages that have been crawled
	 * @return {Array} - Looks like the following:
	 *     [
						{
							url: "http://dropbox.com",
							referringPages: ["http://google.com", "http://bing.com"]
						},
						{
							url: "http://findlaw.com",
							referringPages: ["http://google.com", "http://bing.com", "http://yahoo.com"]
						},
				 ]
	 */
	referringPages: function (urls, pagesCrawled) {
		"use strict";

		if (!Array.isArray(urls)) urls = [urls];

		return urls.map(function (url) {
			return {
				url: url,
				referringPages: this._findReferrers(url, pagesCrawled)
			};
		}.bind(this));

	},

	/**
	 * Uses cheerio to render a "jQuery like" object
	 *
	 * @param page {Object} - The page object. Expects a type and html property
	 * @return {Object} - Cheerio object representing that page's DOM
	 */
	renderDom: function (html) {
		'use strict';

		var cheerio = require('cheerio');

		try {
			return cheerio.load(html);
		} catch (e) {
			debug('Cheerio failed to parse response for: ' + this.id);
			return cheerio.load('');
		}

		
	},

	/**
	 * Given the set of filters, figure out if this check should run
	 *
	 * @param filters {Object}
	 */
	shouldRun: function (filters) {
		var tests = [];

		var _filter = function (filter) {
			return function (attr) {
				return function (value) {
					var check = this;

					if (check[attr] && check[attr][filter]) {
						if (Array.isArray(check[attr][filter])) {

							if (Array.isArray(value)) {
								if (check[attr][filter].length === 0) return true;

								return value
									.filter(function (type) {
										return (
											check[attr][filter].indexOf(type) > -1
										);
									})
									.length > 0
								;
								

							}

							return check[attr][filter].indexOf(value) < -1;


						}

						if (Array.isArray(value)) {
							return value
								.filter(function (type) {
								  return check[attr][filter] === type;
								})
								.length > 0;
						}

						return check[attr][filter] === value;

					} else {
						return attr === 'onlyOn';
					}
				};
			};
		};

		var filterName;
		for (filterName in filters) {
			var filter = filters[filterName];

			var test = _filter(filterName);
			var onlyTest = test('onlyOn');
			var neverTest = test('neverOn');

			tests.push(onlyTest.call(this, filter) && !neverTest.call(this, filter));
		}

		// help the garbage collector
		filters = _filter = filter = test = onlyTest = neverTest = null;

	  return tests.indexOf(false) === -1;
	}
};

exports.Check = Check;