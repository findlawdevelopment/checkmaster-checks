var async = require('async');
var Q = require('q');
var Check = require('./master').Check;
var urllib = require('url');
var request = require('request');
var _ = require('lodash');
var xml2js = require('xml2js');

module.exports = function () {
  'use strict';

  return new Check('Google Local was carried over from an existing website', 'CM200', {
    resultType: 'object',
    onlyOn: {
      roles: ['Developer', 'QA Member', 'Digital Marketer'],
      activities: [],
      types: ['preview', 'internal']
    },
    run: function (page) {
      var deferred = Q.defer(),
          check = this,
          urls;

      // Create data objects
      check.data = {
        preview: {},
        live: {}
      };

      // Initiate live url's
      urls = this.gatherURLs(page.url);

      // If check passes, there is no live domain, so we should exit
      if (check.passed === true) {
        deferred.resolve();
        return deferred.promise;
      }

      this._request(urls).done(function (responses) {
     
        var parser = new xml2js.Parser();

        /*
          Loop through each response asynchronously and parse the XML

          Generates something like this for preview and live:
          {
            geositemap: {
              present: false,
              reason: "whatever reason"
            },
            kml: {
              present: true
            }
          }
        */

        async.map(_.toArray(responses), function (response, finish) {
          var statusCode = response.response.statusCode;
          var url = response.url;
          var site;
          var property;

          // Determine if this path is on preview/live
          if (url.indexOf('firmsitepreview') > -1 || /admin\.[^\.]+\.lawoffice\.com/.test(url) === true) {
            site = check.data.preview;
          } else {
            site = check.data.live;
          }

          // Save results in appropriate type
          if (url.indexOf('geositemap') > -1) {
            property = 'geositemap';
          } else {
            property = 'kml';
          }

          // Create property object
          site[property] = {
            url: url
          };

          // Mark as not present if error occurred during request
          if (response.error) {
            site[property].present = false;
            site[property].reason = 'An error occurred during request: ' + response.error.message;
            finish();
            return false;
          }

          // Mark as not present if a bad status occurs
          if (statusCode !== 200) {
            site[property].present = false;
            site[property].reason = 'Bad status: ' + statusCode;
            finish();
            return false;
          }

          // Parse XML
          parser.parseString(response.body, function (err, json) {

            // Mark as not present if an error occurs
            if (err) {
              site[property].present = false;
              site[property].reason = 'An error occurred when parsing XML: ' + err.message;
              finish();
              return false;
            }

            // Mark as not present if certain XML attributes are not found
            if (property === 'geositemap' && (!json || json.hasOwnProperty('urlset') !== true)) {
              site[property].present = false;
              site[property].reason = 'Incorrect format for geositemap.xml';
              finish();
              return false;
            }

            if (property === 'kml' && (!json || json.hasOwnProperty('kml') !== true)) {
              site[property].present = false;
              site[property].reason = 'Incorrect format for locations.kml';
              finish();
              return false;
            }

            // If everything looks good, mark the file as being present
            site[property].present = true;

            finish();
          });
        }, function () {
          // Fail check if the live site has either of the files but the preview site does not
          if (
            check.data.live.geositemap.present === true && check.data.preview.geositemap.present === false ||
            check.data.live.kml.present === true && check.data.preview.kml.present === false
          ) {
            check.passed = false;
          } else {
            // If neither condition is true, pass the check
            check.passed = true;
          }

          deferred.resolve();
        });
       
      }, function(error) {

        check.passed = false;
        deferred.reject(error);

      });

      return deferred.promise;
    },
    _request: function(urls) {
      
      var deferred = Q.defer(), results = {}, t = urls.length, c = 0,
          handler = function (error, response, body) {

            var url = response.request.uri.href;
              
            results[url] = {
              url: url,
              error: error,
              response: response,
              body: body
            };
                               
            if (++c === urls.length) { deferred.resolve(results); }
       
          }; 
     
      while (t--) { 

        request.get(urls[t], handler);

      }

      return deferred.promise;

    },
    gatherURLs: function(url) {
      // Set preview url's
      var urls = [
        urllib.resolve(url, '/geositemap.xml'),
        urllib.resolve(url, '/locations.kml')
      ];

      var liveURL = this.getSiteInfo().domain;

      if (liveURL) {

        // Add http protocol since the service omits it
        liveURL = 'http://' + liveURL;

        // Push live url's to the main url array
        urls.push(
          urllib.resolve(liveURL, '/geositemap.xml'),
          urllib.resolve(liveURL, '/locations.kml')
        );

        return urls;

      } else {
        this.passed = true;
      }
    }
  });
};