# -*- mode: ruby -*-
# vi: set ft=ruby :

# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  # All Vagrant configuration is done here. The most common configuration
  # options are documented and commented below. For a complete reference,
  # please see the online documentation at vagrantup.com.

  # Enable use of local SSH config/keys
  config.ssh.forward_agent = true

  # Every Vagrant virtual environment requires a box to build off of.
  config.vm.box = "node-20140903"

  # The url from where the 'config.vm.box' box will be fetched if it
  # doesn't already exist on the user's system.
  config.vm.box_url = "http://fldev.int.thomson.com:8115/node/node-20140903.box"

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  config.vm.network :forwarded_port, guest: 3093, host: 3093
  config.vm.network :forwarded_port, guest: 8093, host: 8093

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional thirdvsg
  # argument is a set of non-required options.
  config.vm.synced_folder ".", "/srv/sites/checks"

  # You MUST have a ~/.ssh/bitbucket_deployment_key (BitBucket specific) SSH key to copy to VM
  if File.exists?(File.join(Dir.home, ".ssh", "bitbucket_deployment_key"))
      # Read local machine's BitBucket SSH Key (~/.ssh/bitbucket_deployment_key)
      BitBucket_ssh_key = File.read(File.join(Dir.home, ".ssh", "bitbucket_deployment_key"))
      # Copy it to VM as the /root/.ssh/id_rsa key
      config.vm.provision :shell, :inline => "echo 'Copying local BitBucket SSH Key to VM for provisioning...' && mkdir -p /root/.ssh && echo '#{BitBucket_ssh_key}' > /root/.ssh/id_rsa && chmod 600 /root/.ssh/id_rsa"
  else
      # Else, throw a Vagrant Error. Cannot successfully startup on Windows without a BitBucket SSH Key!
      raise Vagrant::Errors::VagrantError, "\n\nERROR: BitBucket SSH Key not found at ~/.ssh/bitbucket_deployment_key.\nYou have to generate this key manually. see https://thehub.thomsonreuters.com/docs/DOC-766505?sr=stream&ru=19186"
  end
  
  config.vm.provision :shell, :path => "post-install.sh"

  # Run optional provisioning
  if File.exist?("user-post-install.sh")
    config.vm.provision :shell, :path => "user-post-install.sh"
  end


end
