#!/bin/bash

GREEN="\e[0;32m"
RED="\e[0;31m"
BOLD="\e[1;30m"
COLOREND="\e[0m"

echo -e "\n"
echo -e "${BOLD}* NOTICE *: ${COLOREND}You may see some warnings regarding ${RED}dpkg-preconfigure${COLOREND} errors as well as ${RED}make/clock skew${COLOREND} errors. You can safely ignore these messages.\n"

# Update APT package list
echo -e "${GREEN}Updating APT package list...${COLOREND}"
apt-get update > /dev/null


echo -e "${GREEN}Installing jpegoptim...${COLOREND}"
sudo apt-get install -y jpegoptim > /dev/null

echo -e "${GREEN}Installing pngcrush...${COLOREND}"
sudo apt-get install -y pngcrush > /dev/null

echo -e "${GREEN}Installing gifsicle...${COLOREND}"
sudo apt-get install -y gifsicle > /dev/null


# Apply SSH configuration to bypass host check
echo -e "${GREEN}Applying SSH configuration...${COLOREND}"
mkdir -p /root/.ssh
echo "" >> /root/.ssh/config
echo "Host bitbucket.org" >> /root/.ssh/config
echo "    StrictHostKeyChecking no" >> /root/.ssh/config
echo ""
# changes git protocol to ssh
echo "Host github.com" >> /root/.ssh/config
echo "    User git" >> /root/.ssh/config
echo "    Hostname ssh.github.com" >> /root/.ssh/config
echo "    Port 443" >> /root/.ssh/config

echo -e "${GREEN}Applying Git configuration...${COLOREND}"
git config --global url."https://".insteadOf git://

# Install Redis
apt-get install wget
mkdir -p tmp
cd tmp
wget http://download.redis.io/releases/redis-2.8.17.tar.gz
tar xzf redis-2.8.17.tar.gz
cd redis-2.8.17
make
make install
cd ../../
rm -r tmp

# Remove all node_modules
# Install application
echo -e "${GREEN}Installing Node modules (this may take a while)...${COLOREND}"
(cd /srv/sites/checks && rm -rf node_modules && npm install --silent > /dev/null)

echo -e "${GREEN}DONE!${COLOREND}"