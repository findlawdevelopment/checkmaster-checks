var gulp   = require('gulp')
  , server = require('gulp-express')
  , exec   = require('child_process').exec
;

gulp.task('startRedis', function () {
  /* Check if redis-server already running */
  exec('pgrep redis-server && echo Running', function (err, stdout, stderr) {
    /* if no output, meaning not running */
    if (!stdout) {
      exec('redis-server', function (err, stdout, stderr) {
        if (err) {
          console.log(err, stdout, stderr);
        }
      });
    }
  });
});


gulp.task('startExpress', function () {
  server.run({
    file: './server/index.js',
    env: 'development'
  });

});



gulp.task('watch', function () {
  gulp.watch(['./server/**/*.js', './checks/**/*.js'], ['startExpress']);
});

gulp.task('default', ['watch', 'startRedis', 'startExpress']);