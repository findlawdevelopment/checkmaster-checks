'use strict';

var app = require('express')()
  , server = require('http').createServer(app)
  , io = require('socket.io')(server)
  , cluster = require('cluster')
  , numCPUs = require('os').cpus().length
;

var ENV = process.env.NODE_ENV;
process.env.NODE_ENV = ENV || 'development';


if (cluster.isMaster) {
  var config = require('./config/express')(app);
  var routes = require('./routes')(app);

  var jobQueue = require('./jobs/queue')(io);

  var i;
  for (i = 0; i < numCPUs; i++) {
    if ('test' !== ENV) cluster.fork();
  }

  cluster.on('exit', function(worker, code, signal) {
    console.log('worker ' + worker.process.pid + ' died');
  });


}

if (!cluster.isMaster || 'test' === ENV) {
  var jobProcessor = require('./jobs/processor');

  server.listen(3093, function () {
    console.log('listening on port: 3093');
  });
}

/* server monitoring
  * Need api key
  var nodetime = require('nodetime');
*/



module.exports.app = exports.app = app;