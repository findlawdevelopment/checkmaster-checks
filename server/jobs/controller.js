var constants = require('./constants')
  , debug = require('debug')('queue')
;

function CheckJob () {
  
}

/**
 * Callback function to process jobs
 *
 * @param job {Object} - The job to be processed
 * @param done {Function} - When called, tells kue that the job is complete
 */
CheckJob.processJob = function (job, done) {
  var loadChecks = require('../../checks');

  var details = job.details;
  var promises = loadChecks(details.filters);
  var total = promises.length;
  var done = 0;
  var results = {};

  promises.forEach(function (check) {
    
    check.exec(details).finally(function (results) {
      done++;
      job.progress(done, total);
      results[check.id] = results;
    }).done();

  });

  Q.allSettled(promises)
    .done(function () {
      // finish the kue job, calls the onComplete handler
      done(null, results);
    })
  ;
};

/**
 * CheckJob Class
 * ---
 * Handles logic for creating a new check processing job
 *
 * @class CheckJob
 * @param jobs {Object} - The jobs object created from `kue.createQueue()`
 * @param socket {Object} - The socket.io object to communicate with the client
 * @param details {Object} - The details to add to the job
 */
CheckJob.Job = function (socket, details) {
  this.socket = socket;
  this.details = details;
};


CheckJob.Job.prototype.validate = function (details) {
  var tests = [];
  
  if (details.page) {
    tests.push(!!details.page.url);
    tests.push(!!details.page.body);
  }

  return tests.indexOf(false) === -1;
};

var reportError = function (err) {
  debug('Failed to process checks for: ' + this.details.page.url);
  debug('The following error occured: ' + err);
};

/**
 * Error handler for when a job fails, but still has attempts left
 *
 * @method onError
 * @param err {Object}
 */
CheckJob.Job.prototype.onError = function (err) {
  reportError(err);

  this.socket.emit(constants.FAILEDATTEMPT, {
    page: this.details.page.url,
    error: err
  });
};


/**
 * Error handler for when there are no more attempts left
 *
 * @method onFinalError
 * @param err {Object}
 */
CheckJob.Job.prototype.onFinalError = function (err) {
  reportError(err);

  this.socket.emit(constants.FAILED, {
    page: this.details.page.url,
    error: err
  });
};

/**
 * Success handler for when a job is successfully completed
 *
 * @method onComplete
 * @param results {Object}
 */
CheckJob.Job.prototype.onComplete = function (results) {

  this.socket.emit(constants.COMPLETE, results);

};

/**
 * Makes a new job
 *
 * @method addJob
 * @param name {String} - The name of the job
 * @returns {Object} - The job that is created
 */
CheckJob.Job.prototype.addJob = function (name) {
  debug('adding new %s job', name);
  var config = require('../config/environment');
  var jobs = require('kue').createQueue({redis: config.redis || {}});

  if (!this.validate(this.details)) {
    throw new Error('You did not pass in the necessary details');
    return;
  }

  var job = jobs
    .create(name, this.details)

    .attempts(config.kue.failAttempts)

    .on(constants.FAILEDATTEMPT, this.onError)

    .on(constants.FAILED, this.onFinalError)

    .on(constants.COMPLETE, this.onComplete)
  ;

  this.jobID = job.id;
  return job;
};

module.exports = CheckJob;