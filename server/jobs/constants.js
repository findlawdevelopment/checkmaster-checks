module.exports = {
  RUN           : 'run'
, FAILED        : 'failed'
, FAILEDATTEMPT : 'failed attempt'
, COMPLETE      : 'complete'
};
