'use strict';

var kue = require('kue')
  , config = require('../config/environment')
  , jobs = kue.createQueue({ redis: config.redis || {} })
  , Q = require('Q')
;

var CheckJob = require('./controller');

jobs.process('run checks', 5, CheckJob.processJob);