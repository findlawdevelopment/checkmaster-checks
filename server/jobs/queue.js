'use strict';

var kue    = require('kue')
  , config = require('../config/environment')
  , jobs   = kue.createQueue({ redis: config.redis || {} })
;

var constants = require('./constants');
var CheckJob  = require('./controller');

if ('development' === process.env.NODE_ENV) {
  require('debug')('queue')('Starting kue server on port ' + config.kue.port);
  kue.app.listen(config.kue.port);
}

module.exports = function (io) {

  io.on('connection', function (socket) {

    socket.on(constants.RUN, function (details) {

      var checkJob = new CheckJob.Job(jobs, socket, details);
      checkJob.addJob('run checks');

    });

  });

};