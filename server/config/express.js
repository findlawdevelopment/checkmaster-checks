'use strict';

var config = require('./environment');

/* middleware */
var bodyParser = require('body-parser');

module.exports = function (app) {
  app.set('views', config.root + '/views');
  app.engine('html', require('ejs').renderFile);
  app.set('view engine', 'html');

  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json());

  if ('development' === app.get('env')) {
    app.use(require('connect-livereload')());
  }
};