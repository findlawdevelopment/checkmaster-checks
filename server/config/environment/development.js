'use strict';

process.env.DEBUG = 'queue loader';
process.env.NODE_DEBUG = 'cluster';

module.exports = {
  kue: {
    port: 8093
  }
};