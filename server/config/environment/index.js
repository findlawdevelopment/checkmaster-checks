'use strict';

var merge = require('lodash').merge;
var path = require('path');

var config = {
  env: process.env.NODE_ENV,

  root: path.normalize(__dirname + '../../../'),

  port: process.env.PORT || 3093,

  kue: {
    failAttempts: 3
  },

  screenshotDir: __dirname + "../../screenshots"
};

module.exports = merge(
  config,
  require('./' + process.env.NODE_ENV + '.js' || {})
);