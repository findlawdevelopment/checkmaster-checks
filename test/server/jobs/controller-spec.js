var sinon = require('sinon')
  , kue = require('kue')
;

var CheckJob, checkJob, jobs, mockSocket, details;

describe('Check Job Controller', function () {

  beforeEach(function () {
    jobs = {
      create: function () {},
      on: function () {},
      attempts: function () {}
    };

    sinon.stub(jobs, 'create').returns(jobs);
    sinon.stub(jobs, 'on').returns(jobs);
    sinon.stub(jobs, 'attempts').returns(jobs);

    sinon.stub(kue, 'createQueue').returns(jobs);

    mockSocket = {
      on: function () {},
      emit: function () {}
    };

    sinon.stub(mockSocket, 'on').returns(mockSocket);
    sinon.stub(mockSocket, 'emit').returns(mockSocket);

    details = {
      page: {
        url: "http://google.com",
        body: '<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8" /><title>Document</title></head><body></body></html>'
      }
    };

    delete require.cache[require.resolve('../../../server/jobs/controller')];
    CheckJob  = require('../../../server/jobs/controller');
    checkJob = new CheckJob.Job(jobs, mockSocket, details);
  });

  afterEach(function () {
    kue.createQueue.restore();
    jobs.create.restore();
    jobs.on.restore();
    jobs.attempts.restore();

    mockSocket.on.restore();
    mockSocket.emit.restore();
  });

  describe('Add method', function () {


    it('Adds a "run check" job to the queue', function () {
      checkJob.addJob('run check');
      jobs.create.calledWith('run check').should.be.true;
      jobs.attempts.calledWith(3).should.be.true;
      jobs.on.callCount.should.eql(3);
    });
  });

  describe('onFinalError method', function () {
    it('reports the error over socket.io', function () {
      
    });
  });

  describe('validate method', function () {
    it('passes validation', function () {
      checkJob.validate(details).should.be.true;
    });
  });
});