var app = require('../../server').app
  , request = require('supertest')
;



describe('GET /', function () {
  it('responds with a helpful html page', function () {
    request(app)
      .get('/')
      .expect(200)
      .end(function (err, res) {
        if (err) return done(err);
        res.body.indexOf('html').should.not.eql(-1);

        delete require.cache;
        console.log('cache: ' + require.cache);
        require.cache = {};
        done();

      });
  });
});
