var path = require('path');
var fs = require('fs-extra');

var CHECKS_PATH = path.join(__dirname, '../checks'),
	TEST_CHECKS_PATH = path.join(__dirname, '../checks-tests');
	

function updateChecks() {
	/*
	| Load test checks into checks folder
	*/
	fs.removeSync(path.join(TEST_CHECKS_PATH, 'index.js'));
	fs.removeSync(path.join(TEST_CHECKS_PATH, 'master.js'));
	// Copy master and index
	fs.copySync(path.join(CHECKS_PATH, 'index.js'), path.join(TEST_CHECKS_PATH, 'index.js'));
	fs.copySync(path.join(CHECKS_PATH, 'master.js'), path.join(TEST_CHECKS_PATH, 'master.js'));
}

exports.updateChecks = updateChecks;