var urllib = require('url');
var cheerio = require('cheerio');

var Page = function (data) {
  'use strict';

  var key;

  for (key in data) {
    this[key] = data[key];
  }
};

Page.prototype = {
  dom: function () {
    'use strict';

    var $;

    // Cheerio can kill the process during parsing, make sure it doesn't
    try {
      $ = cheerio.load(this.html);
    } catch (e) {
      console.error('Cheerio parsing error: ' + this.url);
    }

    if ($ === undefined) {
      $ = cheerio.load('');
    }

    return $;
  }
};

module.exports = function (url, referrer) {
  return new Page({
    url: url,
    urlData: urllib.parse(url),
    headers: {},
    html: '<div><p>Mock HTML</p></div>',
    isExternal: false,
    links: [],
    redirects: [],
    referrer: referrer,
    statusCode: 200,
    type: 'text/html'
  });
};