/*jshint -W030 */
/*global beforeEach, describe, it */

var mockPage = require('../mocks/page');

describe('CM002 - no tel links check', function () {
  'use strict';

  var check;
  var page;

  beforeEach(function () {
    check = require('../../checks/CM002-tel-link-check')();
    page = mockPage('http://google.com');
  });

  it('passes when no tel links are present', function () {
    page.html = '<!doctype html><html lang="en"><head><meta charset="UTF-8" /><title>Document</title></head><body><a href="/">Home</a></body></html>';
    check.onPageCrawl(page);
    (check.passed === null).should.be.true;
  });

  it('fails when one ore more tel links are present', function () {
    page.html = '<!doctype html><html lang="en"><head><meta charset="UTF-8" /><title>Document</title></head><body><a href="tel:1234567">1234567</a></body></html>';
    check.onPageCrawl(page);
    check.passed.should.be.false;
  });

  it('adds link data to check.data on failure', function () {
    page.html = '<!doctype html><html lang="en"><head><meta charset="UTF-8" /><title>Document</title></head><body><a href="tel:1234567">1234567</a><a href="tel:1029384">1029384</a></body></html>';
    check.onPageCrawl(page);
    check.data.should.eql([{
      url: 'http://google.com',
      reason: 'There is a link with tel: on the site',
      allLinks: [{
        href: 'tel:1234567',
        text: '1234567'
      }, {
        href: 'tel:1029384',
        text: '1029384'
      }]
    }]);
  });

});
