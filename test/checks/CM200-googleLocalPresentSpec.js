/* jshint strict: true */
/*global beforeEach, describe, it */

/* Mocha plugins */
var sinon = require('sinon');

// REQUIRED
var Q = require('q');
var request = require('request');
// END-REQUIRED

describe('Unit: google local check', function() {
  'use strict';

  var beforeResponseCallbacks;
  var check;
  var checkForLiveSiteSpy;
  var mockBody;
  var mockError;
  var mockPreviewGeositemapXML;
  var mockPreviewLocationsXML;
  var mockLiveGeositemapXML;
  var mockLiveLocationsXML;
  var mockRequest;
  var mockStatusCode;
  var siteInfo;
  var urls;

  var GOOD_GEO_XML = '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:geo="http://www.google.com/geo/schemas/sitemap/1.0"></urlset>';
  var GOOD_LOCATIONS_KML = '<?xml version="1.0" encoding="UTF-8"?><kml xmlns="http://www.opengis.net/kml/2.2"></kml>';

  function beforeResponse(callback) {
    beforeResponseCallbacks.push(callback);
  }

  beforeEach(function() {
    beforeResponseCallbacks = [];
    mockBody = '';
    mockPreviewGeositemapXML = GOOD_GEO_XML;
    mockPreviewLocationsXML = GOOD_LOCATIONS_KML;
    mockLiveGeositemapXML = GOOD_GEO_XML;
    mockLiveLocationsXML = GOOD_LOCATIONS_KML;
    mockError = null;
    mockStatusCode = 200;
    mockRequest = function (url, callback) {
      // Return correct body based on domain and filename
      if (url.indexOf('geositemap') > -1) {
        if (url.indexOf('firmsitepreview') > -1 || url.indexOf('admin.') > -1) {
          mockBody = mockPreviewGeositemapXML;
        } else {
          mockBody = mockLiveGeositemapXML;
        }
      } else {
        if (url.indexOf('firmsitepreview') > -1 || url.indexOf('admin.') > -1) {
          mockBody = mockPreviewLocationsXML;
        } else {
          mockBody = mockLiveLocationsXML;
        }
      }

      // Execute some code before the callback (conditional changes to response)
      beforeResponseCallbacks.forEach(function (beforeCallback) {
        beforeCallback(url);
      });

      callback(mockError, {
        statusCode: mockStatusCode,
        request: {
          uri: {
            href: url
          }
        }
      }, mockBody);
    };

    sinon.stub(request, 'get', function () {
      mockRequest.apply(this, arguments);
    });

    check = require('../../checks/CM200-google-local-present-check.js')();
  });

  afterEach(function () {
    request.get.restore();
  });

  it('considers firmsitepreview URLs as the preview site', function (done) {
    check.setSiteData('http://domnitzlaw2.firmsitepreview.com', {
      domain: 'www.domnitzlaw.com'
    });

    mockPreviewGeositemapXML = '';

    check.exec({ url: 'http://domnitzlaw2.firmsitepreview.com' }).then(function () {
        (check.data.preview === undefined).should.be.false;
        check.data.preview.geositemap.present.should.be.false;
        done();
      }, done).done();
  });

  it('considers admin.*.lawoffice.com URLs as the preview site', function (done) {
    check.setSiteData('http://admin.domnitzlaw2.lawoffice.com', {
      domain: 'www.domnitzlaw.com'
    });

    mockPreviewGeositemapXML = '';

    check.exec({ url: 'http://admin.domnitzlaw2.lawoffice.com' }).then(function () {
        (check.data.preview === undefined).should.be.false;
        check.data.preview.geositemap.present.should.be.false;
        done();
      }, done).done();
  });

  describe('passing conditions', function () {

    it('when geositemap.xml and locations.kml exist on both preview and live', function (done) {
      check.setSiteData('http://domnitzlaw2.firmsitepreview.com', {
        domain: 'www.domnitzlaw.com'
      });

      check.exec({ url: 'http://domnitzlaw2.firmsitepreview.com' }).then(function() {
        check.passed.should.be.true;
        done();
      }, done).done();
    });

    it('when the live domain does not exist', function (done) {
      check.setSiteData('http://domnitzlaw2.firmsitepreview.com', {});

      check.exec({ url: 'http://domnitzlaw2.firmsitepreview.com' }).then(function() {
        check.passed.should.be.true;
        done();
      }, done).done();
    });

    it('when the live domain does not contain the geositemap.xml or the locations.kml file (even if the preview has them)', function (done) {
      check.setSiteData('http://domnitzlaw2.firmsitepreview.com', {
        domain: 'www.domnitzlaw.com'
      });
      mockLiveGeositemapXML = '';
      mockLiveLocationsXML = '';

      check.exec({ url: 'http://domnitzlaw2.firmsitepreview.com' }).then(function() {
        check.passed.should.be.true;
        done();
      }, done).done();
    });
  
  });

  describe('failing conditions', function () {

    beforeEach(function () {
      check.setSiteData('http://domnitzlaw2.firmsitepreview.com', {
        domain: 'www.domnitzlaw.com'
      });
    });

    describe('when the live site has a geositemap.xml and', function () {
    
      it('the preview site returns an error', function (done) {
        beforeResponse(function (url) {
          if (url.indexOf('firmsitepreview') > -1 && url.indexOf('geositemap') > -1) {
            mockError = new Error('some error');
          } else {
            mockError = null;
          }
        });
        check.exec({ url: 'http://domnitzlaw2.firmsitepreview.com' }).then(function() {
          check.passed.should.be.false;
          check.data.preview.geositemap.reason.should.eql('An error occurred during request: some error');
          done();
        }, done).done();
      });

      it('the preview site contains bad XML', function (done) {
        mockPreviewGeositemapXML = '<a';
        check.exec({ url: 'http://domnitzlaw2.firmsitepreview.com' }).then(function() {
          check.passed.should.be.false;
          check.data.preview.geositemap.reason.should.eql('An error occurred when parsing XML: Unclosed root tag\nLine: 0\nColumn: 2\nChar: ');
          done();
        }, done).done();
      });

      it('the preview site returns a bad status code', function (done) {
        beforeResponse(function (url) {
          if (url.indexOf('firmsitepreview') > -1 && url.indexOf('geositemap') > -1) {
            mockStatusCode = 404;
          } else {
            mockStatusCode = 200;
          }
        });
        check.exec({ url: 'http://domnitzlaw2.firmsitepreview.com' }).then(function() {
          check.passed.should.be.false;
          check.data.preview.geositemap.reason.should.eql('Bad status: 404');
          done();
        }, done).done();
      });

      it('the preview site geositemap.xml is not the correct format', function (done) {
        mockPreviewGeositemapXML = '<noturlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:geo="http://www.google.com/geo/schemas/sitemap/1.0"></noturlset>';
        check.exec({ url: 'http://domnitzlaw2.firmsitepreview.com' }).then(function() {
          check.passed.should.be.false;
          check.data.preview.geositemap.reason.should.eql('Incorrect format for geositemap.xml');
          done();
        }, done).done();
      });
    
    });

    describe('when the live site has a locations.kml and', function () {
    
      it('the preview site returns an error', function (done) {
        beforeResponse(function (url) {
          if (url.indexOf('firmsitepreview') > -1 && url.indexOf('locations.kml') > -1) {
            mockError = new Error('some error');
          } else {
            mockError = null;
          }
        });
        check.exec({ url: 'http://domnitzlaw2.firmsitepreview.com' }).then(function() {
          check.passed.should.be.false;
          check.data.preview.kml.reason.should.eql('An error occurred during request: some error');
          done();
        }, done).done();
      });

      it('the preview site contains bad XML', function (done) {
        mockPreviewLocationsXML = '<a';
        check.exec({ url: 'http://domnitzlaw2.firmsitepreview.com' }).then(function() {
          check.passed.should.be.false;
          check.data.preview.kml.reason.should.eql('An error occurred when parsing XML: Unclosed root tag\nLine: 0\nColumn: 2\nChar: ');
          done();
        }, done).done();
      });

      it('the preview site returns a bad status code', function (done) {
        beforeResponse(function (url) {
          if (url.indexOf('firmsitepreview') > -1 && url.indexOf('locations.kml') > -1) {
            mockStatusCode = 404;
          } else {
            mockStatusCode = 200;
          }
        });
        check.exec({ url: 'http://domnitzlaw2.firmsitepreview.com' }).then(function() {
          check.passed.should.be.false;
          check.data.preview.kml.reason.should.eql('Bad status: 404');
          done();
        }, done).done();
      });

      it('the preview site locations.kml is not the correct format', function (done) {
        mockPreviewLocationsXML = '<?xml version="1.0" encoding="UTF-8"?><notkml xmlns="http://www.opengis.net/kml/2.2"></notkml>';
        check.exec({ url: 'http://domnitzlaw2.firmsitepreview.com' }).then(function() {
          check.passed.should.be.false;
          check.data.preview.kml.reason.should.eql('Incorrect format for locations.kml');
          done();
        }, done).done();
      });
    
    });
  
  });

});