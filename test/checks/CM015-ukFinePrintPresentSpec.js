/* jshint strict: true */
/*global beforeEach, describe, expect, it */

// REQUIRED
var cheerio = require('cheerio');
var request = require('request');
var Q = require('q');
var sinon = require('sinon');
// END-REQUIRED

describe('uk fine print present check', function () {
	'use strict';

	var check;

	beforeEach(function () {
		check = require('../../checks/CM015-uk-fine-print-present-check')();
	});
	
	describe('a UK Firmsite', function() {
		'use strict';

  	var url, siteInfo, response, responseBody, requestSpy, result;

  	beforeEach(function () {
  		url = 'http://mb-sol.firmsitepreview.com';
  		siteInfo = {
				onlyOn: {
					types: ['blog', 'standard'],
					country: ['uk']
				},
				domain: 'www.baker-law.co.uk'
			};
			response = {
				statusCode: 200
			};
			sinon.stub(request, 'get', function (url, callback) {
      	callback(null, response, responseBody);
      });
		});

  	afterEach(function () {
	    request.get.restore();
  	});

		describe('needsFollowUp', function() {
			'use strict';

			it('Lawyer Marketing UK website link, FindLaw UK link and no cookie validation', function () {				
				responseBody = '<p id="branding"><a target="_blank" rel="nofollow" href="http://www.lawyermarketinguk.co.uk">Business Development Solutions</a> by <a target="_blank" href="http://www.findlaw.co.uk/">FindLaw</a>, a Thomson Reuters business.</p>';
				result = check.exec({ url: url }, siteInfo).then(function () {
					check.needsFollowUp.should.be.true;
				});
			});

			it('no Lawyer Marketing UK website link, FindLaw UK link and no cookie validation', function () {				
				responseBody = '<p id="branding"><a target="_blank" href="http://www.findlaw.co.uk/">FindLaw</a></p>';
				result = check.exec({ url: url }, siteInfo).then(function () {
					check.needsFollowUp.should.be.false;
				});
			});

			it('Lawyer Marketing UK website link, no FindLaw UK link and no cookie validation', function () {
				responseBody = '<p id="branding"><a target="_blank" rel="nofollow" href="http://www.lawyermarketinguk.co.uk">Business Development Solutions</a></p>';
				result = check.exec({ url: url }, siteInfo).then(function () {
					check.needsFollowUp.should.be.false;
				});
			});

			it('no Lawyer Marketing UK website link, no FindLaw UK link and no cookie validation', function () {
				responseBody = '';	
				result = check.exec({ url: url }, siteInfo).then(function () {
					check.needsFollowUp.should.be.false;
				});
			});

		});

		describe('check fails', function() {
			'use strict';

			it('Lawyer Marketing UK website link is present but no FindLaw UK link', function () {				
				responseBody = '<p id="branding"><a target="_blank" rel="nofollow" href="http://www.lawyermarketinguk.co.uk">Business Development Solutions</a> <script type="text/javascript" src="/includes/cookieVerification.js"></script></p>';
				result = check.exec({ url: url }, siteInfo).then(function () {
					check.passed.should.be.false;
				});
			});

			it('FindLaw UK link is present but no Lawyer Marketing UK link', function () {				
				responseBody = '<p id="branding"><a target="_blank" href="http://www.findlaw.co.uk/">FindLaw</a> <script type="text/javascript" src="/includes/cookieVerification.js"></script></p>';
				result = check.exec({ url: url }, siteInfo).then(function () {
					check.passed.should.be.false;
				});
			});

		});

		describe('check passes', function() {
			'use strict';

			it('the Lawyer Marketing UK website link and FindLaw UK link and cookie verification script are present', function () {				
				responseBody = '<p id="branding"><a href="/Privacy-Policy.shtml">Privacy Policy</a> | <a target="_blank" rel="nofollow" href="http://www.lawyermarketinguk.co.uk">Business Development Solutions</a> by <a target="_blank" href="http://www.findlaw.co.uk/">FindLaw</a>, a Thomson Reuters business. <script type="text/javascript" src="/includes/cookieVerification.js"></script> </p>';
				result = check.exec({ url: url }, siteInfo).then(function () {
					check.passed.should.be.true;
				});
			});

		});

	});

});