
/* jshint strict: true */
/*global beforeEach, describe, it */

/* Mocha plugins */


// REQUIRED
var Q = require('q');


describe('CM101 - seo title meta check', function () {
  'use strict';

  var check;
  var defaultPageDesc;
  var page;

  beforeEach(function () {
    check = require('../../checks/CM101-seo-title-meta-check.js')(); // default html that the crawler will return
    check._siteInfo = {};
    check._siteInfo.numbers = {};
    defaultPageDesc = 'My page description that is hopefully long enough. 123-456-7890';
    page = {
      url: 'http://www.google.com/page',
      type: 'text/html'
    };
    page.html = '<!doctype html><html lang="en"><head><meta charset="UTF-8" /><title>My Title Text</title><meta name="description" content="' + defaultPageDesc + '"><meta name="keywords" content="My page keywords."></head><h1>My Page Title</h1><body></body></html>';
  });

  it('logs the url in check.data', function () {
    check.onPageCrawl(page, {}, {});
    check.data[0].url.should.equal(page.url);
  });

  it('logs the pageTitle in check.data', function () {
    check.onPageCrawl(page, {}, {});
    check.data[0].pageTitle.should.equal('My Page Title');
  });

  it('only applies to text/html pages', function () {
    page.type = 'application/pdf';
    check.onPageCrawl(page, {}, {});
    (check.passed === null).should.be.true;
    check.data.should.have.lengthOf(0);
  });

  it('only applies to internal pages', function () {
    page.isExternal = true;
    check.onPageCrawl(page, {}, {});
    (check.passed === null).should.be.true;
    check.data.should.have.lengthOf(0);
  });

  describe('title verification', function () {

    describe('successful <title> tag text retrieval', function () {

      beforeEach(function () {
        // Using default page.html in first beforeEach
        check.onPageCrawl(page, {}, {});
      });

      it('does not fail the check', function () {
        (check.passed === null).should.be.true;
      });

      it('logs the title in check.data', function () {
        check.data[0].title.text.should.equal('My Title Text');
      });

      it('logs no error in check.data', function () {
        (check.data[0].title.error === undefined).should.be.true;
      });

    });

    describe('missing <title> tag', function () {

      beforeEach(function () {
        page.html = '<!doctype html><html lang="en"><head><meta charset="UTF-8" /></head><body></body></html>';
        check.onPageCrawl(page, {}, {});
      });

      it('fails the check', function () {
        check.passed.should.be.false;
      });

      it('logs the title in check.data', function () {
        check.data[0].title.text.should.equal('(missing)');
      });

      it('logs the error in check.data', function () {
        check.data[0].title.error.should.equal(check.ERRORS.title.missing);
      });

    });

    describe('empty <title> tag', function () {

      beforeEach(function () {
        page.html = '<!doctype html><html lang="en"><head><meta charset="UTF-8" /><title> </title></head><body></body></html>';
        check.onPageCrawl(page, {}, {});
      });

      it('fails the check', function () {
        check.passed.should.be.false;
      });

      it('logs the title in check.data', function () {
        check.data[0].title.text.should.equal('(empty)');
      });

      it('logs the error in check.data', function () {
        check.data[0].title.error.should.equal(check.ERRORS.title.empty);
      });

    });

    describe('<title> tag with only the page title', function () {

      beforeEach(function () {
        page.html = '<!doctype html><html lang="en"><head><meta charset="UTF-8" /><title>Page Title</title><h1>\n  Page Title\n</h1></head><body></body></html>';
        check.onPageCrawl(page, {}, {});
      });

      it('fails the check', function () {
        check.passed.should.be.false;
      });

      it('logs the title in check.data', function () {
        check.data[0].title.text.should.equal('Page Title');
      });

      it('logs the error in check.data', function () {
        check.data[0].title.error.should.equal(check.ERRORS.title.onlyPageTitle);
      });

    });

    describe('<title> tag with "Content Page" text', function () {

      beforeEach(function () {
        page.html = '<!doctype html><html lang="en"><head><meta charset="UTF-8" /><title>Content Page</title></head><body></body></html>';
        check.onPageCrawl(page, {}, {});
      });

      it('fails the check', function () {
        check.passed.should.be.false;
      });

      it('logs the title in check.data', function () {
        check.data[0].title.text.should.equal('Content Page');
      });

      it('logs the error in check.data', function () {
        check.data[0].title.error.should.equal(check.ERRORS.title.contentPage);
      });

    });

  });

  describe('meta description verification', function () {

    describe('successful <meta> description text retrieval', function () {

      beforeEach(function () {
        // Using default page.html in first beforeEach
        check.onPageCrawl(page, {}, {});
      });

      it('does not fail the check', function () {
        (check.passed === null).should.be.true;
      });

      it('logs the title in check.data', function () {
        check.data[0].description.text.should.equal(defaultPageDesc);
      });

      it('logs no error in check.data', function () {
        (check.data[0].description.error === undefined).should.be.true;
      });

    });

    describe('missing <meta> description', function () {

      beforeEach(function () {
        page.html = '<!doctype html><html lang="en"><head><meta charset="UTF-8" /></head><body></body></html>';
        check.onPageCrawl(page, {}, {});
      });

      it('fails the check', function () {
        check.passed.should.be.false;
      });

      it('logs the title in check.data', function () {
        check.data[0].description.text.should.equal('(missing)');
      });

      it('logs the error in check.data', function () {
        check.data[0].description.error.should.equal(check.ERRORS.description.missing);
      });

    });

    describe('empty <meta> description', function () {

      beforeEach(function () {
        page.html = '<!doctype html><html lang="en"><head><meta charset="UTF-8" /><meta name="description" content=""></head><body></body></html>';
        check.onPageCrawl(page, {}, {});
      });

      it('fails the check', function () {
        check.passed.should.be.false;
      });

      it('logs the title in check.data', function () {
        check.data[0].description.text.should.equal('(empty)');
      });

      it('logs the error in check.data', function () {
        check.data[0].description.error.should.equal(check.ERRORS.description.empty);
      });

    });

    describe('<meta> description that is less than minimum length', function () {

      var shortDesc;

      beforeEach(function () {
        // Create a description that is 1 less than the minimum as defined in the check
        shortDesc = new Array(check.CONSTRAINTS.description.minLength).join('a');
        page.html = '<!doctype html><html lang="en"><head><meta charset="UTF-8" /><meta name="description" content="' + shortDesc + '"></head><body></body></html>';
        check.onPageCrawl(page, {}, {});
      });

      it('fails the check', function () {
        check.passed.should.be.false;
      });

      it('logs the title in check.data', function () {
        check.data[0].description.text.should.equal(shortDesc);
      });

      it('logs the error in check.data', function () {
        check.data[0].description.error.should.equal(check.ERRORS.description.incorrectLength + ' (length is at ' + shortDesc.length + ' characters)');
      });

    });

    describe('<meta> description that is greater than maximum length', function () {

      var longDesc;

      beforeEach(function () {
        // Create a description that is 1 greater than the maximum as defined in the check
        longDesc = new Array(check.CONSTRAINTS.description.maxLength + 2).join('a');
        page.html = '<!doctype html><html lang="en"><head><meta charset="UTF-8" /><meta name="description" content="' + longDesc + '"></head><body></body></html>';
        check.onPageCrawl(page, {}, {});
      });

      it('fails the check', function () {
        check.passed.should.be.false;
      });

      it('logs the title in check.data', function () {
        check.data[0].description.text.should.equal(longDesc);
      });

      it('logs the error in check.data', function () {
        check.data[0].description.error.should.equal(check.ERRORS.description.incorrectLength + ' (length is at ' + longDesc.length + ' characters)');
      });

    });

    describe('<meta> description that is not unique', function () {

      var page2;
      var page3;

      beforeEach(function () {
        page.html = '<!doctype html><html lang="en"><head><meta charset="UTF-8" /><meta name="description" content="I am not a unique meta description."></head><body></body></html>';
        check.onPageCrawl(page, {}, {});

        page2 = {
          url: 'http://www.google.com/page-2',
          type: 'text/html'
        };
        page2.html = '<!doctype html><html lang="en"><head><meta charset="UTF-8" /><meta name="description" content="I am not a unique meta description."></head><body></body></html>';
        check.onPageCrawl(page2, {}, {});

        page3 = {
          url: 'http://www.google.com/page-3',
          type: 'text/html'
        };
        page3.html = '<!doctype html><html lang="en"><head><meta charset="UTF-8" /><meta name="description" content="I am not a unique meta description."></head><body></body></html>';
        check.onPageCrawl(page3, {}, {});

        // This is here to gather all duplicate URLs that are missed with check.onPageCrawl
        check.onCrawlFinish();
      });

      it('keeps track of all pages and their descriptions', function () {
        check.descriptions.should.eql([{
          url: 'http://www.google.com/page',
          description: 'I am not a unique meta description.'
        }, {
          url: 'http://www.google.com/page-2',
          description: 'I am not a unique meta description.'
        }, {
          url: 'http://www.google.com/page-3',
          description: 'I am not a unique meta description.'
        }]);
      });

      it('fails the check', function () {
        check.passed.should.be.false;
      });

      it('logs the title in check.data', function () {
        check.data[0].description.text.should.equal('I am not a unique meta description.');
      });

      it('logs the error in check.data', function () {
        // 2nd and 3rd pages should have an error
        check.data[1].description.error.should.equal(check.ERRORS.description.notUnique);
        check.data[2].description.error.should.equal(check.ERRORS.description.notUnique);
      });

      it('logs the list of duplicates (URLs) in check.data', function () {
        /*
         * Note: the list of duplicate URLs should not include the page's own URL
         *  - also, the arrays in the "should.eql" are backwards (I don't know why)
         */

        // 2nd and 3rd should have an errorLists
        check.data[0].description.errorList.should.eql([
          'http://www.google.com/page-3',
          'http://www.google.com/page-2'
        ]);
        check.data[1].description.errorList.should.eql([
          'http://www.google.com/page-3',
          'http://www.google.com/page'
        ]);
        check.data[2].description.errorList.should.eql([
          'http://www.google.com/page-2',
          'http://www.google.com/page'
        ]);
      });

    });

    describe('correct phone number verification', function () {
      beforeEach(function () {
        check._siteInfo.numbers.ct = [{ number: '1234567890', type: 'call tracking'}];
      });

      it('Doesn\'t find an error', function () {
        page.html = '<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8" /><title>Document</title><meta name="description" content="Here is our phone number: 123-456-7890" /></head><body></body></html>';
        check.onPageCrawl(page, {}, {});

        (check.data[0].description.error === undefined).should.be.true;
      });

      it('logs the incorrect phone number found', function () {
        page.html = '<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8" /><title>Document</title><meta name="description" content="Here is our phone number: 666-456-7890" /></head><body></body></html>';
        check.onPageCrawl(page, {}, {});
        
        check.data[0].description.error.should.be.ok;
        check.data[0].description.text.should.be.ok;
        check.data[0].description.badNumbers[0].should.be.exactly('666-456-7890');
      });

      it('logs that multiple bad phone numbers were found', function () {
        page.html = '<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8" /><title>Document</title><meta name="description" content="Here is our phone number: 666-456-7890 also: 951-753-1793" /></head><body></body></html>';
        check.onPageCrawl(page, {}, {});
        
        check.data[0].description.error.should.be.ok;
        check.data[0].description.text.should.be.ok;
        check.data[0].description.badNumbers.length.should.be.exactly(2);

      });

      it('logs that no number was found', function () {
        page.html = '<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8" /><title>Document</title><meta name="description" content="No phone number" /></head><body></body></html>';
        check.onPageCrawl(page, {}, {});
        
        check.data[0].description.error.should.be.ok;
        check.data[0].description.text.should.be.ok;
      });

      describe('getNumber function', function () {
        it('returns a string of just numbers', function () {
          check.getNumber('1-_+*23').should.be.exactly(123);
        });
      });

      describe('testNumbers function', function () {
        it('returns true when two numbers are the same value', function () {
          check.testNumbers('1234567890', 1234567890).should.be.true;
        });

        it('returns false for mismatched numbers', function () {
          check.testNumbers(1234567890, 3216549870).should.be.false;
        });
      });

      describe('findNumber function', function () {
        it('returns all the phone numbers found in a string', function () {
          check.findNumber('it should find 123-456-7890')[0].should.be.exactly('123-456-7890');
        });

        it('returns more than one phone number', function () {
          var numbers = check.findNumber('it should 123-456-7890 find two 321-654-9870')
          numbers.length.should.be.exactly(2);
          numbers[0].should.be.exactly('123-456-7890');
          numbers[1].should.be.exactly('321-654-9870');
        });

        it('finds UK phone numbers in a string', function () {
          check._siteInfo = {};
          check._siteInfo.country = 'uk';
          check.findNumber('it should find 1234 456 7890')[0].should.be.exactly('1234 456 7890');
        });
      });
    });

  });

  describe('meta keywords verification', function () {

      describe('successful <meta> keywords text retrieval', function () {

        beforeEach(function () {
          // Using default page.html in first beforeEach
          check.onPageCrawl(page, {}, {});
        });

        it('does not fail the check', function () {
          (check.passed === null).should.be.true;
        });

        it('logs the title in check.data', function () {
          check.data[0].keywords.text.should.equal('My page keywords.');
        });

        it('logs no error in check.data', function () {
          (check.data[0].keywords.error === undefined).should.be.true;
        });

      });

      describe('missing <meta> keywords', function () {

        beforeEach(function () {
          page.html = '<!doctype html><html lang="en"><head><meta charset="UTF-8" /></head><body></body></html>';
          check.onPageCrawl(page, {}, {});
        });

        it('fails the check', function () {
          check.passed.should.be.false;
        });

        it('logs the title in check.data', function () {
          check.data[0].keywords.text.should.equal('(missing)');
        });

        it('logs the error in check.data', function () {
          check.data[0].keywords.error.should.equal(check.ERRORS.keywords.missing);
        });

      });

      describe('empty <meta> keywords', function () {

        beforeEach(function () {
          page.html = '<!doctype html><html lang="en"><head><meta charset="UTF-8" /><meta name="keywords" content=""></head><body></body></html>';
          check.onPageCrawl(page, {}, {});
        });

        it('fails the check', function () {
          check.passed.should.be.false;
        });

        it('logs the title in check.data', function () {
          check.data[0].keywords.text.should.equal('(empty)');
        });

        it('logs the error in check.data', function () {
          check.data[0].keywords.error.should.equal(check.ERRORS.keywords.empty);
        });

      });

  });

  describe('onCrawlFinish', function () {

    it('passes check if not explicity failed', function () {
      check.onCrawlFinish();
      check.passed.should.be.true;
    });

    it('fails check if explicity failed', function () {
      check.passed = false;
      check.onCrawlFinish();
      check.passed.should.be.false;
    });

  });

});
