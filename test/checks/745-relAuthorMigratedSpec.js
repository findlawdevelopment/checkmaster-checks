/* jshint strict: true */
/*global beforeEach, describe, it */

/* Mocha plugins */
var sinon = require('sinon');

// REQUIRED
var http = require('http');
// END-REQUIRED

describe('745 - rel="author" migrated check', function () {
  'use strict';

  var check;
  var crawler;
  var mockSiteData;
  var liveResponse;
  var mockStatus;
  var mockURL;
  var previewResponse;
  var server;

  beforeEach(function () {
    check = require('../../checks/745-rel-author-migrated-check.js')();
    previewResponse = '';
    liveResponse = '';
    mockStatus = 200;
    mockSiteData = {
      domain: 'localhost:6767/live'
    };
    mockURL = 'http://localhost:6767/preview';
    check.setSiteData(mockURL, mockSiteData);

    server = http.createServer(function (req, res) {
      res.statusCode = mockStatus;
      if (req.url === '/preview') {
        res.end(previewResponse);
      } else {
        res.end(liveResponse);
      }
    }).listen(6767);
  });

  afterEach(function () {
    server.close();
  });

  function passFail(html) {

      it('fails check if live domain has a rel="author" link tag but the preview domain does not', function (done) {
          liveResponse = html;
          check.exec({ url: mockURL }).then(function () {
              check.passed.should.be.false;
              done();
          });
      });

      it('does not fail check if both live domain and preview domain have the rel="author" link tag', function (done) {
          liveResponse = html;
          previewResponse = html;
          check.exec({ url: mockURL }).then(function () {
              check.passed.should.be.true;
              done();
          });
      });

      it('does not fail check if neither the live domain or preview domain have the rel="author" link tag', function (done) {
          check.exec({ url: mockURL }).then(function () {
              check.passed.should.be.true;
              done();
          });
      });

      it('does not fail check if live domain does not have the rel="author" link tag but the preview domain does', function (done) {
          previewResponse = html;
          check.exec({ url: mockURL }).then(function () {
              check.passed.should.be.true;
              done();
          });
      });

  }

  it('fails check if an error occurs on the preview request', function (done) {
    mockStatus = 404;
    check.exec({ url: mockURL }).then(function () {}, function () {
      // would not make it here if the check wasn't rejected
      done();
    });
  });

  it('saves errors to check.data', function (done) {
    mockStatus = 404;
    check.exec({ url: mockURL }).then(function () {}, function () {
      check.data.errors.should.containDeep([
        'Error: could not get HTML from http://localhost:6767/preview.',
        'Bad status code: ' + mockStatus
      ]);
      done();
    });
  });

  describe('<link rel="author"> pattern', function () {
  
    passFail('<link rel="author">');
  
  });

  describe('<link rel=author> pattern', function () {
  
      passFail('<link rel=author>');
  
  });

  describe('<link rel=\'author\'> pattern', function () {
  
      passFail('<link rel=\'author\'>');
  
  });

});