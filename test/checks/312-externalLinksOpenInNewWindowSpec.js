/* jshint strict: true */
/*global beforeEach, describe, it */

// Native modules
var childProccess = require('child_process');
var urllib = require('url');

// Vendor modules
var Q = require('q');
var sinon = require('sinon');

describe('312 - external links open in new window check', function () {
	'use strict';

	var check;
	var childProcessError;
	var mockSpawn;
	var page;
	var phantomJSResponse;
	var response;

	beforeEach(function () {
		check = require('../../checks/312-external-links-open-in-new-window-check.js')(); // default html that the crawler will return
		childProcessError = null;
		page = {
			url: 'http://www.domain.com/',
			urlData: urllib.parse('http://www.domain.com')
		};
		phantomJSResponse = '{}';
		response = {};
		mockSpawn = {
			stdin: {
				write: function () {
				  
				},
				end: function () {}
			},
			stdout: {
				on: function (event, callback) {
				  callback(phantomJSResponse);
				}
			},
			stderr: {
				on: function (event, callback) {
					if (childProcessError !== null) {
						callback(childProcessError);
					}
				}
			},
			on: function (event, callback) {
			  callback();
			}
		};
		sinon.stub(childProccess, 'spawn', function () {
		  return mockSpawn;
		});
	});

	afterEach(function () {
	  childProccess.spawn.restore();
	});

	describe('rendering', function () {

		it('strips non-result related stdout from JSON result', function (done) {
			phantomJSResponse = 'this is something else \nPHANTOMJS_RESPONSE_JSON>>>' + JSON.stringify([{
				href: 'http://www.external-domain.com/page',
				target: '',
				text: 'a link'
			}]) + '<<<PHANTOMJS_RESPONSE_JSON \n something random';
			check.onPageCrawl(page, response).then(function () {
				(check.data[0].error === undefined).should.be.true;
			  done();
			});
		});

		describe('with child process error', function () {

			beforeEach(function () {
				childProcessError = 'Error: something';
				phantomJSResponse = '';
				sinon.stub(console, 'error'); // silence console
			});

			afterEach(function () {
			  console.error.restore();
			});
		
			it('adds errors to data', function (done) {
				check.onPageCrawl(page, response).then(function () {
					check.data[0].error.should.equal(check.ERRORS.unableToRender);
				  done();
				});
			});

			it('marks check as needing follow up', function () {
				check.onPageCrawl(page, response).then(function () {
					check.needsFollowUp.should.be.true;
				  done();
				});
			});
		
		});

		describe('with JSON parse failure (response from PhantomJS)', function () {

			beforeEach(function () {
				phantomJSResponse = '{noparse';
				sinon.stub(console, 'error'); // silence console
			});

			afterEach(function () {
			  console.error.restore();
			});
	
			it('adds errors to data', function (done) {
				check.onPageCrawl(page, response).then(function () {
					check.data[0].error.should.equal(check.ERRORS.unableToRender);
				  done();
				});
			});

			it('marks check as needing follow up', function () {
				check.onPageCrawl(page, response).then(function () {
					check.needsFollowUp.should.be.true;
				  done();
				});
			});
		
		});
	
	});

	describe('external links with missing target="_blank"s checking', function () {

		it('does not collect links at the same domain', function (done) {
			phantomJSResponse = JSON.stringify([{
				href: 'http://www.domain.com/other-page',
				target: '',
				text: 'a link'
			}]);
			check.onPageCrawl(page, response).then(function () {
				check.data.should.have.a.lengthOf(0);
			  done();
			});
		});

		it('does not collect links that have target="_blank"', function (done) {
			phantomJSResponse = JSON.stringify([{
				href: 'http://www.external-domain.com/page',
				target: '_blank',
				text: 'a link'
			}]);
			check.onPageCrawl(page, response).then(function () {
				check.data.should.have.a.lengthOf(0);
			  done();
			});
		});

		it('does not collect links that start with "javascript:"', function (done) {
			phantomJSResponse = JSON.stringify([{
				href: 'javascript:void(0)',
				target: '',
				text: 'a JS link'
			}]);
			check.onPageCrawl(page, response).then(function () {
				check.data.should.have.a.lengthOf(0);
			  done();
			});
		});

		it('does not collect links that start with "mailto:"', function (done) {
			phantomJSResponse = JSON.stringify([{
				href: 'mailto:email@domain.com',
				target: '',
				text: 'an email link'
			}]);
			check.onPageCrawl(page, response).then(function () {
				check.data.should.have.a.lengthOf(0);
			  done();
			});
		});

		it('does not collect links that start with "tel:"', function (done) {
			phantomJSResponse = JSON.stringify([{
				href: 'tel:1234567890',
				target: '',
				text: 'a tel link'
			}]);
			check.onPageCrawl(page, response).then(function () {
				check.data.should.have.a.lengthOf(0);
			  done();
			});
		});

		it('fails the check if any external links are missing a target', function (done) {
			phantomJSResponse = JSON.stringify([{
				href: 'http://www.external-domain.com/page',
				target: '',
				text: 'a link'
			}]);
			check.onPageCrawl(page, response).then(function () {
				check.passed.should.be.false;
			  done();
			});
		});

		it('fails the check if any external links are "_self"', function (done) {
			phantomJSResponse = JSON.stringify([{
				href: 'http://www.external-domain.com/page',
				target: '_self',
				text: 'a link'
			}]);
			check.onPageCrawl(page, response).then(function () {
				check.passed.should.be.false;
			  done();
			});
		});

		it('adds each link to check.data', function (done) {
			phantomJSResponse = JSON.stringify([{
				href: 'http://www.external-domain.com/page',
				target: '',
				text: 'a link'
			}, {
				href: 'http://www.external-domain.com/page-2',
				target: '_self',
				text: 'a link 2'
			}]);
			check.onPageCrawl(page, response).then(function () {
				check.data.should.have.a.lengthOf(2);
			  done();
			});
		});

		it('adds the residing page URL to check.data', function (done) {
			phantomJSResponse = JSON.stringify([{
				href: 'http://www.external-domain.com/page',
				target: '',
				text: 'a link'
			}, {
				href: 'http://www.external-domain.com/page-2',
				target: '_self',
				text: 'a link 2'
			}]);
			check.onPageCrawl(page, response).then(function () {
				check.data[0].page.should.equal('http://www.domain.com/');
				check.data[1].page.should.equal('http://www.domain.com/');
			  done();
			});
		});

		it('adds the link\'s HREF to check.data', function (done) {
			phantomJSResponse = JSON.stringify([{
				href: 'http://www.external-domain.com/page',
				target: '',
				text: 'a link'
			}, {
				href: 'http://www.external-domain.com/page-2',
				target: '_self',
				text: 'a link 2'
			}]);
			check.onPageCrawl(page, response).then(function () {
				check.data[0].linkHREF.should.equal('http://www.external-domain.com/page');
				check.data[1].linkHREF.should.equal('http://www.external-domain.com/page-2');
			  done();
			});
		});

		it('adds the link\'s target to check.data', function (done) {
			phantomJSResponse = JSON.stringify([{
				href: 'http://www.external-domain.com/page',
				target: '',
				text: 'a link'
			}, {
				href: 'http://www.external-domain.com/page-2',
				target: '_self',
				text: 'a link 2'
			}]);
			check.onPageCrawl(page, response).then(function () {
				check.data[0].linkTarget.should.equal('');
				check.data[1].linkTarget.should.equal('_self');
			  done();
			});
		});

		it('adds the link\'s text (innerText) to check.data', function (done) {
			phantomJSResponse = JSON.stringify([{
				href: 'http://www.external-domain.com/page',
				target: '',
				text: 'a link'
			}, {
				href: 'http://www.external-domain.com/page-2',
				target: '_self',
				text: 'a link 2'
			}]);
			check.onPageCrawl(page, response).then(function () {
				check.data[0].linkText.should.equal('a link');
				check.data[1].linkText.should.equal('a link 2');
			  done();
			});
		});
	
	});

	describe('out of control links', function () {
	  
	  it('skips known links that are out of our control', function (done) {
	  	phantomJSResponse = JSON.stringify([{
	  		href: 'http://www.adobe.com/go/getflashplayer',
	  		target: '',
	  		text: 'get adobe flash player'
	  	}]);
	  	check.onPageCrawl(page, response).then(function () {
	  		check.data.length.should.be.exactly(0);
	  	  done();
	  	});
	  });

	});

});