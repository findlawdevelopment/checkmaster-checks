/* jshint strict: true */
/*global beforeEach, describe, it */

/* Mocha plugins */
var sinon = require('sinon');

// REQUIRED
var Buffer = require('buffer').Buffer;
var fs = require('fs');
var pathlib = require('path');
var childProc = require('child_process');
var async = require('async');
var imageSize = require('image-size');
var Q = require('q');
var request = require('request');
var _ = require('lodash');

describe('live site matches preview check', function () {
  'use strict';

  var check;
  var imageSizeCallback;
  var ratio;
  var ratioBuffer;
  var response;
  var siteInfo;
  var url;

  beforeEach(function () {
    check = require('../../checks/CM100-live-site-matches-preview-check.js')();
    siteInfo = {
      previewDomain: 'somefolder.firmsitepreview.com',
      domain: 'www.google.com',
      types: [],
      pages: {
        siteMap: []
      }
    };
    url = 'http://www.google.com/';
    ratio = 0;
    imageSizeCallback = function () {
      return {
        height: 200,
        width: 200
      };
    };
    ratioBuffer = null;
    response = {
      statusCode: 200
    }

    sinon.stub(request, 'get', function () {
      return {
        pipe: function () {
          return {
            on: function (event, callback) {
              // This always needs to happen after the "on" method below
              setTimeout(function () {
                callback();
              }, 1);
            }
          };
        },
        on: function (event, callback) {
          callback(response);
        }
      };
    });

    sinon.stub(fs, 'createWriteStream');
    
    /*
    | Huge mock handler for all spawned processes (we don't actually want to run them)
    */
    sinon.stub(childProc, 'spawn', function (command, args, options) {
      return {
        on: function (event, callback) {
          if (event === 'close') {
            callback();
          }
        },
        stdout: {
          on: function () {}
        },
        stderr: {
          on: function (event, callback) {
            // for some reason the ratios come through in stderr
            if (command === 'compare') {
              if (ratioBuffer === null) {
                ratioBuffer = new Buffer('12345 (' + ratio + ')');
              }
              callback(ratioBuffer);
            }
          }
        }
      }
    });

    sinon.stub(check, 'imageSize', function (path) {
      return imageSizeCallback(path);
    });
  });

  afterEach(function () {
    request.get.restore();
    childProc.spawn.restore();
    check.imageSize.restore();
    fs.createWriteStream.restore();
  });

  it('returns a promise', function (done) {
    var promise = check.exec(url, siteInfo);
    Q.isPromise(promise).should.be.true;
    promise.then(done);
  });

  it('passes check if siteInfo.previewDomain is missing', function () {
    siteInfo.previewDomain = '';
    check.exec(url, siteInfo).then(function () {
      check.passed.should.be.true;
    });
  });

  it('passes check if siteInfo.domain is missing', function () {
    siteInfo.domain = '';
    check.exec(url, siteInfo).then(function () {
      check.passed.should.be.true;
    });
  });

  it('sets check.data.previewURL to a firmsitepreview URL based on the siteInfo.folder', function (done) {
    siteInfo.previewDomain = 'mycustomfoldername.whatever.com';
    check.exec(url, siteInfo).then(function () {
      check.data.previewURL.should.equal('http://mycustomfoldername.whatever.com');
      done();
    });
  });

  it('sets check.data.liveURL to a URL based on the siteInfo.domain', function (done) {
    siteInfo.domain = 'www.mydomain.com';
    check.exec(url, siteInfo).then(function () {
      check.data.liveURL.should.equal('http://www.mydomain.com');
      done();
    });
  });

  it('passes, everytime', function (done) {
    check.exec(url, siteInfo).then(function () {
      check.passed.should.be.true;
      done();
    });
  });

  it('does not flag check as needing followup if all ratios are 0', function (done) {
    ratio = 0;
    check.exec(url, siteInfo).then(function () {
      check.needsFollowUp.should.be.false;
      done();
    });
  });

  it('flags check as needing followup if any ratio is not 0', function (done) {
    ratio = .1234;
    check.exec(url, siteInfo).then(function () {
      check.needsFollowUp.should.be.true;
      done();
    });
  });

  it('only checks "/" when siteInfo.types is empty', function (done) {
    check.exec(url, siteInfo).then(function () {
      _.forEach(request.get.args, function (arg) {
        // Look for ".com/@1025x1500"
        arg.should.match(/\.com%2F@\d+x\d+/);
      });
      done();
    });
  });

  it('checks "/blog" if siteInfo.types includes slash_blog', function (done) {
    siteInfo.types.push('slash_blog');
    check.exec(url, siteInfo).then(function () {
      request.get.calledWith(check.THUMBNAIL_GEN_URL + encodeURIComponent('www.google.com/blog') + '@' + check.VIEWPORT_WIDTHS[0] + 'x' + check.VIEWPORT_HEIGHT).should.be.true;
      done();
    });
  });

  it('checks "/CM/Custom/Site-Map.asp" if the site is an FS Control site', function (done) {
    siteInfo.types.push('fscontrol');
    check.exec(url, siteInfo).then(function () {
      request.get.calledWith(check.THUMBNAIL_GEN_URL + encodeURIComponent('www.google.com/CM/Custom/Site-Map.asp') + '@' + check.VIEWPORT_WIDTHS[0] + 'x' + check.VIEWPORT_HEIGHT).should.be.true;
      done();
    });
  });

  it('checks "siteMap" page for Publisher sites', function (done) {
    siteInfo.types.push('publisher');
    siteInfo.pages.siteMap = [{
      id: 'Site-Map',
      path: '/Site-Map-Path.shtml'
    }];
    check.exec(url, siteInfo).then(function () {
      request.get.calledWith(check.THUMBNAIL_GEN_URL + encodeURIComponent('www.google.com/Site-Map-Path.shtml') + '@' + check.VIEWPORT_WIDTHS[0] + 'x' + check.VIEWPORT_HEIGHT).should.be.true;
      done();
    });
  });

  it('does not throw an exception when no pages are found (was a bug)', function (done) {
    siteInfo.pages = {};
    try {
      check.exec(url, siteInfo).then(function () {
        // Shouldn't make it this far
        done();
      });
    } catch (e) {
      console.error(e.stack);
    }
  });

  describe('filename generator (getFileName)', function () {
  
    it('strips "http://"', function () {
      var filename = check.getFileName('http://www.google.com', '500');
      filename.should.match(/^www.google/);
    });
  
    it('replaces "/"s with "!"', function () {
      var filename = check.getFileName('www.google.com/some/path', '500');
      filename.should.match(/www.google.com\!some\!path/);
    });
  
    it('adds supplied width to path', function () {
      var filename = check.getFileName('www.google.com', '500');
      filename.should.match(/www.google.com-500/);
    });
  
    it('adds image height to path', function () {
      var filename = check.getFileName('www.google.com', '500');
      filename.should.match(new RegExp('www.google.com-\\d+x' + check.VIEWPORT_HEIGHT));
    });
  
  });

  describe('screenshot generation', function () {
    var callCount;

    beforeEach(function () {
        // all paths and viewports for the preview/live sites
      callCount = check.VIEWPORT_WIDTHS.length * 2;
    });
  
    it('generates a screenshot for all viewports and paths in each call', function (done) {
      check.exec(url, siteInfo).then(function () {
        request.get.callCount.should.equal(callCount);
        done();
      });
    });

    it('formats request correctly', function (done) {
      check.exec(url, siteInfo).then(function () {
        request.get.calledWith(check.THUMBNAIL_GEN_URL + encodeURIComponent(siteInfo.domain + '/') + '@' + check.VIEWPORT_WIDTHS[0] + 'x' + check.VIEWPORT_HEIGHT).should.be.true;
        done();
      });
    });

    it('saves images in specified screenshotDir', function (done) {
      check.exec(url, siteInfo).then(function () {
        fs.createWriteStream.calledWith(check.getScreenshotPath(url, check.VIEWPORT_WIDTHS[0])).should.be.true;
        done();
      });
    });

    it('saves images for all viewports and paths in each call', function (done) {
      check.exec(url, siteInfo).then(function () {
        fs.createWriteStream.callCount.should.equal(callCount);
        done();
      });
    });

    it('does not store any errors in check.data when status codes are 200', function (done) {
      check.exec(url, siteInfo).then(function () {
        _.forEach(check.data.diffs, function (diff) {
          (diff.previewError === undefined).should.be.true;
          (diff.liveError === undefined).should.be.true;
        });
        done();
      });
    });

    it('stores status code errors in check.data', function (done) {
      sinon.stub(console, 'error'); // silence console
      response.statusCode = 400;
      check.exec(url, siteInfo).then(function () {
        _.forEach(check.data.diffs, function (diff) {
          diff.previewError.should.be.true;
          diff.liveError.should.be.true;
        });
        console.error.restore();
        done();
      });
    });

    it('checks the image sizes of all images', function (done) {
      check.exec(url, siteInfo).then(function () {
        check.imageSize.callCount.should.equal(callCount);
        done();
      });
    });

    it('resizes preview/live image to the larger size if they are different heights', function (done) {
      imageSizeCallback = function (path) {
        if (path.indexOf('firmsitepreview') > -1) {
          return {
            height: 250,
            width: 200
          }
        }
        return {
          height: 200,
          width: 200
        };
      };
      check.exec(url, siteInfo).then(function () {
        childProc.spawn.firstCall.args[0].should.equal('convert');
        childProc.spawn.firstCall.args[1][0].should.equal('-extent');
        childProc.spawn.firstCall.args[1][1].should.equal('200x250');
        childProc.spawn.firstCall.args[1][4].should.not.match(/firmsitepreview/);
        done();
      });
    });
  
  });

  describe('image comparing', function () {
  
    it('records the ratio for each viewport in check.data', function (done) {
      ratio = .1234;
      check.exec(url, siteInfo).then(function () {
        _.forEach(check.data.diffs, function (diff) {
          // Converts the number to an integer (percent)
          // e.g. 12.00 = 12%
          diff.ratio.should.equal(12);
        });
        done();
      });
    });

    it('logs an error when the ratio pattern is not found in stderr', function (done) {
      ratioBuffer = new Buffer('');
      sinon.stub(console, 'error');

      check.exec(url, siteInfo).then(function () {
        console.error.called.should.be.true;
        console.error.restore();
        done();
      });
    });
  
  });

});