/* jshint strict: true */
/*global beforeEach, describe, it */

/* Mocha plugins */
var sinon = require('sinon');
var mockPage = require('../mocks/page');

describe('323 - Broken Link check', function() {
  'use strict';

  var check;
  var page;
  var pagesCrawled;

  beforeEach(function() {
    check = require('../../checks/323-broken-link-check.js')();
    page = mockPage('http://www.google.com/broken-url', 'http://www.google.com/');
    page.redirects = ['http://google.com//broken-url-redirect'];
    page.error = {
      code: '123',
      message: 'some message'
    };
  });

  it('sets "needsCredentials" to the broken URL when status code 401s are encountered', function () {
    page.statusCode = 401;
    check.onPageCrawl(page, {code:'',message:''}, pagesCrawled);
    check.needsCredentials.should.equal(page.url);
  });

  describe('onPageCrawl method', function () {
    describe('when no error occurs', function () {
      it('does nothing', function () {
        page.error = '';
        check.onPageCrawl(page, pagesCrawled).then(function () {
          (check.passed === null).should.be.true;
        });
      });
    });

    describe('when an error occurs', function () {
      beforeEach(function () {
        check.onPageCrawl(page, pagesCrawled);
      });

      it('fails the check', function () {
        check.passed.should.be.false;
      });

      it('adds broken link data to check.data', function () {
        check.data.should.have.a.lengthOf(1);
        check.data.should.be.an.Object;
      });

      it('adds broken link url to check.data', function () {
        check.data[0].brokenLink.should.equal(page.url);
      });

      it('adds broken link redirects to check.data', function () {
        check.data[0].redirects.should.equal(page.redirects);
      });

      it('adds broken link error to check.data', function () {
        check.data[0].error.should.equal(page.error.message);
      });

      it('adds broken link referrer (as an array) to check.data', function () {
        check.data[0].referrers.should.eql([page.referrer]);
      });
    });

  });

  describe('error messages', function () {

    it('sets a generic error to the error.message', function () {
      check.onPageCrawl(page, pagesCrawled);
      check.data[0].error.should.equal(page.error.message);
    });
  
    it('sets error to "SSL connection error" message when message contains "SSL23_GET_SERVER_HELLO:unknown protocol"', function () {
      page.error.message = 'SSL23_GET_SERVER_HELLO:unknown protocol';
      check.onPageCrawl(page, pagesCrawled);
      check.data[0].error.should.equal(check.SSL_CONNECTION_ERROR);
    });
  
    it('sets error to "domain not found" message when page.error.code is "ENOTFOUND"', function () {
      page.error.code = 'ENOTFOUND';
      check.onPageCrawl(page, pagesCrawled);
      check.data[0].error.should.equal(check.DOMAIN_NOT_FOUND);
    });
  
    it('sets error to "domain not found" message when page.error.code is "EADDRINFO"', function () {
      page.error.code = 'EADDRINFO';
      check.onPageCrawl(page, pagesCrawled);
      check.data[0].error.should.equal(check.DOMAIN_NOT_FOUND);
    });

    it('sets error to "bad status: STATUS_CODE" when a non 200 status code is encountered', function () {
      page.statusCode = 404;
      check.onPageCrawl(page, pagesCrawled);
      check.data[0].error.should.equal('Bad status: ' + page.statusCode);
    });

    it('sets error to `"PROTOCOL" protocol not supported` when an unsupported protocol is encountered', function () {
      page.error = new Error('Protocol:file: not supported.: file://some-path/blah.html');
      check.onPageCrawl(page, pagesCrawled);
      check.data[0].error.should.equal('"file" protocol not supported');
    });

    it('sets error to the error.message when the page.statusCode is undefined (bug)', function () {
      page.error = new Error('my error');
      page.statusCode = undefined;
      check.onPageCrawl(page, pagesCrawled);
      check.data[0].error.should.equal('my error');
    });
  
  });

  describe('when crawl is finished', function () {
    var additionalPage;

    beforeEach(function () {
      check.onPageCrawl(page, pagesCrawled);
      additionalPage = mockPage('http://www.google.com/page-1');
      pagesCrawled = [page, additionalPage];
    });
  
    it('updates broken link data with any other pages that included the broken link', function () {
      additionalPage.links = ['http://www.google.com/broken-url'];
      check.onCrawlFinish(pagesCrawled);
      check.data[0].referrers.should.containEql('http://www.google.com/page-1');
    });

    it('updates the broken link data with any other pages that include a redirect to that broken link', function () {
      additionalPage.links = page.redirects;
      check.onCrawlFinish(pagesCrawled);
      check.data[0].referrers.should.containEql('http://www.google.com/page-1');
    });

    it('does not add itself to the list of referrers (bug)', function () {
      page.links = page.redirects;
      page.links.push(page.url);
      check.onCrawlFinish(pagesCrawled);
      check.data[0].referrers.should.not.containEql('http://www.google.com/broken-url');
    });

    it('does not add the same referrer twice', function () {
      // Since the additionalPage has two URLs leading to the same broken link, it has a chance of showing up twice
      additionalPage.links = [page.url, page.redirects[0]];
      check.onCrawlFinish(pagesCrawled);
      check.data[0].referrers.should.eql([
        'http://www.google.com/',
        'http://www.google.com/page-1'
      ]);
    });

    it('passes the check if it has not failed', function () {
      check.passed = null;
      check.onCrawlFinish(pagesCrawled);
      check.passed.should.equal(true);
    });
  
  });

});