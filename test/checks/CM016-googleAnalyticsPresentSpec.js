/* jshint strict: true */
/*global beforeEach, describe, expect, it */

// REQUIRED
var request = require('request');
var Q = require('q');
var sinon = require('sinon');
// END-REQUIRED

describe('google analytics present check', function () {
	'use strict';

	var check, url, siteInfo, response, previewBody, liveBody, requestSpy;

	beforeEach(function () {

		check = require('../../checks/CM016-google-analytics-present-check')();
		url = 'http://defectivedrugsite.firmsitepreview.com';
		siteInfo = {
			onlyOn: {
				types: ['preview']
			},
			domain: 'www.defectivedrugsite.com'
		};
		response = {
				statusCode: 200
		};
		previewBody = '<!DOCTYPE html><html><head></head><body></body></html>';
		liveBody = '<!DOCTYPE html><html><head></head><body><script type="text/javascript">var _gaq=_gaq||[];_gaq.push("_setAccount","UA-22813545-2");_gaq.push("_trackPageview");(function() {var ga=document.createElement("script");ga.type="text/javascript";ga.async=true;ga.src=("https:"==document.location.protocol?"https://ssl":"http://www")+".google-analytics.com/ga.js";var s=document.getElementsByTagName("script")[0];s.parentNode.insertBefore(ga,s);} )();</script></body></html>';
		sinon.stub(request, 'get', function (url, callback) {
			if (url.indexOf('firmsitepreview') > -1) {
				callback(null, response, previewBody);	
			} else {
				callback(null, response, liveBody);
			}
		});

	});

	afterEach(function () {
	    request.get.restore();
	});

	it('should only run on preview sites', function (done) {
		check.exec({ url: url }, siteInfo).then(function() {
			check.onlyOn.types[0].should.eql('preview');
			done();
		});
	});

	it('checks for the correct domain', function (done) {
			check.exec({ url: url }, siteInfo).then(function() {
				check._siteInfo.domain.should.eql('www.defectivedrugsite.com');
				done();
			});
	});

	it('the Google Analytics code should fail if it does not exist on the preview website but exists on the live website', function (done) {
		previewBody = '<!DOCTYPE html><html><head></head><body></body></html>';
		check.exec({ url: url }, siteInfo).then(function() {
			check.passed.should.be.false;
			done();
		});
	});

	it('the Google Analytics code should pass if it does not exist on the live and preview website', function (done) {
			previewBody = '<!DOCTYPE html><html><head></head><body></body></html>';
			liveBody = '<!DOCTYPE html><html><head></head><body></body></html>';
			check.exec({ url: url }, siteInfo).then(function() {
				check.passed.should.be.true;
				done();
			});
	});

	it('the Google Analytics code should pass if appears on the preview site but not the live website', function (done) {
			previewBody = '<!DOCTYPE html><html><head></head><body><script type="text/javascript">var _gaq=_gaq||[];_gaq.push("_setAccount","UA-22813545-2");_gaq.push("_trackPageview");(function() {var ga=document.createElement("script");ga.type="text/javascript";ga.async=true;ga.src=("https:"==document.location.protocol?"https://ssl":"http://www")+".google-analytics.com/ga.js";var s=document.getElementsByTagName("script")[0];s.parentNode.insertBefore(ga,s);} )();</script></body></html>';      
			liveBody = '<!DOCTYPE html><html><head></head><body></body></html>';
			check.exec({ url: url }, siteInfo).then(function() {
				check.passed.should.be.true;
				done();
			});
	});

	it('does not consider the live site as having Google Analytics if the live site returns a non-200 status code', function (done) {
			response.statusCode = 404;
			previewBody = '<!DOCTYPE html><html><head></head><body></body></html>';      
			liveBody = '<!DOCTYPE html><html><head></head><body><script type="text/javascript">var _gaq=_gaq||[];_gaq.push("_setAccount","UA-22813545-2");_gaq.push("_trackPageview");(function() {var ga=document.createElement("script");ga.type="text/javascript";ga.async=true;ga.src=("https:"==document.location.protocol?"https://ssl":"http://www")+".google-analytics.com/ga.js";var s=document.getElementsByTagName("script")[0];s.parentNode.insertBefore(ga,s);} )();</script></body></html>';
			check.exec({ url: url }, siteInfo).then(function() {
				// We expect the check to pass even though the script is found on the page
				// Note: was also causing the test to never finish before this fix was implemented
				check.passed.should.be.true;
				done();
			});
	});

});