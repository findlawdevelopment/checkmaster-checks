/* jshint strict: true */
/*global beforeEach, describe, it */

// Vendor modules
var Q = require('q');
var _ = require('lodash');

// Module being tested
var render = require('../../checks/utils/312-render.js');

describe('312 - rendering for external links open in new window check', function () {
  'use strict';

  var document;
  var MockElement = function (attribs) {
    var element = this;
    _.forIn(attribs, function (value, attrib) {
      element[attrib] = value;
    });
  };
  var elements;
  var href;

  MockElement.prototype = {
    getAttribute: function (attrib) {
      return this[attrib];
    }
  };

  beforeEach(function () {
    elements = [];
    
    document = {
      getElementsByTagName: function () {
        return elements;
      }
    };
    href = 'http://www.google.com';
  });

  it('returns all links found in document in array format', function () {
    var result = render.call({ test: true, document: document });
    result.should.be.instanceOf(Array);
  });

  it('converts non-Arrays to an Array', function () {
    elements = {'key1': {}};
    var result = render.call({ test: true, document: document });
    result.should.be.instanceOf(Array);
  });

  it('extracts the link "href"', function () {
    elements = [new MockElement({
      href: 'http://www.google.com'
    })];
    var result = render.call({ test: true, document: document });
    result[0].href.should.equal('http://www.google.com');
  });

  it('extracts the link "target"', function () {
    elements = [new MockElement({
      target: 'http://www.google.com'
    })];
    var result = render.call({ test: true, document: document });
    result[0].target.should.equal('http://www.google.com');
  });

  it('extracts the link "text"', function () {
    elements = [new MockElement({
      innerText: 'http://www.google.com'
    })];
    var result = render.call({ test: true, document: document });
    result[0].text.should.equal('http://www.google.com');
  });

});