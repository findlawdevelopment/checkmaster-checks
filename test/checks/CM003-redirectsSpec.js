/* jshint strict: true */
/*global beforeEach, describe, expect, it */

/* Mocha plugins */
var sinon = require('sinon');

describe('CM003 - Redirects check', function () {
  'use strict';

  var check;
  var page;
  var pagesCrawled;
  var response;

  beforeEach(function () {
    check = require('../../checks/CM003-redirects-check.js')();
    page = {
      url: 'http://www.google.com/redirect',
      referrer: 'http://www.google.com/referrer',
      finalURL: 'http://www.google.com/page',
      links: []
    };
  });

  describe('on internal page redirects', function () {
  
    beforeEach(function () {
      check.onPageCrawl(page, response, pagesCrawled);
    });

    it('fails the check', function () {
      check.passed.should.be.false;
    });

    it('adds an entry to check.data', function () {
      check.data.length.should.equal(1);
    });

    it('adds page URL to check.data', function () {
      check.data[0].url.should.equal('http://www.google.com/redirect');
    });

    it('adds the current referrer to check.data', function () {
      check.data[0].referrers.should.eql(['http://www.google.com/referrer']);
    });
  
  });

  describe('on external page redirects', function () {

    beforeEach(function () {
      page.isExternal = true;
      check.onPageCrawl(page, response, pagesCrawled);
    });

    it('does not fail check', function () {
      (check.passed === null).should.be.true;
    });

    it('does not add to check.data', function () {
      check.data.should.eql([]);
    });
  
  });

  describe('on crawl finish', function () {
  
    beforeEach(function () {
      var pageRef1 = {
        url: 'http://www.google.com/referrer',
        links: ['http://www.google.com/redirect']
      };
      var pageRef2 = {
        url: 'http://www.google.com/another-page',
        links: ['http://www.google.com/redirect']
      };
      pagesCrawled = {
        'http://www.google.com/referrer': pageRef1,
        'http://www.google.com/another-page': pageRef2,
        'http://www.google.com/page': page
      };
      check.onPageCrawl(page);
      check.onCrawlFinish(pagesCrawled);
    });

    it('retrieves other pages that contain the redirects', function () {
      console.log(check.data);
      check.data[0].referrers.should.eql([
        'http://www.google.com/referrer',
        'http://www.google.com/another-page'
      ]);
    });

    it('passes check if check has not failed', function () {
      check.passed = null;
      check.onCrawlFinish(pagesCrawled);
      check.passed.should.be.true;
    });
  
  });

});