/* jshint strict: true */
/*global beforeEach, describe, expect, it */

// REQUIRED
var mockPage = require('../mocks/page');
// END-REQUIRED

describe('contextual navigation check', function () {
	'use strict';

	var check,
		page,
		pagesCrawled,
		response;

	var ATTORNEY_SECTIONS = ['Attorney', 'Attorneys', 'Our-Attorneys', 'Attorney-Profiles'];
	var NON_CONTEXTUAL = '<section class="content"></section><nav><ul><li><a href="/Other-Section/Page.shtml">Other Child</a></li></ul></nav>';
	var OTHER_SECTION = '<section class="content"></section><nav data-nav="contextual"><ul><li><a href="/Other-Section/">Section</a></li></ul></nav>';
	var OTHER_SECTION_CHILD = '<section class="content"></section><nav data-nav="contextual"><ul><li><a href="/Other-Section/Page.shtml">Page</a></li></ul></nav>';
	var SAME_SECTION_CHILD = '<section class="content"></section><nav data-nav="contextual"><ul><li><a href="/Section/Page.shtml">Page</a></li></ul></nav>';
	var TOP_LEVEL = '<section class="content"></section><nav data-nav="contextual"><ul><li><a href="/Top-Level.shtml">Top Level</a></li></ul></nav>';

	beforeEach(function () {
		check = require('../../checks/CM004-contextual-navigation-check')();
		response = {};
		pagesCrawled = {};
	});

	it('does not fail when checking links that don\'t have an href', function (done) {
		// This could throw an exception if it's not handled
		page = mockPage('http://www.domain.com/Section/Page.shtml');
		page.html = '<nav><a id="navigationLowerJump" name="navigationLowerJump"> </a></nav>';
		check.onPageCrawl(page, response, pagesCrawled).then(function () {
		  done();
		});
	});

	/*
	| Includes/Failures
	*/

	describe('with child page', function () {

		beforeEach(function () {
			page = mockPage('http://www.domain.com/Section/Child.shtml');
		});

		includePage();
	
	});

	/*
	| Excludes/Passes
	*/

	describe('with top-level page', function () {

		beforeEach(function () {
			page = mockPage('http://www.domain.com/Top-Level.shtml');
		});

		excludedPage();
	
	});

	describe('with Attorney section', function () {
	
		ATTORNEY_SECTIONS.forEach(function (section) {

			// Section page
			describe('/' + section + '/', function () {
				excludedPage();
			});

			// Section child
			describe('/' + section + '/Child.shtml', function () {
				excludedPage();
			});

		});
	
	});

	describe('with /blog/', function () {

		beforeEach(function () {
			page = mockPage('http://www.domain.com/blog/');
		});

		excludedPage();
	
	});

	describe('with /blog/some/path', function () {

		beforeEach(function () {
			page = mockPage('http://www.domain.com/blog/some/path');
		});

		excludedPage();
	
	});

	describe('with /mt-bin/*', function () {

		beforeEach(function () {
			page = mockPage('http://www.domain.com/mt-bin/blah');
		});

		excludedPage();
	
	});

	function excludedPage() {

		it('does not fail check when contextual navigation contains links to child pages from a section different than the current section', function () {
			page.html = OTHER_SECTION_CHILD;
			check.onPageCrawl(page, response, pagesCrawled).then(function () {
				expect(check.needsFollowUp).toBe(false);
			});
		});

	}

	function includePage() {

		it('fails check when contextual navigation contains links to child pages from a section different than the current section', function () {
			page.html = OTHER_SECTION_CHILD;
			check.onPageCrawl(page, response, pagesCrawled).then(function () {
				check.needsFollowUp.should.be.true;
			});
		});

		it('does not fail check when a non-contextual navigation contains links to child pages from a section different than the current section', function () {
			page.html = NON_CONTEXTUAL;
			check.onPageCrawl(page, response, pagesCrawled).then(function () {
				expect(check.needsFollowUp).toBe(false);
			});
		});

		it('does not fail check when contextual navigation contains links to child pages from the same section as the page', function () {
			page.html = SAME_SECTION_CHILD;
			check.onPageCrawl(page, response, pagesCrawled).then(function () {
				expect(check.needsFollowUp).toBe(false);
			});
		});

		it('does not fail check when contextual navigation does not contain any child pages, but contains sections other than the current section', function () {
			page.html = OTHER_SECTION;
			check.onPageCrawl(page, response, pagesCrawled).then(function () {
				expect(check.needsFollowUp).toBe(false);
			});
		});

		it('does not fail check when contextual navigation does not contain any child pages, but contains top-level links', function () {
			page.html = TOP_LEVEL;
			check.onPageCrawl(page, response, pagesCrawled).then(function () {
				expect(check.needsFollowUp).toBe(false);
			});
		});

		describe('does not fail check when contextual navigation only contains attorney pages', function () {
		
			ATTORNEY_SECTIONS.forEach(function (section) {

				// Section page
				it('/' + section + '/', function () {
					page.html = '<section class="content"></section><nav data-nav="contextual"><ul><li><a href="/' + section + '/">Section</a></li></ul></nav>';
					check.onPageCrawl(page, response, pagesCrawled).then(function () {
						expect(check.needsFollowUp).toBe(false);
					});
				});

				// Section child
				it('/' + section + '/Child.shtml', function () {
					page.html = '<section class="content"></section><nav data-nav="contextual"><ul><li><a href="/' + section + '/Child.shtml">Child</a></li></ul></nav>';
					check.onPageCrawl(page, response, pagesCrawled).then(function () {
						expect(check.needsFollowUp).toBe(false);
					});
				});

			});
		
		});

	}

	describe('after crawl', function () {

		beforeEach(function () {
			pagesCrawled = {};
		});
	
		it('marks check as passed if the check did not fail', function () {
			check.onCrawlFinish(pagesCrawled)
			check.passed.should.be.true;
		});
	
		it('keeps check as failed if the check failed', function () {
			check.passed = false;
			check.onCrawlFinish(pagesCrawled);
			check.passed.should.be.false;
		});
	
	});

});