/* jshint strict: true */
/*global beforeEach, describe, it */

/* Mocha plugins */
var sinon = require('sinon');

// REQUIRED
var mockPage = require('../mocks/page');
// END-REQUIRED

describe('CM008 - Bad code check', function() {
  'use strict';

  var check,
    page,
    response,
    pagesCrawled;

  beforeEach(function () {
    check = require('../../checks/CM008-bad-code-check.js')();
    page = mockPage('http://www.google.com/');
  });

  it('does not fail if bad code does not appear on any page', function () {
    page.html = 'blah blah blah blah';
    check.onPageCrawl(page, response, pagesCrawled);
    (check.passed === null).should.be.true;
  });

  it('adds all failures to an array', function () {
    page.html = 'blah [an error occurred while processing this directive] &lt;#if&gt; blah';
    check.onPageCrawl(page, response, pagesCrawled);
    check.data.length.should.equal(2);
  });

  describe('getContext', function () {
  
    it('returns a substring of arg1 that\'s "50" characters before and after the first instance of the text', function () {
      // Looking for "deliverables" in the block of text
      var context = check.getContext(
        'Efficiently unleash cross-media information without cross-media value. Quickly maximize timely deliverables for real-time schemas. Dramatically maintain clicks-and-mortar solutions without functional solutions.',
        'deliverables'
      );
      context.should.equal('ithout cross-media value. Quickly maximize timely deliverables for real-time schemas. Dramatically maintain clic');
    });
  
  });

  describe('directive failure', function () {

    beforeEach(function () {
      page.html = 'blah blah [an error occurred while processing this directive] blah blah';
      check.onPageCrawl(page, response, pagesCrawled);
    });

    it('fails check if an "[an error occurred while processing this directive]" appears anywhere on any page', function () {
      check.passed.should.be.false;
    });

    it('stores the URL in check.data', function () {
      check.data[0].url.should.equal('http://www.google.com/');
    });

    it('stores "[an error occurred while processing this directive]" reason message in check.data', function () {
      check.data[0].reason.message.should.equal(check.REASONS.DIRECTIVE_ERROR.message);
    });

    it('stores "[an error occurred while processing this directive]" reason instructions in check.data', function () {
      check.data[0].reason.instructions.should.equal(check.REASONS.DIRECTIVE_ERROR.instructions);
    });

    it('stores "[an error occurred while processing this directive]" reason list (context) in check.data', function () {
      check.data[0].reason.list.should.eql([check.getContext(page.html, '[an error occurred while processing this directive]')]);
    });
  
  });

  describe('FreeMarker failure', function () {

    beforeEach(function () {
      page.html = 'blah &lt;#if&gt; blah';
      check.onPageCrawl(page, response, pagesCrawled);
    });

    it('fails check if "<#" is found on any page', function () {
      check.passed.should.be.false;
    });

    it('stores the URL in check.data', function () {
      check.data[0].url.should.equal('http://www.google.com/');
    });

    it('stores "Broken FreeMarker" reason in check.data', function () {
      check.data[0].reason.should.equal(check.REASONS.BROKEN_FREEMARKER);
    });
  
  });

  describe('FreeMarker within comments', function () {

    beforeEach(function () {
      page.html = '<!-- blah &lt;#if&gt; blah -->';
      check.onPageCrawl(page, response, pagesCrawled);
    });
  
    it('does not fail the check', function () {
      (check.passed === null).should.be.true;
    });

    it('marks check as neeeding followup', function () {
      check.needsFollowUp.should.be.true;
    });

    it('stores the URL in check.data', function () {
      check.data[0].url.should.equal('http://www.google.com/');
    });

    it('stores "Commented out FreeMarker found. Please uncomment or remove the commented code." in check.data', function () {
      check.data[0].reason.should.eql({
        message: check.REASONS.COMMENTED_FREEMARKER.message,
        instructions: check.REASONS.COMMENTED_FREEMARKER.instructions,
        isWarning: true,
        list: ['<!-- blah &lt;#if&gt; blah -->']
      });
    });
  
  });

  describe('Pub3008 reference failure', function () {

    beforeEach(function () {
      page.html = 'blah <script src="file://///fl-custops-nas"></script> blah';
      check.onPageCrawl(page, response, pagesCrawled);
    });

    it('fails check if "file://///fl-custops-nas" is found on any page', function () {
      check.passed.should.be.false;
    });

    it('stores the URL in check.data', function () {
      check.data[0].url.should.equal('http://www.google.com/');
    });

    it('stores "References to Pub3008 found on page." in check.data', function () {
      check.data[0].reason.should.eql({
        message: check.REASONS.PUB3008_REF.message,
        instructions: check.REASONS.PUB3008_REF.instructions,
        list: ['file://///fl-custops-nas']
      });
    });
  
  });

  describe('duplicate meta tag failure', function () {

    it('fails check if two meta tags with the same name attribute are found', function () {
      page.html = '<meta name="description"><meta name="description">';
      check.onPageCrawl(page, response, pagesCrawled);
      check.passed.should.be.false;
    });

    it('stores the URL in check.data', function () {
      page.html = '<meta name="description"><meta name="description">';
      check.onPageCrawl(page, response, pagesCrawled);
      check.data[0].url.should.equal('http://www.google.com/');
    });

    it('stores "Duplicate meta tags found" in check.data', function () {
      page.html = '<meta name="description" content=""><meta name="description" content="">';
      check.onPageCrawl(page, response, pagesCrawled);
      check.data[0].reason.should.eql({
        message: check.REASONS.DUPLICATE_METAS.message,
        instructions: check.REASONS.DUPLICATE_METAS.instructions,
        list: ['<meta name="description">']
      });
    });

    it('finds duplicate meta charsets', function () {
      page.html = '<meta charset="utf-8"/><meta charset="utf-8"/>';
      check.onPageCrawl(page, response, pagesCrawled);
      check.passed.should.be.false;
      check.data[0].reason.should.eql({
        message: check.REASONS.DUPLICATE_METAS.message,
        instructions: check.REASONS.DUPLICATE_METAS.instructions,
        list: ['<meta charset>']
      });
    });

    it('lists each duplicate name found in "list" property', function () {
      page.html = '<meta name="description"><meta name="description"><meta name="keywords"><meta name="keywords">';
      check.onPageCrawl(page, response, pagesCrawled);
      check.data[0].reason.list.should.eql([
        '<meta name="description">',
        '<meta name="keywords">'
      ]);
    });
  
  });

  describe('content module error', function () {

    beforeEach(function () {
      page.html = 'Collaboratively administrate empowered markets via plug-and-play networks. Dynamically procrastinate B2C users after installed base benefits. Dramatically visualize customer directed convergence without revolutionary ROI. *Module2* Collaboratively administrate empowered markets via plug-and-play networks. Dynamically procrastinate B2C users after installed base benefits. Dramatically visualize customer directed convergence without revolutionary ROI.';
      check.onPageCrawl(page, response, pagesCrawled);
    });

    describe('uses a pattern that', function () {
    
      it('matches *Module1*', function () {
        check.CONTENT_MODULE_PATTERN.test('*Module1*').should.be.true;
      });
    
      it('matches *module1*', function () {
        check.CONTENT_MODULE_PATTERN.test('*module1*').should.be.true;
      });
    
      it('matches *Module245*', function () {
        check.CONTENT_MODULE_PATTERN.test('*Module245*').should.be.true;
      });
    
    });

    it('fails check if *Content#* is found on the page', function () {
      check.passed.should.be.false;
    });

    it('stores the URL in check.data', function () {
      check.data[0].url.should.equal('http://www.google.com/');
    });

    it('stores "content module found" reason message in check.data', function () {
      check.data[0].reason.message.should.equal(check.REASONS.CONTENT_MODULE_ERROR.message);
    });

    it('stores "content module found" reason instructions in check.data', function () {
      check.data[0].reason.instructions.should.equal(check.REASONS.CONTENT_MODULE_ERROR.instructions);
    });

    it('stores "content module found" reason list (context) in check.data', function () {
      check.data[0].reason.list.should.eql([check.getContext(page.html, '*Module2*')]);
    });
  
  });

  describe('onCrawlFinish', function () {
  
    it('passes check if check has not failed', function () {
      check.passed = null;
      check.onCrawlFinish();
      check.passed.should.be.true;
    });
  
    it('does not pass check if check fails', function () {
      check.passed = false;
      check.onCrawlFinish();
      check.passed.should.be.false;
    });
  
  });


});