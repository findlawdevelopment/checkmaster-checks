/* jshint strict: true */
/*global beforeEach, describe, it */

/* Mocha plugins */


// REQUIRED
var Q = require('q');
var cheerio = require('cheerio');

describe('call tracking number check:', function () {
  'use strict';

  var check, page;

  before(function () {

    page = {};
    page.url = 'http://www.google.com';
    page.dom = function () {
      return cheerio.load(page.html);
    };

  });

  beforeEach(function () {
    check = require('../../checks/805-call-tracking-number-check.js')(); // default html that the crawler will return
    
    page.html = '<!doctype html><html lang="en"><head><meta charset="UTF-8" /><title>Document</title></head><body><p>(012)345-6789</p><p>(123)456-7890</p><img src="badsorce.jpg" alt="(012)345-6789" /><a href="tel:123-456-7890">(123)456-7890</a> <a href="tel:123-456-7890">(123) 456-7890</a></body></html>';

    check._siteInfo = {};
    check._siteInfo.numbers = {
      fax: [
        {
          number: '3216549870'
        }
      ],
      ct: [
        {
          number: '0123456789'
        },
        {
          number: '1234567890'
        }
      ],
      office: [
        {
          number: '9876543210'
        }
      ],
      wld: [
        {
          number: '0123456789'
        },
        {
          number: '1234567890'
        }
      ]
    };

    // alias for testing specific functions
    check.numbers = check._siteInfo.numbers;
  });

  describe('getNumbersFromHTML method:', function () {
    
    it('returns all numbers from page html', function () {

      var numbers = check.getNumbersFromHTML(page);

      // children node text is included in the parent so duplicates will be returned
      numbers.length.should.be.exactly(12);
      
    });

    it('finds numbers that begin with parenthesis (bugfix: CHECK-277)', function () {
      var numbers = check.getNumbersFromHTML(page);
      numbers.forEach(function (number) {
        if (number.originalNumber.indexOf(')') > -1) {
          number.originalNumber.indexOf('(').should.equal(0);
        }
      });
    });

  });

  describe('reduceNumbers method:', function () {
    
    it('reduces array of numbers to be all unique', function () {
      
      var numbers = [
        { number: '1234567890' },
        { number: '1234567890' },
        { number: '1234567890' },
        { number: '1234567891' },
        { number: '1234567891' },
        { number: '1234567891' },
        { number: '1234567891' }
      ];

      check.reduceNumbers(numbers).length.should.be.exactly(2);

    });

  });

  describe('onPageCrawl method:', function () {
    it('finds all the numbers in the html and adds them to the data property', function () {
      
      page.html = '<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8" /><title>Document</title></head><body> <div>123-456-7890</div> </body></html>';
      check.onPageCrawl(page, {}, {}).done(function () {
        check.data.length.should.be.exactly(1);
      });
    });
  });

  describe('onCrawlFinish method:', function () {
    it('adds all WLD numbers to data', function () {
      check._siteInfo.numbers.wld = [{ number: '1234567890' }];

      check.onCrawlFinish();

      check.data.length.should.be.exactly(1);
    });
  });
});