var sinon = require('sinon');

describe('Check Class', function () {
  var Check = require('../../Checks/master').Check
    , name = 'Test Check'
    , defectID = '123'
    , params = {}
  ;
  
  describe('Cunstruction', function () {
    

    it('throws if no name is provided', function (done) {

      try {
        Check(null, defectID, params);
      } catch (e) {
        // test will timeout if it doesn't throw
        done();
      }

    });

    it('throws if no defectID was provided', function (done) {

      try {
        Check(name, null, params);
      } catch (e) {
        // test will timeout if it doesn't throw
        done();
      }

    });

  });

  describe('setSiteData method', function () {
    var url, siteInfo;

    beforeEach(function () {

      url = 'http://google.com';

      params.onlyOn = {};
      params.onlyOn.types = [];

      siteInfo = {};
      siteInfo.types = [];

    });

    it('adds the main url and site info to the check', function () {
      
      var check = new Check(name, defectID, params);
      check.setSiteData(url, siteInfo);

      check._mainURL.should.eql(url);
      (typeof check._siteInfo === 'object').should.be.true;

    });

    it('builds blog urls if it has a blog', function () {
      
      var check = new Check(name, defectID, params);
      siteInfo.types.push('blog');
      params.onlyOn.types.push('blog');

      check.setSiteData(url, siteInfo);
      Array.isArray(check._blogURLs).should.be.true;


    });
  });

  describe('exec method', function () {
    var Q = require('q');

    it('calls complete when no crawler is needed', function () {

      var check = new Check(name, defectID, {
        onlyOn: {},
        run: function () {
          var deferred = Q.defer();
          deferred.resolve();
          return deferred.promise;
        }
      });

      sinon.spy(check, 'complete');

      check.exec().finally(function () {
        check.complete.called.should.be.true;
      })

      check.complete.restore();
      
    });

    it('does not call complete when a crawler is needed', function () {
      var check = new Check(name, defectID, {
        onlyOn: {},
        requires: ['crawler'],
        onPageCrawl: function () {
          var deferred = Q.defer();
          deferred.resolve();
          return deferred.promise;
        }
      });

      sinon.spy(check, 'complete');

      check.exec().finally(function () {
        check.complete.called.should.be.false;
      })

      check.complete.restore();
    });
  });

  describe('referrer methods', function () {
    
    var url = 'http://google.com';
    var pagesCrawled = {
      'http://findlaw.com': {
        links: [url]
      },
      'http://reddit.com': {
        links: [url]
      }
    };

    describe('_findReferrers method', function () {
      it('finds the all referrers', function () {

        Check.prototype._findReferrers(url, pagesCrawled)
          .should.eql(['http://findlaw.com', 'http://reddit.com']);
      });
    });

    describe('referringPages method', function () {
      it('given a string URL, returns a collection of referring urls', function () {
        var check = new Check(name, defectID, params);

        check.referringPages(url, pagesCrawled)[0]
          .should.eql({
            url: url,
            referringPages: ['http://findlaw.com', 'http://reddit.com']
          });
        ;
      });

      it('given an array of URLS, returns a collection of referring URLs', function () {
        var urls = [url];

        var check = new Check(name, defectID, params);

        check.referringPages(urls, pagesCrawled)[0]
          .should.eql({
            url: url,
            referringPages: ['http://findlaw.com', 'http://reddit.com']
          });
        ;
      });
    });
  });

  describe('renderDom method', function () {
    var page
      , cheerio = require('cheerio');
    ;

    beforeEach(function () {
      page = 'HTML';

      sinon.stub(cheerio, 'load');
    });

    afterEach(function () {
        cheerio.load.restore();
    });

    it('returns a cheerio function to search the DOM', function () {

      var $ = Check.prototype.renderDom(page);

      cheerio.load.calledWith('HTML').should.be.true;
      
    });

  });

  describe('shouldRun method', function () {
    var filters;

    beforeEach(function () {
      filters = {};
    });

    it('returns true for only on blog', function () {
      
      var check = new Check(name, defectID, {
        onlyOn: {
          types: ['blog']
        }
      });

      filters.types = ['blog'];

      check.shouldRun(filters).should.be.true;

    });

    it('returns true for only on blog with neverOn defined', function () {
      
      var check = new Check(name, defectID, {
        onlyOn: {
          types: ['blog']
        },
        neverOn: {
          types: ['external']
        }
      });

      filters.types = ['blog'];

      check.shouldRun(filters).should.be.true;

    });

    it('returns true when the attr is a string and filter is an array', function () {
      var check = new Check(name, defectID, {
        foo: 'bar'
      });

      filters.types = ['bar'];

      check.shouldRun(filters).should.be.true;
    });

    it('returns true when the attr is a string and filter is a string', function () {
      var check = new Check(name, defectID, {
        foo: 'bar'
      });

      filters.types = 'bar';

      check.shouldRun(filters).should.be.true;
    });

    it('returns true when the attr is not defined', function () {
      var check = new Check(name, defectID, {});

      filters.types = 'bar';

      check.shouldRun(filters).should.be.true;
    });

    it('returns false with neverOn blog', function () {
      var check = new Check(name, defectID, {
        neverOn: {
          types: ['blog']
        }
      });

      filters.types = ['blog'];

      check.shouldRun(filters).should.be.false;
    });

    it('returns false with neverOn blog and onlyOn defined', function () {
      var check = new Check(name, defectID, {
        neverOn: {
          types: ['blog']
        },
        onlyOn: {
          types: ['external']
        }
      });

      filters.types = ['blog'];

      check.shouldRun(filters).should.be.false;   
    });

    it('passes when onlyOn[type] === []', function () {
      var check = new Check(name, defectID, {
        onlyOn: {
          activities: []
        }
      });

      filters.types = ['blog'];

      check.shouldRun(filters).should.be.true;   
    });
  });
});