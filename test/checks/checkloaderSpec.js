describe('Check loader', function () {
  var filters = {}
    , loader = require('../../checks')
  ;

  var fs = require('fs')
    , sinon = require('sinon')
  ;

  var checksDir = __dirname + '/mock-checks';

  beforeEach(function () {
    filters.types = [];
    filters.roles = [];
    filters.activities = [];

    sinon.stub(fs, 'readdir', function (dir, cb) {
      cb(null, ['123-test-check.js', '321-other-test-check.js', '666-check-of-the-beast-check.js']);
    });
  });

  afterEach(function () {
    fs.readdir.restore();
  });

  it('loads checks based on filters', function (done) {
    filters.types.push('blog');
    loader(filters, checksDir).done(function (checks) {
      checks.length.should.eql(2);
      done();
    });
  });

  it('caches the checks', function (done) {
    loader(filters, checksDir).done(function () {
      fs.readdir.called.should.be.false;
      done();
    });

  });

  it('calls the local and prototype version of shouldRun', function (done) {
    filters.dontRun666 = true;
    loader(filters, checksDir).done(function (checks) {
      checks.length.should.eql(1);
      checks[0].defectID.should.not.eql('666');
      done();
    });
  });
});
