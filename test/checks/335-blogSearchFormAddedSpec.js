
describe('Blog search form added check', function () {
  "use strict";
  var request = require('request');
  var sinon = require('sinon');

  var check, defaultURL;

  beforeEach(function () {
    check = require('../../checks/335-blog-search-form-added-check')();
    defaultURL = 'http://www.hooperstrike.com/blog/';

    sinon.stub(request, 'get', function (url, cb) {
      cb(null, {}, require('./335-assets/' + url));
    })

  });

  afterEach(function () {
    request.get.restore();
  });

  it('should not pass if any errors are found - even if it\'s correct in other places', function (done) {
    var siteInfo = {
      types: ['blog', 'live'],
      test_blogURLs: [
        'validForm',
        'nothing'
      ]
    };
    check.setSiteData(defaultURL, siteInfo);
    var promise = check.exec({ url: defaultURL }, siteInfo);

    promise.then(function (result) {
      check.passed.should.be.false;
      check.data[0].message.should.eql('Search form is missing');
      check.data[0].url.should.eql('nothing');
      done();
    });
  });

  describe('when finding the search form', function () {
    it('should fail if a search form is not found at /blog on an integrated blog', function (done) {
      var siteInfo = {
        types: ['blog', 'slash_blog', 'live'],
        test_blogURLs: ['nothing']
      };
      check.setSiteData(defaultURL, siteInfo);
      var promise = check.exec({ url: defaultURL }, siteInfo);

      promise.then(function (result) {
        check.passed.should.be.false;
        done();
      });
    });

    it('should pass if a form with the ID of "searchForm" is found (and there are no other issues)', function (done) {
      var siteInfo = {
        types: ['blog', 'slash_blog', 'live'],
        test_blogURLs: ['searchFormID']
      };
      check.setSiteData(defaultURL, siteInfo);
      var promise = check.exec({ url: defaultURL }, siteInfo);

      promise.then(function (result) {
        check.passed.should.be.true;
        done();
      });
    });

    it('should pass if an element with the ID of "searchFormDesign" is found with a <form> within (and there are no other issues)', function (done) {
      var siteInfo = {
        types: ['blog', 'slash_blog', 'live'],
        test_blogURLs: ['searchFormDesignID']
      };
      check.setSiteData(defaultURL, siteInfo);
      var promise = check.exec({ url: defaultURL }, siteInfo);

      promise.then(function (result) {
        check.passed.should.be.true;
        
        done();
      });
    });

    it('should pass if a form with a "Search-Results.shtml" action is found (and there are no other issues)', function (done) {
      var siteInfo = {
        types: ['blog', 'slash_blog', 'live'],
        test_blogURLs: ['formActionResults']
      };
      check.setSiteData(defaultURL, siteInfo);
      var promise = check.exec({ url: defaultURL }, siteInfo);

      promise.then(function (result) {
        check.passed.should.be.true;
        
        done();
      });
    });

    it('should pass if a form with an action that ends with "-Results.shtml" is found (and there are no other issues)', function (done) {
      var siteInfo = {
        types: ['blog', 'slash_blog', 'live'],
        test_blogURLs: ['formActionResultsAlt']
      };
      check.setSiteData(defaultURL, siteInfo);
      var promise = check.exec({ url: defaultURL }, siteInfo);

      promise.then(function (result) {
        check.passed.should.be.true;
        
        done();
      });
    });

    it('should include each URL that\'s missing the form in a list', function (done) {
      var siteInfo = {
        types: ['blog', 'slash_blog', 'live'],
        test_blogURLs: ['nothing']
      };
      check.setSiteData(defaultURL, siteInfo);
      var promise = check.exec({ url: defaultURL }, siteInfo);

      promise.then(function () {
        check.data[0].message.should.eql('Search form is missing');
        check.data[0].url.should.eql('nothing');
        done();
      });
    });
  });

  describe('when looking for correct search text', function () {
    it('should fail if the search input is not found', function (done) {
      var siteInfo = {
        types: ['blog', 'slash_blog', 'live'],
        test_blogURLs: ['noInput']
      };
      check.setSiteData(defaultURL, siteInfo);
      var promise = check.exec({ url: defaultURL }, siteInfo);

      promise.then(function (result) {
        check.passed.should.be.false;
        done();
      });
    });

    it('should fail if the search text is not "Search our site" for slash_blogs', function (done) {
      var siteInfo = {
        types: ['blog', 'slash_blog', 'live'],
        test_blogURLs: ['wrongTextSlashBlog']
      };
      check.setSiteData(defaultURL, siteInfo);
      var promise = check.exec({ url: defaultURL }, siteInfo);

      promise.then(function (result) {
        check.passed.should.be.false;
        done();
      });
    });

    it('should fail if the search text is not "Search our blog" for standalone blogs', function (done) {
      var siteInfo = {
        types: ['blog', 'live'],
        test_blogURLs: ['wrongTextStandaloneBlog']
      };
      check.setSiteData(defaultURL, siteInfo);
      var promise = check.exec({ url: defaultURL }, siteInfo);

      promise.then(function (result) {
        check.passed.should.be.false;
        done();
      });
    });

    it('should include each URL that\'s has incorrect search text on a slash blog', function (done) {
      var siteInfo = {
        types: ['blog', 'slash_blog', 'live'],
        test_blogURLs: ['wrongTextSlashBlog']
      };
      check.setSiteData(defaultURL, siteInfo);
      var promise = check.exec({ url: defaultURL }, siteInfo);

      promise.then(function (result) {
        check.data[0].message.should.eql('Search text should be "Search our site"');
        check.data[0].url.should.eql('wrongTextSlashBlog');
        done();
      });
    });

    it('should include each URL that\'s has incorrect search text on a standalone blog', function (done) {
      var siteInfo = {
        types: ['blog', 'live'],
        test_blogURLs: ['wrongTextStandaloneBlog']
      };
      check.setSiteData(defaultURL, siteInfo);
      var promise = check.exec({ url: defaultURL }, siteInfo);

      promise.then(function (result) {
        check.data[0].message.should.eql('Search text should be "Search our blog"');
        check.data[0].url.should.eql('wrongTextStandaloneBlog');
        done();
      });
    });

    it('should not care about the case of the "Search our blog/site" text', function (done) {
      var siteInfo = {
        types: ['blog', 'live'],
        test_blogURLs: ['mixedCaseTitle']
      };
      check.setSiteData(defaultURL, siteInfo);
      var promise = check.exec({ url: defaultURL }, siteInfo);

      promise.then(function (result) {
        check.passed.should.be.true;
        done();
      });
    });

    it('should not experience a fatal error when the title attribute is missing from the input', function (done) {
      var siteInfo = {
        types: ['blog', 'live'],
        test_blogURLs: ['missingInputTitle']
      };
      check.setSiteData(defaultURL, siteInfo);
      var promise = check.exec({ url: defaultURL }, siteInfo);

      promise.then(function (result) {
        // Test will experience a fatal error if this is not resolved
        done();
      });
    });
  });

});