/* jshint strict: true */
/*global beforeEach, describe, it */

/* Mocha plugins */


// REQUIRED
var check = require('../../checks/366-logo-links-to-homepage-check.js')();
var sinon = require('sinon');
var request = require('request');
var Q = require('q');
var cheerio = require('cheerio');


describe('366 - logo links to homepage check', function () {
	'use strict';

	var check, page;

	beforeEach(function () {
		check = require('../../checks/366-logo-links-to-homepage-check.js')(); // default html that the crawler will return
		page = {};
		page.type = 'text/html';
		page.html = '<!doctype html><html lang="en"><head><meta charset="UTF-8" /><title>Document</title></head><body></body></html>';
		page.url = 'http://google.com';
    page.dom = function () {
      return cheerio.load(page.html);
    };		
	});

	describe('someMethod method', function () {

		it('passes when the logo links correctly', function (done) {			
			
			page.html = '<!doctype html><html lang="en"><head><meta charset="UTF-8" /><title>Document</title></head><body><div class="brand"><a href="/">test</a></div></body></html>';
			
			check.onPageCrawl(page, {}, {}).then(function () {
				// test specific html that the crawler will return
			
				check.data.should.eql([]);
				check.onCrawlFinish();
				check.passed.should.be.true;

				done();

				// the below done will be called with your error if the promise is rejected;
			}, done);

		});

		it('fails when there is no brand class found', function (done) {	
			
			page.html = '<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:og="http://opengraphprotocol.org/schema/" lang="en" class="no-js"><head></head><body><div><a href="/">Law Offices of <div class="line2">Winifred Whitaker</div></a></div></body></html>';
			
			check.onPageCrawl(page, {}, {}).then(function () {
				// test specific html that the crawler will return
			
				check.data[0].should.eql("http://google.com");
				check.passed.should.be.false;
				done();

				// the below done will be called with your error if the promise is rejected;
			}, done);

		});	

		it('fails when the logo doesnt link to the homepage', function (done) {
			
			page.html = '<!doctype html><html lang="en"><head><meta charset="UTF-8" /><title>Document</title></head><body><div class="brand"><a href="/something-else/">test</a></div></body></html>';
			
			check.onPageCrawl(page, {}, {}).then(function () {
				// test specific html that the crawler will return
			
				check.data[0].should.equal('http://google.com');
				check.passed.should.be.false;

				done();

				// the below done will be called with your error if the promise is rejected;
			}, done);

		});	

		it('only runs on text/html pages', function (done) {
			page.type = 'application/pdf';
			page.html = '<!doctype html><html lang="en"><head><meta charset="UTF-8" /><title>Document</title></head><body><div class="brand"><a href="/something-else/">test</a></div></body></html>';
			
			check.onPageCrawl(page, {}, {}).then(function () {
				(check.passed === null).should.be.true;
				done();
			}, done);
		});
	
	});

});