describe('Blog short form is present check', function () {

	it('should fail if the blog home page doesn\'t have a short form', function (done) {
		var check = require('../../checks/341-blog-short-form-present-check')();
		
		check.exec({ url: 'https://dl.dropboxusercontent.com/s/ii3168qnp6h7xs4/passing-posts-on-home-page.html?dl=1&token_hash=AAGPsb5JV7_z2Dv4kMGc8ENqWiZB7uWjDq0Xla8H3VkYMw' }).then(function () {
			check.passed.should.be.false;
			done();
		});
	});

});