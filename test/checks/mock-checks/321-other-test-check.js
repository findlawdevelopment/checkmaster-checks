var Check = require('../../../checks/Master').Check;
var Q = require('q');

module.exports = function () {
  return new Check('other test check', '321', {
    neverOn: {
      types: ['blog']
    },
    resultType: 'list',

    run: function (page, siteInfo) {
      var deferred = Q.defer();
      deferred.resolve();
      return deferred.promise;
    }
  });
};