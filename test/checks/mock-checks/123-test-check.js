var Check = require('../../../checks/Master').Check;
var Q = require('q');

module.exports = function () {
  return new Check('test check', '123', {
    onlyOn: {
      types: ['blog']
    },
    resultType: 'list',

    run: function (page, siteInfo) {
      var deferred = Q.defer();
      deferred.resolve();
      return deferred.promise;
    }
  });
};