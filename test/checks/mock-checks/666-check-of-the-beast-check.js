var Check = require('../../../checks/Master').Check;
var Q = require('q');

module.exports = function () {
  return new Check('check of the beast', '666', {
    resultType: 'list',

    shouldRun: function (filters) {
      if (filters.dontRun666) {
        return false;
      }
    },

    run: function (page, siteInfo) {
      var deferred = Q.defer();
      deferred.resolve();
      return deferred.promise;
    }
  });
};