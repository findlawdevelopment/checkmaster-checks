/* jshint strict: true */
/*global beforeEach, describe, expect, it */

// REQUIRED
var sinon = require('sinon');
var mockPage = require('../mocks/page');
// END-REQUIRED

describe('carryover check', function () {
	'use strict';

	var check, page, pagesCrawled, response;

	beforeEach(function () {
		check = require('../../checks/CM009-carryover-check')();
		sinon.stub(check, 'getSiteInfo').returns({
			pages: {
				video: ['Video-ID']
			}
		});

		page = mockPage('http://www.google.com/');
		response = {
			statusCode: 200,
			headers: {
				'content-type': 'text/html'
			}
		};
		pagesCrawled = {};
	});

	afterEach(function () {
	    check.getSiteInfo.restore();
	});

	it('ignores text outside of content area', function () {
		page.html = ' http://www.google.com/ <div class="content">something</div>';
		check.onPageCrawl(page, response, pagesCrawled).then(function () {
			(check.passed === null).should.be.true;
		});
	});

	it('does not fail if no content is found', function () {
		page.html = '<div class="content"></div>';
		check.onPageCrawl(page, response, pagesCrawled).then(function () {
			(check.passed === null).should.be.true;
		});
	});

	it('finds carryover URLs via the HTML', function () {
		// Logic behind this: if you're grabbing the text via jQuery.text(), the ".html" and the "We" are combined
		page.html = '<div class="content"><p>http://www.google.com/carryover.html</p>We will carry over this content from your existing site.</div>';
		check.onPageCrawl(page, response, pagesCrawled).then(function () {
			check.data[0].carryover.should.eql('http://www.google.com/carryover.html');
		});
	});

	it('does not fail if the page is a video page (by page type)', function () {
		page = mockPage('http://www.google.com/Video-ID.shtml');
		page.html = '<div class="content"> carry over http://www.domain.com/ </div>';
		check.onPageCrawl(page, response, pagesCrawled).then(function () {
			(check.passed === null).should.be.true;
		});
	});

	describe('finding same carryover URL on multiple different pages', function () {

	
		it('updates both records in check.data with "Same carryover URL exists on multiple different pages" reason.', function () {
			page = mockPage('http://www.google.com/Page1.shtml');
			page.html = '<div class="content">carry over http://www.google.com/blah.html</div>';
			check.onPageCrawl(page, response, pagesCrawled).then(function () {

				page = mockPage('http://www.google.com/Page2.shtml');
				page.html = '<div class="content">carry over http://www.google.com/blah.html</div>';
				check.onPageCrawl(page, response, pagesCrawled).then(function () {
				  
				});

			});			
		});

	});

	describe('with multiple carryover URLs found', function () {

		beforeEach(function () {
			page.html = '<div class="content">carry over http://www.google.com/blah.html and http://www.google.com/other.html</div>';
			check.onPageCrawl(page, response, pagesCrawled);
		});

		it('uses an empty string for "carryover" if no carryover URL is found for a carryover page', function () {
			check.data[0].carryover.should.eql(['http://www.google.com/blah.html', 'http://www.google.com/other.html']);
		});

		it('sets the reason to "Found multiple carryover URLs for one page" reason', function () {
			check.data[0].reason.should.eql('Found multiple carryover URLs for one page. Please determine what carryover this page should have and/or follow up with someone who would be able to provide you with the URL.');
		});
	
	});

	describe('pageID logic', function () {
	
		it('generates pageID for .shtml pages', function () {
			page = mockPage('http://www.google.com/Page.shtml');
			page.html = '<div class="content"> carry over http://www.domain.com/ </div>';
			check.onPageCrawl(page, response, pagesCrawled).then(function () {
				check.data[0].id.should.eql('Page');
			});
		});
	
		it('generates pageID for child .shtml pages', function () {
			page = mockPage('http://www.google.com/Section/Page.shtml');
			page.html = '<div class="content"> carry over http://www.domain.com/ </div>';
			check.onPageCrawl(page, response, pagesCrawled).then(function () {
				check.data[0].id.should.eql('Page');
			});
		});

		it('generates pageID for section pages', function () {
			page = mockPage('http://www.google.com/Section/');
			page.html = '<div class="content"> carry over http://www.domain.com/ </div>';
			check.onPageCrawl(page, response, pagesCrawled).then(function () {
				check.data[0].id.should.eql('Section');
			});
		});

		it('generates "PAGE-ID-NOT-FOUND" for Carry-It-Over pageID when pageID can\'t be determined', function () {
			page = mockPage('http://www.google.com/----');
			page.html = '<div class="content"> carry over http://www.domain.com/ </div>';
			check.onPageCrawl(page, response, pagesCrawled).then(function () {
				check.data[0].id.should.eql('PAGE-ID-NOT-FOUND');
			});
		});

		it('generates "Home" for Carry-It-Over pageID when pageID is at the root level of a site', function () {
			page = mockPage('http://www.google.com/');
			page.html = '<div class="content"> carry over http://www.domain.com/ </div>';
			check.onPageCrawl(page, response, pagesCrawled).then(function () {
				check.data[0].id.should.eql('Home');
			});
		});
	
	});

	describe('carryover URL error', function () {
	
		describe('finding "http://" on content that\'s < 500 characters text', function () {

			beforeEach(function () {
				page = mockPage('http://www.google.com/Section/Page.shtml');
				page.html = '<div class="content"> http://www.google.com/blah.html </div>';
				check.onPageCrawl(page, response, pagesCrawled);
			});
		
			it('fails check', function () {
				check.passed.should.be.false;
			});

			it('adds the URL to check.data', function () {
				check.data[0].url.should.eql('http://www.google.com/Section/Page.shtml');
			});

			it('adds the reason to check.data', function () {
				check.data[0].reason.should.eql('Carryover URL Found');
			});

			it('adds the page ID to check.data', function () {
				check.data[0].id.should.eql('Page');
			});

			it('adds the Carry-It-Over URL to check.data', function () {
				check.data[0].carryover.should.eql('http://www.google.com/blah.html');
			});
		
		});
	
	});

});