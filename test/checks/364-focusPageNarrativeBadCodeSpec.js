/* jshint strict: true */
/*global beforeEach, describe, it */

/* Mocha plugins */

var sinon = require('sinon');

// REQUIRED
var Mustache = require('mustache');
var Q = require('q');
var request = require('request');
var winston = require('winston');

var gfp = require('../../checks/utils/getFocusPages');
var gfpConstants = gfp.constants;

describe('364 - focus page narrative bad code check', function () {
  'use strict';

  var check;
  var fpResults;
  var grammarReqError;
  var grammarReqResponse;
  var grammarReqBody;
  var siteInfo;

  beforeEach(function () {
    check = require('../../checks/364-focus-page-narrative-bad-code-check.js')();

    fpResults = {};
    grammarReqError = null;
    grammarReqResponse = { statusCode: 200 };
    grammarReqBody = '<results></results>';

    sinon.stub(gfp, 'getFocusPages', function () {
      return {
        then: function (callback) {
          callback(fpResults);
        }
      }
    });

    sinon.stub(request, 'post', function (url, data, callback) {
      callback(grammarReqError, grammarReqResponse, grammarReqBody);
    });

    siteInfo = {
      getFocusPages: gfp.getFocusPages
    };
  });

  afterEach(function () {
    gfp.getFocusPages.restore();
    request.post.restore();
  });

  it('always passes', function (done) {
    fpResults = {
      error: 'some error',
      url: 'http://www.errorurl.com'
    };
    check.exec('http://www.somesite.com', siteInfo).then(function () {
      check.passed.should.be.true;
      done();
    });
  });

  it('sets needsFollowUp to true when anything but "no Focus Pages found" occurs', function (done) {
    fpResults = {
      error: 'some error',
      url: 'http://www.errorurl.com'
    };
    check.exec('http://www.somesite.com', siteInfo).then(function () {
      check.needsFollowUp.should.be.true;
      done();
    });
  });

  it('does not set needsFollowUp if no Focus Pages were found', function (done) {
    fpResults = {
      error: gfpConstants.NO_FOCUS_PAGES,
      url: 'http://www.errorurl.com'
    };
    check.exec('http://www.somesite.com', siteInfo).then(function () {
      check.needsFollowUp.should.not.be.true;
      done();
    });
  });

  it('marks the error as non-fatal if no Focus Pages were found', function (done) {
    fpResults = {
      error: gfpConstants.NO_FOCUS_PAGES,
      url: 'http://www.errorurl.com'
    };
    check.exec('http://www.somesite.com', siteInfo).then(function () {
      check.data.nonFatal.should.be.true;
      done();
    });
  });

  describe('data setting', function () {

    it('sets check.data to the results from getFocusPages', function (done) {
      fpResults = {
        error: 'some error',
        url: 'http://www.errorurl.com'
      };
      check.exec('http://www.somesite.com', siteInfo).then(function () {
        check.data.should.eql(fpResults);
        done();
      });
    });

    it('strips unrelated text from data', function (done) {
      fpResults = {
        focusPages: [{
          title: 'my title',
          narrative: 'something',
          somethingNew: 'blah',
          url: 'http://www.google.com'
        }]
      };
      check.exec('http://www.somesite.com', siteInfo).then(function () {
        check.data.should.eql({
          focusPages: [{
            title: 'my title',
            narrative: 'something',
            url: 'http://www.google.com'
          }]
        }); 
        done();
      });
    });
  
  });

  describe('bad code checking', function () {
  
    it('loops through each focus page for bad code', function (done) {
      fpResults = {
        focusPages: [{
          narrative: 'content goes here',
          url: 'http://www.google.com/1'
        }, {
          narrative: 'content goes here',
          url: 'http://www.google.com/2'
        }]
      };

      check.exec('http://www.somesite.com', siteInfo).then(function () {
        check.data.focusPages[0].issues.length.should.be.greaterThan(0);
        check.data.focusPages[1].issues.length.should.be.greaterThan(0);
        done();
      });
    });

    it('grabs the context of the discovered text', function (done) {
      fpResults = {
        focusPages: [{
          narrative: 'blah blah goes here blah blah',
          url: 'http://www.google.com/1'
        }]
      };

      check.exec('http://www.somesite.com', siteInfo).then(function () {
        check.data.focusPages[0].issues[0].context.should.equal('blah blah goes here blah blah');
        done();
      });
    });

    describe('specific failures/flags', function () {

      it('finds "goes here" text', function (done) {
        fpResults = {
          focusPages: [{
            narrative: 'text goes here',
            url: 'http://www.google.com/1'
          }]
        };

        check.exec('http://www.somesite.com', siteInfo).then(function () {
          check.data.focusPages[0].issues[0].found.should.equal('goes here');
          done();
        });
      });

      it('finds "note to" text', function (done) {
        fpResults = {
          focusPages: [{
            narrative: 'note to PM:',
            url: 'http://www.google.com/1'
          }]
        };

        check.exec('http://www.somesite.com', siteInfo).then(function () {
          check.data.focusPages[0].issues[0].found.should.equal('note to');
          done();
        });
      });

      it('finds text in all caps within "[]"', function (done) {
        fpResults = {
          focusPages: [{
            narrative: 'something [ FIRM NAME ] something',
            url: 'http://www.google.com/1'
          }]
        };

        check.exec('http://www.somesite.com', siteInfo).then(function () {
          check.data.focusPages[0].issues[0].found.should.equal('[ FIRM NAME ]');
          done();
        });
      });
    
    });

    describe('grammar checking', function () {
    
      it('gracefully fails on error', function (done) {
        grammarReqError = new Error('some request error');
        sinon.stub(winston, 'error');

        fpResults = {
          focusPages: [{
            narrative: 'blah',
            url: 'http://www.google.com/1'
          }]
        };

        check.exec('http://www.somesite.com', siteInfo).then(function () {
          // Check that no issue was found
          (check.data.focusPages[0].issues === undefined).should.be.true;

          // Check for winston log
          winston.error.calledWith(Mustache.render(check.ERROR_DETAILS, {
            url: 'http://www.google.com/1',
            error: 'some request error'
          })).should.be.true;

          winston.error.restore();
          done();
        });
      });
    
      it('gracefully fails on status code errors', function (done) {
        grammarReqResponse.statusCode = 500;
        sinon.stub(winston, 'error');

        fpResults = {
          focusPages: [{
            narrative: 'blah',
            url: 'http://www.google.com/1'
          }]
        };

        check.exec('http://www.somesite.com', siteInfo).then(function () {
          // Check that no issue was found
          (check.data.focusPages[0].issues === undefined).should.be.true;

          // Check for winston log
          winston.error.calledWith(Mustache.render(check.ERROR_DETAILS, {
            url: 'http://www.google.com/1',
            error: 'Bad status code when requesting grammar check: 500'
          })).should.be.true;

          winston.error.restore();
          done();
        });
      });
    
      it('gracefully fails on bad XML files', function (done) {
        grammarReqBody = '<</>>>>';
        sinon.stub(winston, 'error');

        fpResults = {
          focusPages: [{
            narrative: 'blah',
            url: 'http://www.google.com/1'
          }]
        };

        check.exec('http://www.somesite.com', siteInfo).then(function () {
          // Check that no issue was found
          (check.data.focusPages[0].issues === undefined).should.be.true;

          // Check for winston log
          winston.error.calledWith(Mustache.render(check.ERROR_DETAILS, {
            url: 'http://www.google.com/1',
            error: 'Could not load grammar XML - Extra content at the end of the document\n'
          })).should.be.true;

          winston.error.restore();
          done();
        });
      });

      it('adds issue if grammatical error is found', function (done) {
        grammarReqBody = '<results><error><string>isnt</string><description>Missing apostrophe</description><precontext>name</precontext></error></results>';

        fpResults = {
          focusPages: [{
            narrative: 'My name isnt Earl.',
            url: 'http://www.google.com/1'
          }]
        };

        check.exec('http://www.somesite.com', siteInfo).then(function () {
          check.data.focusPages[0].issues.should.eql([{
            found: 'isnt',
            description: 'Missing apostrophe',
            context: 'name isnt'
          }]);
        done();
        });
      });
    
    });
  
  });

});