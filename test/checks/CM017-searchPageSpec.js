/* jshint strict: true */
/*global beforeEach, describe, it */

// REQUIRED
var sinon = require('sinon');

var request = require('request'); // include it so we can spy on it
var check = require('../../checks/CM017-search-page-check')();

// END-REQUIRED
describe('Unit: search page check', function () {
  'use strict';

  var page, response, siteInfo,

      resultsErr = '',
      resultsStatus = {},
      resultsBody = '<!doctype html><html lang="en"><head><meta charset="UTF-8" /><title>Document</title></head><body><form action="/Search-Results.shtml"></form></body></html>',
      
      searchResultsErr = '',
      searchResultsStatus = {},
      searchResultsBody = '<!doctype html><html lang="en"><head><meta charset="UTF-8" /><title>Document</title></head><body><form action="/Search-Results.shtml"></form></body></html>';

  beforeEach(function () {
    
    page = { html: '', url: 'http://www.google.com/bad-code.shtml' };
    response = {};
    siteInfo = {
      onlyOn: {
        types: ['both']
      },
      domain: { url: 'http://www.murlawmn.com/' }
    };


    var fakeRequest = function (url, callBack) {
      if (url.indexOf('Search-Results') >= 0) {

        callBack(searchResultsErr, searchResultsStatus, searchResultsBody);

      } else {

        callBack(resultsErr, resultsStatus, resultsBody);

      }
    };

    sinon.stub(request, 'get', fakeRequest);

  });

  afterEach(function () {
      request.get.restore();
  });

  describe('run method', function () {

    it('passes because search form and results page exists', function (done) {

      var prom = check.run({ url: 'http://www.murlawmn.com/' }, siteInfo);
      prom.then(function () {
        done();
      }, done);

    });


    it('passes because no search form exists', function (done) {
      resultsBody = '<!doctype html><html lang="en"><head><meta charset="UTF-8" /><title>Document</title></head><body></body></html>';

      var prom = check.run({ url: 'http://www.murlawmn.com/' }, siteInfo);

      prom.then(function () {

        done();

      }, done);


    });


    it('fails when search page is not found', function (done) {
      resultsBody = '<!doctype html><html lang="en"><head><meta charset="UTF-8" /><title>Document</title></head><body><form id="searchFormDesign" action="/Search-Results.shtml"></form></body></html>';
      searchResultsErr = 'Page not found';

      var prom = check.run({ url: 'http://www.murlawmn.com/' }, siteInfo);

      prom.then(undefined, function () {

        check.data[0].message.should.equal('Search page was not found at: http://www.murlawmn.com/Search-Results.shtml');
        done();

      });

    });

  });

});