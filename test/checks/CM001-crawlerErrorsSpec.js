/* jshint strict: true */
/*global beforeEach, describe, it */

/* Mocha plugins */
var sinon = require('sinon');

// REQUIRED
var path = require('path');
var mockPage = require('../mocks/page');
// END-REQUIRED

describe('CM001 - Crawler errors check', function() {
  'use strict';

  var check;
  var page;
  var pagesCrawled;
  var response;

  beforeEach(function() {
    check = require('../../checks/CM001-crawler-errors-check.js')();
    page = mockPage('http://www.google.com/page');
  });

  it('fails if any error occurs in a crawl', function () {
    page.error = new Error('something');
    check.onError(page);
    check.passed.should.be.false;
  });

  it('shows the URL and error message if a timeout error occurs in a crawl', function () {
    page.error = new Error('something');
    check.onError(page);
    check.data[0].url.should.equal('http://www.google.com/page');
  });

  function itIsExcluded() {

    it('returns false', function () {
      var result = check.onError(page, response, pagesCrawled);
      result.should.be.false;
    });

    it('does not add to check.data', function () {
      check.onError(page, response, pagesCrawled);
      check.data.should.eql({});
    });

    it('does not fail check', function () {
      check.onError(page, response, pagesCrawled);
      (check.passed === null).should.be.true;
    });

  }
  
  describe('"ECONNREFUSED" error code', function () {
    
    beforeEach(function () {
      page.error = {
        code: 'ECONNREFUSED'
      };
    });

    itIsExcluded();

  });
  
  describe('"ECONNRESET" error code', function () {
    
    beforeEach(function () {
      page.error = {
        code: 'ECONNRESET'
      };
    });

    itIsExcluded();

  });
  
  describe('"ENOTFOUND" error code', function () {
    
    beforeEach(function () {
      page.error = {
        code: 'ENOTFOUND'
      };
    });

    itIsExcluded();

  });
  
  describe('"EADDRINFO" error code', function () {
    
    beforeEach(function () {
      page.error = {
        code: 'EADDRINFO'
      };
    });

    itIsExcluded();

  });
  
  describe('"ETIMEDOUT" error code', function () {
    
    beforeEach(function () {
      page.error = {
        code: 'ETIMEDOUT'
      };
    });

    itIsExcluded();

  });
  
  describe('"STATUS CODE ERROR" error code', function () {
    
    beforeEach(function () {
      page.error = {
        code: 'STATUS CODE ERROR'
      };
    });

    itIsExcluded();

  });
  
  describe('"DOMNodeInsertedIntoDocument" error message', function () {
    
    beforeEach(function () {
      page.error = {
        message: 'DOMNodeInsertedIntoDocument'
      };
    });

    itIsExcluded();

  });
  
  describe('"Exceeded maxRedirects" error message', function () {
    
    beforeEach(function () {
      page.error = {
        message: 'Exceeded maxRedirects'
      };
    });

    itIsExcluded();

  });
  
  describe('"Invalid URI" error message', function () {
    
    beforeEach(function () {
      page.error = {
        message: 'Invalid URI'
      };
    });

    itIsExcluded();

  });
  
  describe('"SSL23_GET_SERVER_HELLO:unknown protocol" error message', function () {
    
    beforeEach(function () {
      page.error = {
        message: 'SSL23_GET_SERVER_HELLO:unknown protocol'
      };
    });

    itIsExcluded();

  });
  
  describe('"Protocol not supported" error message', function () {
    
    beforeEach(function () {
      page.error = {
        message: 'Protocol:file: not supported.: file://some-path/blah.html'
      };
    });

    itIsExcluded();

  });

});