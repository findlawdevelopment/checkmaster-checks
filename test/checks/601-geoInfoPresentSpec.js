/*jshint -W030 */
/*global beforeEach, describe, it */

var mockPage = require('../mocks/page');

describe('601 - geo info present check', function () {
  'use strict';

  var check;
  var page;

  beforeEach(function () {
    check = require('../../checks/601-geo-info-present-check')();
    page = mockPage('http://google.com');
  });

  describe('when .geography is present and with content', function () {
  
      beforeEach(function () {
          page.html = '<div class="geography">I have content</div>';
          check.onPageCrawl(page);
      });

      successSituations();
  
  });

  describe('when #geographicalFooter is present and with content', function () {
  
      beforeEach(function () {
          page.html = '<div id="geographicalFooter">I have content</div>';
          check.onPageCrawl(page);
      });

      successSituations();
  
  });

  function successSituations() {

    it('passes check', function () {
      (check.passed === null).should.be.true;
    });

    it('does not flag check as needing followup', function () {
      check.needsFollowUp.should.be.false;
    });

    it('does not add anything to data', function () {
      check.data.should.eql([]);
    });

  }

  describe('when .geography is missing', function () {

    beforeEach(function () {
      page.html = 'missing';
    });

    failureSituations();
  
  });

  describe('when .geography is present, but without content', function () {

    beforeEach(function () {
      page.html = '<div class="geography">                           \t\t\t\n\n   </div>';
    });

    failureSituations();
  
  });

  describe('when #geographicalFooter is present, but without content', function () {

    beforeEach(function () {
      page.html = '<div id="geographicalFooter">                           \t\t\t\n\n   </div>';
    });

    failureSituations();
  
  });

  function failureSituations() {

    it('flags check as needing followup if .geography is missing', function () {
      check.onPageCrawl(page);
      check.needsFollowUp.should.be.true;
    });

    it('does not fail check', function () {
      check.onPageCrawl(page);
      (check.passed === null).should.be.true;
    });

    it('adds URL to data', function () {
      check.onPageCrawl(page);
      check.data.should.eql([{
        isMissing: null,
        url: 'http://google.com'
      }]);
    });

    it('does not fail on non-html pages', function () {
      page.type = 'image/jpeg';
      check.onPageCrawl(page);
      (check.passed === null).should.be.true;
      check.data.should.eql([]);
    });

  }

});
