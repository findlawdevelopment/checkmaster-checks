/* jshint strict: true */
/*global beforeEach, describe, it */

/* Mocha plugins */

var sinon = require('sinon');


// REQUIRED
var Q = require('q');

var request = require('request');


describe('quantity post check', function () {
  'use strict';

  var check, page, siteInfo;

  beforeEach(function () {
    check = require('../../checks/CM300-quantity-post-check.js')();
    check._blogURLs = ['http://www.google.com'];

    // default html that request.get will return
    page = '<!doctype html><html lang="en"><head><meta charset="UTF-8" /><title>Document</title></head><body> <ul id="archives"><li>posts (10)</li><li>other posts (1)</li></ul> <article class="content"><div class="post"></div><div class="post"></div><div class="post"></div><div class="post"></div><div class="post"></div><div class="post"></div><div class="post"></div><div class="post"></div><div class="post"></div><div class="post"></div><div class="post"></div></article></body></html>';
    
    sinon.stub(request, 'get', function (url, callback) {
      callback('', {}, page);
    });

    siteInfo = {};
  });

  afterEach(function () {

    // cleanup stub
    request.get.restore();

  });

  describe('passes', function () {

    it('finds at least 11 posts', function (done) {

      var promise = check.run('http://www.google.com', siteInfo);

      promise.then(function () {

        // Resolving means it passed
        done();

      }, done);

    });
  
  });

  describe('fails', function () {
    
    it('doesnt find at least 11 posts', function (done) {
      page = '<!doctype html><html lang="en"><head><meta charset="UTF-8" /><title>Document</title></head><body><article class="content"><div class="post"></div></article></body></html>'

      check.run('http://google.com', siteInfo).then(undefined, function (err) {
        // test specific html that request.get will return
        
        err.should.be.instanceOf(Error).and.have.property('message', 'Not enough posts');

        done();

        // the below done will be called with your error if the promise is rejected;
      });

    });


  });

  describe('Specific methods:', function () {
    
    var cheerio;

    before(function () {
        
      cheerio = require('cheerio');

    });

    describe('countArchiveLinks method', function () {
      
      it('should return 10', function () {
        
        var links = ['archive (5)', 'archive (2)', 'archive (3)',]
        var count = check.countArchiveLinks(links);

        count.should.eql(10);

      });

      it('should return 0 when links is undefined', function () {
        
        var count = check.countArchiveLinks();

        count.should.eql(0);

      });

      it('doesn\'t error when a number isn\'t found on a link ', function () {
        
        var links = ['complete archives'];

        check.countArchiveLinks(links).should.eql(0);

      });

    });

    describe('getArchiveLinks method', function () {

      it('finds 2 links', function () {
        
        var html = '<!doctype html><html lang="en"><head><meta charset="UTF-8" /><title>Document</title></head><body><ul id="archives"><li>some text</li><li>more text</li></ul></body></html>'

        var links = check.getArchiveLinks(cheerio.load(html));

        links.length.should.eql(2);

      });
      
    });

  });


});