/*jshint -W030 */
/*global beforeEach, describe, it */

var mockPage = require('../mocks/page');

describe('CM010 - missing or very little content check', function () {
  'use strict';

  var check;
  var page;

  beforeEach(function () {
    check = require('../../checks/CM010-missing-content-check')();
    check.setSiteData('http://google.com/', {
      pages: {}
    });
    page = mockPage('http://google.com/');
  });

  it('fails when no content element is found', function () {
    page.html = '';
    check.onPageCrawl(page);
    check.passed.should.be.false;
  });

  describe('#content pages', function () {
  
    situations('<section id="content">', '</section>');
  
  });

  describe('.content pages', function () {
  
    situations('<section class="content">', '</section>');
  
  });

  function situations(elemStart, elemEnd) {

    it('adds URL to data upon failure', function () {
      page.html = elemStart + '' + elemEnd;
      check.onPageCrawl(page);
      check.data[0].url.should.eql('http://google.com/');
    });

    it('fails when page content contains only spaces', function () {
      page.html = elemStart + '                              \t\n\r<p>  </p>' + elemEnd;
      check.onPageCrawl(page);
      check.passed.should.be.false;
    });

    it('Sets needsFollowUp when 0 < page > 500', function () {
      page.html = elemStart + 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa' + elemEnd;
      check.onPageCrawl(page);
      check.needsFollowUp.should.be.true;
    });

    it('Fails when 0 === page', function () {
      page.html = elemStart + '' + elemEnd;
      check.onPageCrawl(page);
      check.passed.should.be.false;
    });

    it('does not fail when page content is less than 500 and a link (it\'s carryover)', function () {
      page.html = elemStart + 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaahttp://www.google.com/aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa' + elemEnd;
      check.onPageCrawl(page);
      (check.passed === null).should.be.true;
    });

    it('does not fail when page content contains at least 500 characters', function () {
      page.html = elemStart + 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa' + elemEnd;
      check.onPageCrawl(page);
      (check.passed === null).should.be.true;
    });

    it('does not fail when the page is the disclaimer page', function () {
      page.html = elemStart + '' + elemEnd;
      check.setSiteData('http://domain.com/', {
        pages: {
          disclaimer: ['Something'] // Declare disclaimer page as page ID: Disclaimer
        }
      });
      page.url = 'http://domain.com/Something.shtml';
      check.onPageCrawl(page);
      (check.passed === null).should.be.true;
    });

    it('does not fail when the pageID contains "disclaimer" (not case sensitive)', function () {
      page.html = elemStart + '' + elemEnd;
      page.url = 'http://domain.com/Disclaimer.shtml';
      check.onPageCrawl(page);
      (check.passed === null).should.be.true;
    });

    it('does not fail when the url ends with "atom.xml"', function () {
      page.html = elemStart + '' + elemEnd;
      page.url = 'http://domain.com/whatever/atom.xml';
      check.onPageCrawl(page);
      (check.passed === null).should.be.true;
    });

    it('does not fail when the page content contains a form', function () {
      page.html = elemStart + '<form></form>' + elemEnd;
      check.onPageCrawl(page);
      (check.passed === null).should.be.true;
    });

    it('does not fail when the page is not a text/html content-type', function () {
      page.html = elemStart + '' + elemEnd;
      page.type = 'application/pdf';
      check.onPageCrawl(page);
      (check.passed === null).should.be.true;
    });

    it('does not fail when the page contains an <object> (video)', function () {
      page.html = elemStart + '<object></object>' + elemEnd;
      check.onPageCrawl(page);
      (check.passed === null).should.be.true;
    });

    describe('map pages', function () {

      it('does not fail check', function () {
        page.html = elemStart + '<iframe src="https://maps.google.com/maps"></iframe>' + elemEnd;
        check.onPageCrawl(page);
        (check.passed === null).should.be.true;
      });

      it('does not throw exceptions when iframe has no "src"', function () {
        page.html = elemStart + '<iframe></iframe>' + elemEnd;
        check.onPageCrawl(page);
      });
    
    });

    describe('pages with link lists', function () {

      it('does not fail check when there are two <a> tags', function () {
        page.html = elemStart + '<a href="">link</a><a href="">link 2</a>' + elemEnd;
        check.onPageCrawl(page);
        (check.passed === null).should.be.true;
      });

      it('does not fail check when there are more than two <a> tags', function () {
        page.html = elemStart + '<a href="">link</a><a href="">link 2</a><a href="">link 3</a><a href="">link 4</a><a href="">link 5</a>' + elemEnd;
        check.onPageCrawl(page);
        (check.passed === null).should.be.true;
      });

      it('fails check when page only contains a single anchor', function () {
        page.html = elemStart + '<a href="">link</a>' + elemEnd;
        check.onPageCrawl(page);
        check.needsFollowUp.should.be.true;
      });
    
    });
  }

});
