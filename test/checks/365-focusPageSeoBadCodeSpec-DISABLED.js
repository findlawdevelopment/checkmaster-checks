/* jshint strict: true */
/*global beforeEach, describe, it */

/* Mocha plugins */

var sinon = require('sinon');

// REQUIRED
var Mustache = require('mustache');
var Q = require('q');
var request = require('request');
var winston = require('winston');
var gfp = require('../../checks/utils/getFocusPages');
var gfpConstants = gfp.constants;

describe.skip('365 - focus page seo bad code check', function () {
	'use strict';

	var check;
  var fpResults;
  var siteInfo;

	beforeEach(function () {
    check = require('../../checks/365-focus-page-seo-bad-code-check.js')();

    fpResults = {};

    sinon.stub(gfp, 'getFocusPages', function () {
      return {
        then: function (callback) {
          callback(fpResults);
        }
      }
    });

    siteInfo = {
      getFocusPages: gfp.getFocusPages
    };
  });

  afterEach(function () {
    gfp.getFocusPages.restore();
  });

  it('always passes', function (done) {
    fpResults = {
      error: 'some error',
      url: 'http://www.errorurl.com'
    }
    check.exec('http://www.killackeylaw.com', siteInfo).then(function () {
      check.passed.should.be.true;
      done();
    });
  });

  it('sets needsFollowUp to true when anything but "no Focus Pages found" occurs', function (done) {
    fpResults = {
      error: 'some error',
      url: 'http://www.errorurl.com'
    };
    check.exec('http://www.somesite.com', siteInfo).then(function () {
      check.needsFollowUp.should.be.true;
      done();
    });
  });

  it('does not set needsFollowUp if no Focus Pages were found', function (done) {
    fpResults = {
      error: gfpConstants.NO_FOCUS_PAGES,
      url: 'http://www.errorurl.com'
    };
    check.exec('http://www.somesite.com', siteInfo).then(function () {
      check.needsFollowUp.should.not.be.true;
      done();
    });
  });

  it('marks the error as non-fatal if no Focus Pages were found', function (done) {
    fpResults = {
      error: gfpConstants.NO_FOCUS_PAGES,
      url: 'http://www.errorurl.com'
    };
    check.exec('http://www.somesite.com', siteInfo).then(function () {
      check.data.nonFatal.should.be.true;
      done();
    });
  });

  describe('data setting', function () {

    it('sets check.data to the results from getFocusPages', function (done) {
      fpResults = {
        error: 'some error',
        url: 'http://www.errorurl.com'
      };
      check.exec('http://www.somesite.com', siteInfo).then(function () {
        check.data.should.eql(fpResults);
        done();
      });
    });

    it('strips unrelated text from data', function (done) {
      fpResults = {
        focusPages: [{
          title: 'my title',
          narrative: 'something',
          somethingNew: 'blah',
          meta: {
            title: 'title tag',
            description: 'my description',
            keywords: 'my keywords'
          },
          practicePhrase: 'practice phrase',
          lawyerPhrase: 'lawyer phrase',
          url: 'http://www.google.com',
        }]
      };
      check.exec('http://www.somesite.com', siteInfo).then(function () {
        check.data.should.eql({
          focusPages: [{
              meta: {
                title: 'title tag',
                description: 'my description',
                keywords: 'my keywords'
              },
              practicePhrase: 'practice phrase',
              lawyerPhrase: 'lawyer phrase',
              url: 'http://www.google.com',
          }]
        }); 
        done();
      });
    });

    it('strips unrelated text from data', function (done) {
      fpResults = {
        focusPages: [{
          title: 'my title',
          narrative: 'something',
          meta: {
            title: 'title tag',
            description: 'my description',
            keywords: 'my keywords'
          },
          practicePhrase: 'practice phrase',
          lawyerPhrase: 'lawyer phrase',
          url: 'http://www.google.com',
        }]
      };
      check.exec('http://www.somesite.com', siteInfo).then(function () {
        check.data.should.eql({
          focusPages: [{
              meta: {
                title: 'title tag',
                description: 'my description',
                keywords: 'my keywords'
              },
              practicePhrase: 'practice phrase',
              lawyerPhrase: 'lawyer phrase',
              url: 'http://www.google.com',
          }]
        }); 
        done();
      });
    });
  
  });

});