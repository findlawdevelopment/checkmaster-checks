/* jshint strict: true */
/*global beforeEach, describe, it */

/* Mocha plugins */


// REQUIRED
var Q = require('q');
var cheerio = require('cheerio');
var fs = require('fs');

describe('blog test posts check:', function () {
  'use strict';

  var check, page = {}, html;

  before(function () {

    page.url = 'http://www.google.com';
    page.dom = function () {
      return cheerio.load(page.html);
    };

  });

  beforeEach(function () {
    check = require('../../checks/342-blog-test-posts-check.js')();
    check._blogURLs = ['http://www.google.com'];
  });

  describe('check passes:', function () {

    beforeEach(function () {
      // default html that the crawler will return
      page.html = '<!doctype html><html lang="en"><head><meta charset="UTF-8" /><title>Document</title></head><body class="blog-entry"><article class="content"></article></body></html>';
    });
    
    it('doesnt find test posts', function (done) {
      
      check.onPageCrawl(page, {}, []).then(function () {

        check.onCrawlFinish();
        check.passed.should.be.true;

        done();

      }, done);

    });

  });

  describe('check fails:', function () {
    

    it('by finding test tags', function (done) {

      page.html = '<!doctype html><html lang="en"><head><meta charset="UTF-8" /><title>Document</title></head><body class="blog-entry"><section id="tagCloud" class="group nav-tag-cloud"> <h4>Popular Topics</h4> <ul> <li class="itemFirst item-first tagRank10 tag-rank-10"><a href="/mt-bin/mt-search.cgi?IncludeBlogs=31089&amp;tag=Test%20Comments%20Disabled" rel="tag">Test Comments Disabled</a></li> <li class="tagRank8 tag-rank-8"><a href="/mt-bin/mt-search.cgi?IncludeBlogs=31089&amp;tag=Test%20Extended%20Post" rel="tag">Test Extended Post</a></li> <li class="tagRank6 tag-rank-6"><a href="/mt-bin/mt-search.cgi?IncludeBlogs=31089&amp;tag=Test%20G%2B" rel="tag">Test G+</a></li> <li class="tagRank8 tag-rank-8"><a href="/mt-bin/mt-search.cgi?IncludeBlogs=31089&amp;tag=Test%20List%20Post" rel="tag">Test List Post</a></li> <li class="tagRank8 tag-rank-8"><a href="/mt-bin/mt-search.cgi?IncludeBlogs=31089&amp;tag=Test%20Long%20Post" rel="tag">Test Long Post</a></li> <li class="tagRank10 tag-rank-10"><a href="/mt-bin/mt-search.cgi?IncludeBlogs=31089&amp;tag=Test%20Old%20Post" rel="tag">Test Old Post</a></li> <li class="tagRank10 tag-rank-10"><a href="/mt-bin/mt-search.cgi?IncludeBlogs=31089&amp;tag=Test%20Related1" rel="tag">Test Related1</a></li> <li class="tagRank8 tag-rank-8"><a href="/mt-bin/mt-search.cgi?IncludeBlogs=31089&amp;tag=Test%20Related2" rel="tag">Test Related2</a></li> <li class="tagRank7 tag-rank-7"><a href="/mt-bin/mt-search.cgi?IncludeBlogs=31089&amp;tag=Test%20Related3" rel="tag">Test Related3</a></li> <li class="tagRank6 tag-rank-6"><a href="/mt-bin/mt-search.cgi?IncludeBlogs=31089&amp;tag=Test%20Related4" rel="tag">Test Related4</a></li> <li class="tagRank6 tag-rank-6"><a href="/mt-bin/mt-search.cgi?IncludeBlogs=31089&amp;tag=Test%20Related5" rel="tag">Test Related5</a></li> <li class="itemLast item-last tagRank1 tag-rank-1"><a href="/mt-bin/mt-search.cgi?IncludeBlogs=31089&amp;tag=Test%20Tag" rel="tag">Test Tag</a></li> </ul> </section></body></html>';
      
      check.onPageCrawl(page, {}, []).then(function (data) {

        check.passed.should.be.false;
        done();

      }, done);

    });

    it('by finding test categories', function (done) {
      
      page.html = '<!doctype html><html lang="en"><head><meta charset="UTF-8" /><title>Document</title></head><body class="blog-entry"><section id="categories" class="group nav-categories"> <h4>Categories</h4> <ul> <li class="itemOnly item-only"><a href="/blog/test-category/">Test Category (22)</a></li> </ul> </section></body></html>'

      check.onPageCrawl(page, {}, []).then(function (data) {

        check.passed.should.be.false;
        done();

      }, done);

    });

  });

  describe('needs follow-up', function () {
    
    it('Finds titles that match a test post pattern', function (done) {
      
      page.html = '<!doctype html><html lang="en"><head><meta charset="UTF-8" /><title>Document</title></head><body class="blog-entry"><h1 id="pageTitle" class="page-title">test post</h1></body></html>'

      check.onPageCrawl(page, {}, []).then(function (data) {

        check.needsFollowUp.should.be.true;
        done();

      }, done);

    });

  });

  describe('non blog pages', function () {

    it('does nothing', function (done) {
      
      page.url = 'http://not.ablog.page';
      check.onPageCrawl(page, {}, []).then(function () {

        (check.passed === null).should.be.true;
        check.needsFollowUp.should.not.be.ok;
        done();

      }, done);

    });

  });

  describe('individual methods:', function () {
    var cheerio, html;

    before(function (done) {
        
      cheerio = require('cheerio');
      done();

    });

    describe('getTextFromList method', function () {


      it('returns an array of text', function () {
        
        html = '<!doctype html><html lang="en"><head><meta charset="UTF-8" /><title>Document</title></head><body> <ul class="list"><li>this</li><li>text</li><li>should</li><li>be</li><li>an</li><li>array</li></ul> </body></html>'

        var arr = check.getTextFromList(cheerio.load(html), '.list');
        arr.should.be.instanceOf(Array);
        arr.should.eql(['this', 'text', 'should', 'be', 'an', 'array']);
        
      });
      
    });

    describe('getPageTitle method', function () {

      it('returns a string page title', function () {
        
        html = '<!doctype html><html lang="en"><head><meta charset="UTF-8" /><title>Document</title></head><body class="blog-entry"><div class="page-title">page title</div></body></html>'

        var title = check.getPageTitle(cheerio.load(html));

        title.should.eql('page title');

      });
      
    });

    describe('removeNumberText method', function () {
      
      it('returns an array with no indexes containing the pattern: (#) ', function () {
        
        var arr = ['category one (1)', 'category two (12)', 'category three'];
        var newArr = check.removeNumberText(arr);

        newArr.should.eql(['category one', 'category two', 'category three']);

      });

    });

  });

});