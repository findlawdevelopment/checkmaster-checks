/* jshint strict: true */
/*global beforeEach, describe, it */

var sinon = require('sinon');

// REQUIRED
var Q = require('q');
var mockPage = require('../mocks/page');

describe('site catalyst code matches coportal check', function () {
	'use strict';

	var check, 
			page;

	beforeEach(function () {
		check = require('../../checks/CM201-site-catalyst-code-matches-coportal-check.js')(); // default html that the crawler will return
		page = mockPage('http://www.helzercromar.com');
		page.html = '<!doctype html><html lang="en"><head><meta charset="UTF-8" /><title>Document</title></head><body></body></html>';
		check._siteInfo = {
			"domain": "www.helzercromar.com",
			"subid": "1981066"
		};
		check._siteInfo.reportSuiteID = 'findlaw-15374';
	});

	describe('getSiteCatalystCode method', function () {

		it('should pass when an s_account is found in the HTML', function (done) {

			sinon.stub(console, 'log'); // silence console
			
			page.html = '<!doctype html><html lang="en"><head><meta charset="UTF-8" /><title>Document</title></head><body><!-- Site Catalyst code --> <!-- SiteCatalyst code version: H.20.3. Copyright 1997-2009 Omniture, Inc. More info available at http://www.omniture.com --> <script language=JavaScript type="text/javascript">var s_account="";var s_filter="";var sfl_logicalSite;var sfl_subID;var sfl_charSet="utf-8"</script> <script language=JavaScript type="text/javascript">var s_account="findlaw-15374";var s_filter="javascript:,localhost,helzercromar.com";var sfl_logicalSite="50809";var sfl_subID="1981066";var sfl_charSet="utf-8";</script> <script language=JavaScript type="text/javascript" src="includes/omtr_code.js.pagespeed.jm.Cuckss9Q0l.js"></script> <script language=JavaScript type="text/javascript" src="includes/omtr_variable.js.pagespeed.jm.i2L2q8N_WN.js"></script> <script language=JavaScript type="text/javascript">if(omtr){var omtr_code=omtr.t();if(omtr_code)document.write(omtr_code);}</script> <!--/DO NOT REMOVE/--> <!-- End SiteCatalyst code version: H.20.3. --></body></html>';
						
			check.onPageCrawl(page, {}, {}).then(function (data) {
				
				console.log('CHECK: ' + JSON.stringify(check, null, ' '));

				check.passed.should.be.true;

				console.log.restore();

				done();

			}).done();

		});

		it('should equal the s_account provided in the page.html', function (done) {

			sinon.stub(console, 'log'); // silence console
					
			page.html = '<!doctype html><html lang="en"><head><meta charset="UTF-8" /><title>Document</title></head><body><!-- Site Catalyst code --> <!-- SiteCatalyst code version: H.20.3. Copyright 1997-2009 Omniture, Inc. More info available at http://www.omniture.com --> <script language=JavaScript type="text/javascript">var s_account="";var s_filter="";var sfl_logicalSite;var sfl_subID;var sfl_charSet="utf-8"</script> <script language=JavaScript type="text/javascript">var s_account="findlaw-15374";var s_filter="javascript:,localhost,helzercromar.com";var sfl_logicalSite="50809";var sfl_subID="1981066";var sfl_charSet="utf-8";</script> <script language=JavaScript type="text/javascript" src="includes/omtr_code.js.pagespeed.jm.Cuckss9Q0l.js"></script> <script language=JavaScript type="text/javascript" src="includes/omtr_variable.js.pagespeed.jm.i2L2q8N_WN.js"></script> <script language=JavaScript type="text/javascript">if(omtr){var omtr_code=omtr.t();if(omtr_code)document.write(omtr_code);}</script> <!--/DO NOT REMOVE/--> <!-- End SiteCatalyst code version: H.20.3. --></body></html>';
						
			check.onPageCrawl(page, {}, {}).then(function (data) {
		
				console.log('CHECK: ' + JSON.stringify(check, null, ' '));

				check._siteInfo.reportSuiteID.should.equal('findlaw-15374');

				console.log.restore();

				done();

			}).done();

		});

		it('should pass with when no site catalyst code is found', function (done) {

			sinon.stub(console, 'log'); // silence console
								
			check.onPageCrawl(page, {}, {}).then(function (data) {
		
				console.log('CHECK: ' + JSON.stringify(check, null, ' '));
				check.passed.should.be.true;

				console.log.restore();

				done();

			}).done();

		});

		it('should fail when the s_account does not match the report suite ID', function (done) {

			sinon.stub(console, 'log'); // silence console
								
			page.html = '<!doctype html><html lang="en"><head><meta charset="UTF-8" /><title>Document</title></head><body><!-- Site Catalyst code --> <!-- SiteCatalyst code version: H.20.3. Copyright 1997-2009 Omniture, Inc. More info available at http://www.omniture.com --> <script language=JavaScript type="text/javascript">var s_account="";var s_filter="";var sfl_logicalSite;var sfl_subID;var sfl_charSet="utf-8"</script> <script language=JavaScript type="text/javascript">var s_account="findlaw-14000";var s_filter="javascript:,localhost,helzercromar.com";var sfl_logicalSite="50809";var sfl_subID="1981066";var sfl_charSet="utf-8";</script> <script language=JavaScript type="text/javascript" src="includes/omtr_code.js.pagespeed.jm.Cuckss9Q0l.js"></script> <script language=JavaScript type="text/javascript" src="includes/omtr_variable.js.pagespeed.jm.i2L2q8N_WN.js"></script> <script language=JavaScript type="text/javascript">if(omtr){var omtr_code=omtr.t();if(omtr_code)document.write(omtr_code);}</script> <!--/DO NOT REMOVE/--> <!-- End SiteCatalyst code version: H.20.3. --></body></html>';

			check.onPageCrawl(page, {}, {}).then(function (data) {
		
				console.log('CHECK: ' + JSON.stringify(check, null, ' '));
				check.passed.should.be.false;

				console.log.restore();

				done();

			}).done();

		});
	
	});

});