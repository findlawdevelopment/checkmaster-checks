/* jshint strict: true */
/*global beforeEach, describe, it */

var childProcess = require('child_process');
var fs = require('fs');
var http = require('http');
var pathlib = require('path');
var urllib = require('url');
var request = require('request');
var sinon = require('sinon');

// END-REQUIRED
describe('CM013 - all images are optimized check', function () {
  'use strict';

  var check;
  var page;
  var responseBody;
  var server;

  beforeEach(function () {
    check = require('../../checks/CM013-images-optimized-check')();
    page = {
      html: '',
      url: 'http://localhost:6767',
      urlData: urllib.parse('http://localhost:6767')
    };

    server = http.createServer(function (req, res) {
      var body;
      var request = req.url;
      var contentType;
      var statusCode = 200;

      res.setHeader('Content-Type', 'text/html');

      if (request.match(/\.(jpg|jpeg|gif|png)$/) !== null) {

        // JPEG
        if (
          request.indexOf('.jpeg', request.length - 5) > -1 ||
          request.indexOf('.jpg', request.length - 4) > -1
        ) {
          contentType = 'image/jpeg';
        }

        // PNG
        if (request.indexOf('.png', request.length - 4) > -1) {
          contentType = 'image/png';
        }

        // GIF
        if (request.indexOf('.gif', request.length - 4) > -1) {
          contentType = 'image/gif';
        }

        // Broken image
        if (request.indexOf('idontexist') > -1) {
          statusCode = 404;
        }

        res.writeHead(statusCode, { 'Content-Type': contentType });

        if (statusCode === 200) {
          body = fs.readFileSync(pathlib.join(__dirname, 'CM013-assets', req.url.replace(/.*\//gi, '')));
        } else {
          body = '';
        }
      }

      res.end(body);
    }).listen(6767);

    // spyOn(console, 'error'); // Silence console.errors

  });

  afterEach(function () {
    server.close();
  });

  it('resolves relative image paths', function (done) {
    page.url = 'http://localhost:6767/section/page.html';
    page.html = '<img src="../fail-image.jpg">';
    check.onPageCrawl(page).then(function () {
      check.passed.should.be.false;
      check.data.should.have.keys('http://localhost:6767/fail-image.jpg');
      done();
    });
  });

  it('fails if a JPG image optimization ratio is greater than or equal to 10%', function (done) {
    page.html = '<img src="/fail-image.jpg">';
    check.onPageCrawl(page).then(function () {
      check.passed.should.be.false;
      done();
    });
  });

  it('fails if a JPEG image optimization ratio is greater than or equal to 10%', function (done) {
    page.html = '<img src="/fail-image.jpeg">';
    check.onPageCrawl(page).then(function () {
      check.passed.should.be.false;
      done();
    });
  });

  it('fails if a PNG image optimization ratio is greater than or equal to 10%', function (done) {
    page.html = '<img src="/fail-image.png">';
    check.onPageCrawl(page).then(function () {
      check.passed.should.be.false;
      done();
    });
  });

  it('fails if a GIF image optimization ratio is greater than or equal to 10%', function (done) {
    page.html = '<img src="/fail-image.gif">';
    check.onPageCrawl(page).then(function () {
      check.passed.should.be.false;
      done();
    });
  });

  it('fails if an invalid GIF image is found', function (done) {
    sinon.stub(console, 'error'); // silence console errors
    page.html = '<img src="/invalid.gif">';
    check.onPageCrawl(page).then(function () {
      check.passed.should.be.false;
      console.error.restore();
      done();
    });
  });

  it('passes if all images\' optimization ratios are less than 10%', function (done) {
    page.html = '<img src="/good-image.jpg"><img src="/good-image.jpeg"><img src="/good-image.png"><img src="/good-image.gif">';
    check.onPageCrawl(page).then(function () {
      (check.passed === null).should.be.true;
      done();
    });
  });

  it('skips external images', function (done) {
    page.html = '<img src="http://domain.com/blah.jpg">';
    check.onPageCrawl(page).then(function () {
      (check.passed === null).should.be.true;
      Object.keys(check.data).should.have.a.lengthOf(0);
      done();
    });
  });

  it('does not re-download already downloaded images', function (done) {
    var downloadImage = check.downloadImage;
    sinon.stub(check, 'downloadImage', downloadImage.bind(check));
    check.data = {
      'http://localhost:6767/fail-image.jpg': {
      }
    };
    page.html = '<img src="/fail-image.jpg">';
    check.onPageCrawl(page).then(function () {
      check.data.should.eql({
        'http://localhost:6767/fail-image.jpg': {
        }
      });
      check.downloadImage.called.should.be.false;
      check.downloadImage.restore();
      done();
    });
  });

  describe('failure data', function () {

    beforeEach(function (done) {
      sinon.stub(console, 'error'); // silence console errors
      page.html = '<img src="/fail-image.jpg"><img src="/fail-image.png"><img src="/good-image.gif"><img src="/invalid.gif"><img src="/idontexist.png">';
      check.onPageCrawl(page).then(function () {
        console.error.restore();
        done();
      });
    });

    it('includes all image URLs', function () {
      Object.keys(check.data).should.containEql('http://localhost:6767/fail-image.jpg');
      Object.keys(check.data).should.containEql('http://localhost:6767/good-image.gif');
      Object.keys(check.data).should.containEql('http://localhost:6767/invalid.gif');
      Object.keys(check.data).should.containEql('http://localhost:6767/idontexist.png');
    });

    it('includes the original size of each image', function () {
      check.data['http://localhost:6767/fail-image.jpg'].size.should.equal(37103);
      check.data['http://localhost:6767/fail-image.png'].size.should.equal(122997);
      check.data['http://localhost:6767/good-image.gif'].size.should.equal(2619);
      check.data['http://localhost:6767/invalid.gif'].size.should.equal(999);
      (check.data['http://localhost:6767/idontexist.png'].size === undefined).should.be.true;
    });

    it('includes the savings % for each image', function () {
      check.data['http://localhost:6767/fail-image.jpg'].savings.should.equal(23);
      check.data['http://localhost:6767/fail-image.png'].savings.should.equal(50);
      check.data['http://localhost:6767/good-image.gif'].savings.should.equal(0);
      check.data['http://localhost:6767/invalid.gif'].savings.should.equal(100);
      (check.data['http://localhost:6767/idontexist.png'].savings === undefined).should.be.true;
    });

    it('includes error message for invalid GIF files', function () {
      (check.data['http://localhost:6767/good-image.gif'].error === undefined).should.be.true;
      check.data['http://localhost:6767/invalid.gif'].error.should.equal('Invalid GIF file.');
      check.data['http://localhost:6767/idontexist.png'].error.should.equal('Image could not be downloaded: (404 status code)');
    });
  
  });

  it('captures responses from stderr when using pngcrush (bug: CHECK-330)', function (done) {
    sinon.stub(console, 'error'); // silence console errors
    sinon.stub(childProcess, 'spawn', function () {
      return {
        on: function (event, callback) {
          callback();
        },
        stderr: {
          on: function (event, callback) {
            // Output "no filesize change" in stderr
            callback('(no filesize change)');
          }
        },
        stdout: {
          on: function (event, callback) {
            callback('');
          }
        }
      };
    });

    page.html = '<img src="/fail-image.png">';
    check.onPageCrawl(page).then(function () {
      check.data['http://localhost:6767/fail-image.png'].savings.should.equal(0);
      childProcess.spawn.restore();
      console.error.restore();
      done();
    });
  });

});