/* jshint strict: true */
/*global beforeEach, describe, expect, it */

// REQUIRED
var Q = require('q');
var request = require('request');
var sinon = require('sinon');
// END-REQUIRED

describe('webmaster tools verification check', function () {
	'use strict';

	var check, url, siteInfo,responsePreview, responseLive,
			previewBody, liveBody, requestSpy;

	beforeEach(function () {
		check = require('../../checks/CM012-webmaster-tools-verification-check')();

		responsePreview = {
				statusCode: 200
		};
		responseLive = {
				statusCode: 200
		};
		previewBody = '<!DOCTYPE html> <!--[if lte IE 7]><html id="ie" class="no-js" lang="en" xmlns="http://www.w3.org/1999/xhtml"><![endif]--> <!--[if IE 8]><html id="ie8" class="no-js" lang="en" xmlns="http://www.w3.org/1999/xhtml"><![endif]--> <!--[if IE 9]><html id="ie9" class="no-js" lang="en" xmlns="http://www.w3.org/1999/xhtml"><![endif]--> <!--[if gt IE 9]><!--><html lang="en" class="no-js" xmlns="http://www.w3.org/1999/xhtml"><!--<![endif]--> <head> <meta charset="utf-8"/> <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/> <meta name="keywords" content=""/> <meta name="description" content=""/> <meta name="viewport" content="width=device-width"/> <meta name="google-site-verification" content="kkFM9liY8CUmA7mgnZBPSPwQzV2m2PUTrlyFqXRUyC0"/> <link rel="stylesheet" href="/design/css/site.css"/> </head> <body> </body> </html>';
		liveBody = '<!DOCTYPE html> <!--[if lte IE 7]><html id="ie" class="no-js" lang="en" xmlns="http://www.w3.org/1999/xhtml"><![endif]--> <!--[if IE 8]><html id="ie8" class="no-js" lang="en" xmlns="http://www.w3.org/1999/xhtml"><![endif]--> <!--[if IE 9]><html id="ie9" class="no-js" lang="en" xmlns="http://www.w3.org/1999/xhtml"><![endif]--> <!--[if gt IE 9]><!--><html lang="en" class="no-js" xmlns="http://www.w3.org/1999/xhtml"><!--<![endif]--> <head> <meta charset="utf-8"/> <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/> <meta name="keywords" content=""/> <meta name="description" content=""/> <meta name="viewport" content="width=device-width"/> <meta name="google-site-verification" content="kkFM9liY8CUmA7mgnZBPSPwQzV2m2PUTrlyFqXRUyC0"/> <link rel="stylesheet" href="/design/css/site.css"/> </head> <body> </body> </html>';
		url = 'http://daniellalevilaw.firmsitepreview.com/';
		siteInfo = {
			onlyOn: {
				types: ['preview']
			},
			domain: 'www.levilawny.com'
		};

		sinon.stub(request, 'get', function (url, callback) {
			if (url.indexOf('firmsitepreview') > -1) {
				callback(null, responsePreview, previewBody);	
			} else {
				callback(null, responseLive, liveBody);
			}
		});
	});

	afterEach(function () {
	    request.get.restore();
	});

	it('checks for the home page', function (done) {
			check.exec({ url: url }, siteInfo).then(function() {
				check._siteInfo.domain.should.eql('www.levilawny.com');
				done();
			});
	});

	it('checks for the existing Google Webmaster Tools verification code', function (done) {
			check.exec({ url: url }, siteInfo).then(function() {
				check.passed.should.be.true;
				done();
			});
	});

	it('the Google Webmaster Tools verification code should pass if it does not exist on the live and preview websites', function (done) {
			previewBody = '';
			liveBody = '';
			check.exec({ url: url }, siteInfo).then(function() {
				check.passed.should.be.true;
				done();
			});
	});

	it('the Google Webmaster Tools verification code should fail if it does not appear on the preview site but it does on the live', function (done) {
			previewBody = '';
			check.exec({ url: url }, siteInfo).then(function() {
				check.passed.should.eql(false);
				done();
			});
	});

	it('passes if the live site returns a non 200 status code', function (done) {
			previewBody = '';
			responseLive.statusCode = 404;
			check.exec({ url: url }, siteInfo).then(function() {
				check.passed.should.be.true;
				done();
			});
	});

});