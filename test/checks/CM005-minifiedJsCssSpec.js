/* jshint strict: true */
/*global beforeEach, describe, expect, it */

// REQUIRED
var Q = require('q');
var request = require('request');
var sinon = require('sinon');
var _ = require('lodash');
var mockPage = require('../mocks/page');
// END-REQUIRED

describe('minified js css check', function () {
	'use strict';

	var check,
		response,
		page,
		pagesCrawled;

	beforeEach(function () {
		check = require('../../checks/CM005-minified-js-css-check')();
		response = {};
		pagesCrawled = {};
		page = mockPage('http://www.google.com');
	});

	describe('promise', function () {
	
		beforeEach(function () {
			sinon.stub(request, 'get', function (url, callback) {
				var includeContents = '';

				if (url === 'http://www.google.com/design/scripts/script.js') {
					includeContents = 'var blah = "1234567890123456789012345678901234567890123456789012345678901234567890123456781234567890123456789012345678901234567890123456789012345678901234567890123456781234567890123456789012345678901234567890123456789012345678901234567890123456781234567890123456789012345678901234567890123456789012345678901234567890123456781234567890123456789012345678901234567890123456789012345678901234567890123456781234567890123456789012345678901234567890123456789012345678901234567890123456781234567890123456789012345678901234567890123456789012345678901234567890123456781234567890123456789012345678901234567890123456789012345678901234567890123456781234567890123456789012345678901234567890123456789012345678901234567890123456781234567890123456789012345678901234567890123456789012345678901234567890123456781234567890123456789012345678901234567890123456789012345678901234567890123456781234567890123456789012345678901234567890123456789012345678901234567890123456781234567890123456789012345678901234567890123456789012345678901234567890123456781234567890123456789012345678901234567890123456789012345678901234567890123456781234567890123456789012345678901234567890123456789012345678901234567890123456781234567890123456789012345678901234567890123456789012345678901234567890123456781234567890123456789012345678901234567890123456789012345678901234567890123456781234567890123456789012345678901234567890123456789012345678901234567890123456781234567890123456789012345678901234567";                                                                                                ';
				} else {
					includeContents = 'var blah = "123456789012345671234567890123456712345678901234567123456789012345671234567890123456712345678901234567123456789012345671234567890123456712345678901234567123456789012345123456789012345671234567890123456712345678901234567123456789012345671234567890123456712345678901234567123456789012345671234567890123456712345678901234567123456789012345123456789012345671234567890123456712345678901234567123456789012345671234567890123456712345678901234567123456789012345671234567890123456712345678901234567123456789012345";                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                ';
				}

				setTimeout(function () {
					callback(null, {}, includeContents);
				}, 200);
			});
		});

		afterEach(function () {
		  request.get.restore();
		});

		it('is resolved after all includes are processed', function (done) {
			page.html = '<script src="/design/scripts/script.js"></script><script src="/design/scripts/script2.js"></script>';
			var promise = check.exec(page, response, pagesCrawled);
			promise.then(function () {
				check.data['http://www.google.com/design/scripts/script.js'].isProcessing.should.be.false;
				check.data['http://www.google.com/design/scripts/script2.js'].isProcessing.should.be.false;
				done();
			});
		});
	
	});

	describe('include gathering', function () {
	
		beforeEach(function () {
			sinon.stub(request, 'get');
		});

		afterEach(function () {
	    request.get.restore();
		});

		it('gathers every JS include for a page', function () {
			page.html = '<script src="/design/scripts/script.js"></script><script src="/design/scripts/script2.js"></script>';
			check.exec(page, response, pagesCrawled).then(function () {
				_.keys(check.data).should.eql(['http://www.google.com/design/scripts/script.js', 'http://www.google.com/design/scripts/script2.js']);
			});
		});

		it('gathers every CSS include for a page', function () {
			page.html = '<link rel="stylesheet" href="/design/css/css.css"></link rel="stylesheet"><link rel="stylesheet" href="/design/css/css2.css"></link rel="stylesheet">';
			check.exec(page, response, pagesCrawled).then(function () {
				_.keys(check.data).should.eql(['http://www.google.com/design/css/css.css', 'http://www.google.com/design/css/css2.css']);
			});
		});

		it('requests every include', function () {
			page.html = '<script src="/design/scripts/script.js"></script><link rel="stylesheet" src="/design/css/css.css">';
			check.exec(page, response, pagesCrawled).then(function () {
				request.get.callCount.should.eql(2);
				request.get.calledWith('http://www.google.com/design/scripts/script.js', sinon.match.func);
				request.get.calledWith('http://www.google.com/design/css/css.css', sinon.match.func);
			});
		});

		it('does not gather includes twice', function () {
			page.html = '<script src="/design/scripts/script.js"></script><script src="/design/scripts/script.js"></script>';
			check.exec(page, response, pagesCrawled).then(function () {
				_.keys(check.data).should.eql(['http://www.google.com/design/scripts/script.js']);
				request.get.callCount.should.eql(1);
			});
		});

		it('gathers video CSS as separate includes', function () {
			page.html = '<param name=css value="mediaControls=http://cpl-coh.firmsitepreview.com/content/css/Video/mediaControls.css&amp;videoList=http://cpl-coh.firmsitepreview.com/content/css/Video/videoList.css&amp;playlistCombo=http://cpl-coh.firmsitepreview.com/content/css/Video/playlistCombo.css"/>';
			check.exec(page, response, pagesCrawled).then(function () {
				_.keys(check.data).should.eql([
					'http://cpl-coh.firmsitepreview.com/content/css/Video/mediaControls.css',
					'http://cpl-coh.firmsitepreview.com/content/css/Video/videoList.css',
					'http://cpl-coh.firmsitepreview.com/content/css/Video/playlistCombo.css'
				]);
				request.get.callCount.should.eql(3);
			});
		});

		it('does not gather google analytics code (was a bug)', function () {
			page.html = '<script type="text/javascript">var _gaq=_gaq||[];_gaq.push([\'_setAccount\',\'UA-XXXXX-X\']);_gaq.push([\'_trackPageview\']);(function(){var ga=document.createElement(\'script\');ga.type=\'text/javascript\';ga.async=true;ga.src=(\'https:\'==document.location.protocol?\'https://ssl\':\'http://www\')+\'.google-analytics.com/ga.js\';var s=document.getElementsByTagName(\'script\')[0];s.parentNode.insertBefore(ga,s);})();</script>';
			check.exec(page, response, pagesCrawled).then(function () {
				_.keys(check.data).length.should.eql(0);
			});
		});
	
	});

	describe('minification comparison', function () {
		var error,
			response,
			includeContents;
	
		beforeEach(function () {
			response = {
				statusCode: 200
			};
			error = null;
			sinon.stub(request, 'get', function (url, callback) {
				callback(error, response, includeContents);
			});
		});

		afterEach(function () {
		    request.get.restore();
		});

		describe('for JavaScript', function () {

			it('does not fail check when contents of include result in a 90%+ minification ratio', function () {
				// Minifying this will trim whitespace and equal an exactly 90% minification ratio
				includeContents = 'var blah = "123456789012345678901234567890123456789012345678901234567890123456789012345678";        ';
				page.html = '<script src="/design/scripts/script.js"></script>';
				check.exec(page, response, pagesCrawled).then(function () {
					(check.passed === null).should.be.true;
				});
			});

			it('does not fail check when contents of include are less than 1000 characters', function () {
				includeContents = _.range(999).map(function () { return ' '; }).join('');
				page.html = '<script src="/design/scripts/script.js"></script>';
				check.exec(page, response, pagesCrawled).then(function () {
					(check.passed === null).should.be.true;
				});
			});

			it('fails check when contents of include result in a *less than* 90% minification ratio', function () {
				// Minifying this will trim whitespace and equal less than 90% minification ratios
				includeContents = 'var blah = "12345678901234567";                                                              var blah = "12345678901234567";                                                              var blah = "12345678901234567";                                                              var blah = "12345678901234567";                                                              var blah = "12345678901234567";                                                              var blah = "12345678901234567";                                                              var blah = "12345678901234567";                                                              var blah = "12345678901234567";                                                              var blah = "12345678901234567";                                                              var blah = "12345678901234567";                                                              var blah = "12345678901234567";                                                              var blah = "12345678901234567";                                                              var blah = "12345678901234567";                                                              ';
				page.html = '<script src="/design/scripts/script.js"></script>';
				check.exec(page, response, pagesCrawled).then(function () {
					check.passed.should.be.false;
				});
			});

			it('records the minification ratio in the check\'s data property', function () {
				includeContents = 'var blah = "123456789012345678901234567890123456789012345678901234567890123456789012345678123456789012345678901234567890123456789012345678901234567890123456789012345678123456789012345678901234567890123456789012345678901234567890123456789012345678123456789012345678901234567890123456789012345678901234567890123456789012345678123456789012345678901234567890123456789012345678901234567890123456789012345678123456789012345678901234567890123456789012345678901234567890123456789012345678123456789012345678901234567890123456789012345678901234567890123456789012345678123456789012345678901234567890123456789012345678901234567890123456789012345678123456789012345678901234567890123456789012345678901234567890123456789012345678123456789012345678901234567890123456789012345678901234567890123456789012345678123456789012345678901234567890123456789012345678901234567890123456789012345678123456789012345678901234567890123456789012345678901234567890123456789012345678123456789012345678901234567890123456789012345678901234567890123456789012345678123456789012345678901234567890123456789012345678901234567890123456789012345678123456789012345678901234567890123456789012345678901234567890123456789012345678";        ';
				page.html = '<script src="/design/scripts/script.js"></script>';
				check.exec(page, response, pagesCrawled).then(function () {
					check.data['http://www.google.com/design/scripts/script.js'].ratio.should.eql(0.008389261744966459);
				});
			});

			it('fails when parse errors are encountered', function () {
				sinon.stub(console, 'log'); // Silencing console logs

				includeContents = 'var blah = ;        var blah = ;        var blah = ;        var blah = ;        var blah = ;        var blah = ;        var blah = ;        var blah = ;        var blah = ;        var blah = ;        var blah = ;        var blah = ;        var blah = ;        var blah = ;        var blah = ;        var blah = ;        var blah = ;        var blah = ;        var blah = ;        var blah = ;        var blah = ;        var blah = ;        var blah = ;        var blah = ;        var blah = ;        var blah = ;        var blah = ;        var blah = ;        var blah = ;        var blah = ;        var blah = ;        var blah = ;        var blah = ;        var blah = ;        var blah = ;        var blah = ;        var blah = ;        var blah = ;        var blah = ;        var blah = ;        var blah = ;        var blah = ;        var blah = ;        var blah = ;        var blah = ;        var blah = ;        var blah = ;        var blah = ;        var blah = ;        var blah = ;        var blah = ;        ';
				page.html = '<script src="/design/scripts/script.js"></script>';
				check.exec(page, response, pagesCrawled).then(function () {
					check.data['http://www.google.com/design/scripts/script.js'].error.should.eql('Parsing error: Unexpected token: punc (;) on line 1 col 11');
					check.data['http://www.google.com/design/scripts/script.js'].isMinified.should.be.false;
					check.passed.should.be.false;
				});

				console.log.restore();
			});

			it('fails when a bad status code is encountered', function () {
				includeContents = '';
				response.statusCode = 404;
				page.html = '<script src="/design/scripts/script.js"></script>';
				check.exec(page, response, pagesCrawled).then(function () {
					check.passed.should.be.false;
					check.data['http://www.google.com/design/scripts/script.js'].error.should.eql(response.statusCode + ' status code');
				});
			});

			it('fails when an error occurs during a request for the include', function () {
				includeContents = '';
				error = new Error('some error');

				sinon.stub(console, 'log'); // silence console.error

				page.html = '<script src="/design/scripts/script.js"></script>';
				check.exec(page, response, pagesCrawled).then(function () {
					check.passed.should.be.false;
					check.data['http://www.google.com/design/scripts/script.js'].error.should.eql('Error: ' + error.message);
				});

				console.log.restore();
			});

			// Excluded situations

			function isExcluded() {
				it('does not fail check', function () {
					(check.passed === null).should.be.true;
				});

				it('sets justWarn property of include (in check.data) to true', function () {
					check.data[_.keys(check.data)[0]].justWarn.should.eql(true);
				});
			}

			describe('external includes that are not minified', function () {
			
				beforeEach(function () {
					// Minifying this will trim whitespace and equal less than 90% minification ratios
					includeContents = 'var blah = "12345678901234567";                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      ';
					page.html = '<script src="http://external.com/design/scripts/script.js"></script>';
					check.exec(page, response, pagesCrawled);
				});

				isExcluded();
			
			});

			describe('unminified /includes/*', function () {

				beforeEach(function () {
					// Minifying this will trim whitespace and equal less than 90% minification ratios
					includeContents = 'var blah = "12345678901234567";                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      ';
					page.html = '<script src="http://www.google.com/includes/script.js"></script>';
					check.exec(page, response, pagesCrawled);
				});

				isExcluded();

			});

			describe('unminified init.js', function () {

				beforeEach(function () {// Minifying this will trim whitespace and equal less than 90% minification ratios
					includeContents = 'var blah = "12345678901234567";                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      ';
					page.html = '<script src="http://www.google.com/design/scripts/init.js"></script>';
					check.exec(page, response, pagesCrawled);
				});

				isExcluded();
				
			});

			describe('unminified /blog/mt.js', function () {

				beforeEach(function () {
					// Minifying this will trim whitespace and equal less than 90% minification ratios
					includeContents = 'var blah = "12345678901234567";                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      ';
					page.html = '<script src="http://www.google.com/blog/mt.js"></script>';
					check.exec(page, response, pagesCrawled);
				});

				isExcluded();
				
			});
		
		});

		describe('for CSS', function () {

			it('does not fail check when contents of include result in a 90%+ minification ratio', function () {
				// Minifying this will trim whitespace and equal an exactly 90% minification ratio
				includeContents = 'div { content: "123456789012345678901234567890123456789012345678901234567890123456789012345" }      div { content: "123456789012345678901234567890123456789012345678901234567890123456789012345" }      div { content: "123456789012345678901234567890123456789012345678901234567890123456789012345" }      div { content: "123456789012345678901234567890123456789012345678901234567890123456789012345" }      div { content: "123456789012345678901234567890123456789012345678901234567890123456789012345" }      div { content: "123456789012345678901234567890123456789012345678901234567890123456789012345" }      div { content: "123456789012345678901234567890123456789012345678901234567890123456789012345" }      div { content: "123456789012345678901234567890123456789012345678901234567890123456789012345" }      div { content: "123456789012345678901234567890123456789012345678901234567890123456789012345" }      div { content: "123456789012345678901234567890123456789012345678901234567890123456789012345" }      div { content: "123456789012345678901234567890123456789012345678901234567890123456789012345" }      div { content: "123456789012345678901234567890123456789012345678901234567890123456789012345" }      div { content: "123456789012345678901234567890123456789012345678901234567890123456789012345" }      ';
				page.html = '<link rel="stylesheet" href="/design/css/site.css">';
				check.exec(page, response, pagesCrawled).then(function () {
					(check.passed === null).should.be.true;
				});
			});

			it('fails check when contents of include result in a *less than* 90% minification ratio', function () {
				// Minifying this will trim whitespace and equal less than 90% minification ratios
				includeContents = 'div { content: "1234567890" }                                                                    div { content: "1234567890" }                                                                    div { content: "1234567890" }                                                                    div { content: "1234567890" }                                                                    div { content: "1234567890" }                                                                    div { content: "1234567890" }                                                                    div { content: "1234567890" }                                                                    div { content: "1234567890" }                                                                    div { content: "1234567890" }                                                                    div { content: "1234567890" }                                                                    div { content: "1234567890" }                                                                    ';
				page.html = '<link rel="stylesheet" href="/design/css/site.css">';
				check.exec(page, response, pagesCrawled).then(function () {
					check.passed.should.be.false;
				});
			});

			it('records the minification ratio in the check\'s data property', function () {
				includeContents = 'div { content: "12345678901234567890123456789012345678901234567890123456789012345678901234512345678901234567890123456789012345678901234567890123456789012345678901234512345678901234567890123456789012345678901234567890123456789012345678901234512345678901234567890123456789012345678901234567890123456789012345678901234512345678901234567890123456789012345678901234567890123456789012345678901234512345678901234567890123456789012345678901234567890123456789012345678901234512345678901234567890123456789012345678901234567890123456789012345678901234512345678901234567890123456789012345678901234567890123456789012345678901234512345678901234567890123456789012345678901234567890123456789012345678901234512345678901234567890123456789012345678901234567890123456789012345678901234512345678901234567890123456789012345678901234567890123456789012345678901234512345678901234567890123456789012345678901234567890123456789012345678901234512345678901234567890123456789012345678901234567123456712345671234567123456712345674567123456745671234567456712345674567123456745671234567456712345674567123456745671234567456712345674567123456745671234567456712345674567123456745671234567" }      ';
				page.html = '<link rel="stylesheet" href="/design/css/site.css">';
				check.exec(page, response, pagesCrawled).then(function () {
					check.data['http://www.google.com/design/css/site.css'].ratio.should.eql(0.008613264427217882);
				});
			});
		
		});
	
	});

});